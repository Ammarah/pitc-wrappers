
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TPINChangeResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}TPINChangeResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tpinChangeResult"
})
@XmlRootElement(name = "TPINChangeResponse")
public class TPINChangeResponse {

    @XmlElement(name = "TPINChangeResult")
    protected TPINChangeResponseTransaction tpinChangeResult;

    /**
     * Gets the value of the tpinChangeResult property.
     * 
     * @return
     *     possible object is
     *     {@link TPINChangeResponseTransaction }
     *     
     */
    public TPINChangeResponseTransaction getTPINChangeResult() {
        return tpinChangeResult;
    }

    /**
     * Sets the value of the tpinChangeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPINChangeResponseTransaction }
     *     
     */
    public void setTPINChangeResult(TPINChangeResponseTransaction value) {
        this.tpinChangeResult = value;
    }

}
