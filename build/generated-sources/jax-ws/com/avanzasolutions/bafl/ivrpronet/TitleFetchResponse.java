
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TitleFetchResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}TitleFetchResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "titleFetchResult"
})
@XmlRootElement(name = "TitleFetchResponse")
public class TitleFetchResponse {

    @XmlElement(name = "TitleFetchResult")
    protected TitleFetchResponseTransaction titleFetchResult;

    /**
     * Gets the value of the titleFetchResult property.
     * 
     * @return
     *     possible object is
     *     {@link TitleFetchResponseTransaction }
     *     
     */
    public TitleFetchResponseTransaction getTitleFetchResult() {
        return titleFetchResult;
    }

    /**
     * Sets the value of the titleFetchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TitleFetchResponseTransaction }
     *     
     */
    public void setTitleFetchResult(TitleFetchResponseTransaction value) {
        this.titleFetchResult = value;
    }

}
