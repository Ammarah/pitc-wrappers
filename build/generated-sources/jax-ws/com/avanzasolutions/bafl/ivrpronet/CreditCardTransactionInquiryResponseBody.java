
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardTransactionInquiryResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardTransactionInquiryResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BlockCCTranInq" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockCCTranInq" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionInquiryResponseBody", propOrder = {
    "blockCCTranInq"
})
public class CreditCardTransactionInquiryResponseBody {

    @XmlElement(name = "BlockCCTranInq")
    protected BlockCCTranInq blockCCTranInq;

    /**
     * Gets the value of the blockCCTranInq property.
     * 
     * @return
     *     possible object is
     *     {@link BlockCCTranInq }
     *     
     */
    public BlockCCTranInq getBlockCCTranInq() {
        return blockCCTranInq;
    }

    /**
     * Sets the value of the blockCCTranInq property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockCCTranInq }
     *     
     */
    public void setBlockCCTranInq(BlockCCTranInq value) {
        this.blockCCTranInq = value;
    }

}
