
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChargeCustomerResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}ChargeCustomerResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "chargeCustomerResult"
})
@XmlRootElement(name = "ChargeCustomerResponse")
public class ChargeCustomerResponse {

    @XmlElement(name = "ChargeCustomerResult")
    protected ChargeCustomerResponseTransaction chargeCustomerResult;

    /**
     * Gets the value of the chargeCustomerResult property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeCustomerResponseTransaction }
     *     
     */
    public ChargeCustomerResponseTransaction getChargeCustomerResult() {
        return chargeCustomerResult;
    }

    /**
     * Sets the value of the chargeCustomerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeCustomerResponseTransaction }
     *     
     */
    public void setChargeCustomerResult(ChargeCustomerResponseTransaction value) {
        this.chargeCustomerResult = value;
    }

}
