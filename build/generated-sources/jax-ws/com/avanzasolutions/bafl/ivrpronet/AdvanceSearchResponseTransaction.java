
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdvanceSearchResponseTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdvanceSearchResponseTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}Header" minOccurs="0"/>
 *         &lt;element name="BlockAdvanceSearch" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockAdvanceSearch" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdvanceSearchResponseTransaction", propOrder = {
    "header",
    "blockAdvanceSearch"
})
public class AdvanceSearchResponseTransaction {

    @XmlElement(name = "Header")
    protected Header header;
    @XmlElement(name = "BlockAdvanceSearch")
    protected BlockAdvanceSearch blockAdvanceSearch;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the blockAdvanceSearch property.
     * 
     * @return
     *     possible object is
     *     {@link BlockAdvanceSearch }
     *     
     */
    public BlockAdvanceSearch getBlockAdvanceSearch() {
        return blockAdvanceSearch;
    }

    /**
     * Sets the value of the blockAdvanceSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockAdvanceSearch }
     *     
     */
    public void setBlockAdvanceSearch(BlockAdvanceSearch value) {
        this.blockAdvanceSearch = value;
    }

}
