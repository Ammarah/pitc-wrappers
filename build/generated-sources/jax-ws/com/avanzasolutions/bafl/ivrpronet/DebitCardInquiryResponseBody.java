
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DebitCardInquiryResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DebitCardInquiryResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardExpiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WithdrawalCycleBeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WithdrawalCycleLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WithdrawalLimits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailableWithdrawalLimits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseCycleBeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseCycleLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailablePurchaseLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundsTransCycleBeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundsTransCycleLen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundsTransLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailableFundsTransLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxPINRetries" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PINRetriesRemaining" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PINExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowATMSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowCIRRUSSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowPHX_SW1LinkSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowEFTPOSSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowVISASource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowAMEXSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowMNETSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowIVRSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowMobileSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowCardProSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitCardInquiryResponseBody", propOrder = {
    "debitCardNumber",
    "cardExpiry",
    "procCode",
    "withdrawalCycleBeginDate",
    "withdrawalCycleLength",
    "withdrawalLimits",
    "availableWithdrawalLimits",
    "purchaseFlag",
    "purchaseCycleBeginDate",
    "purchaseCycleLength",
    "purchaseLimit",
    "availablePurchaseLimit",
    "fundFlag",
    "fundsTransCycleBeginDate",
    "fundsTransCycleLen",
    "fundsTransLimit",
    "availableFundsTransLimit",
    "name",
    "address1",
    "address2",
    "maxPINRetries",
    "pinRetriesRemaining",
    "pinExpiryDate",
    "cardStatusCode",
    "allowATMSource",
    "allowCIRRUSSource",
    "allowPHXSW1LinkSource",
    "allowEFTPOSSource",
    "allowVISASource",
    "allowAMEXSource",
    "allowMNETSource",
    "allowIVRSource",
    "allowMobileSource",
    "allowCardProSource"
})
public class DebitCardInquiryResponseBody {

    @XmlElement(name = "DebitCardNumber")
    protected String debitCardNumber;
    @XmlElement(name = "CardExpiry")
    protected String cardExpiry;
    @XmlElement(name = "ProcCode")
    protected String procCode;
    @XmlElement(name = "WithdrawalCycleBeginDate")
    protected String withdrawalCycleBeginDate;
    @XmlElement(name = "WithdrawalCycleLength")
    protected String withdrawalCycleLength;
    @XmlElement(name = "WithdrawalLimits")
    protected String withdrawalLimits;
    @XmlElement(name = "AvailableWithdrawalLimits")
    protected String availableWithdrawalLimits;
    @XmlElement(name = "PurchaseFlag")
    protected String purchaseFlag;
    @XmlElement(name = "PurchaseCycleBeginDate")
    protected String purchaseCycleBeginDate;
    @XmlElement(name = "PurchaseCycleLength")
    protected String purchaseCycleLength;
    @XmlElement(name = "PurchaseLimit")
    protected String purchaseLimit;
    @XmlElement(name = "AvailablePurchaseLimit")
    protected String availablePurchaseLimit;
    @XmlElement(name = "FundFlag")
    protected String fundFlag;
    @XmlElement(name = "FundsTransCycleBeginDate")
    protected String fundsTransCycleBeginDate;
    @XmlElement(name = "FundsTransCycleLen")
    protected String fundsTransCycleLen;
    @XmlElement(name = "FundsTransLimit")
    protected String fundsTransLimit;
    @XmlElement(name = "AvailableFundsTransLimit")
    protected String availableFundsTransLimit;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Address1")
    protected String address1;
    @XmlElement(name = "Address2")
    protected String address2;
    @XmlElement(name = "MaxPINRetries")
    protected String maxPINRetries;
    @XmlElement(name = "PINRetriesRemaining")
    protected String pinRetriesRemaining;
    @XmlElement(name = "PINExpiryDate")
    protected String pinExpiryDate;
    @XmlElement(name = "CardStatusCode")
    protected String cardStatusCode;
    @XmlElement(name = "AllowATMSource")
    protected String allowATMSource;
    @XmlElement(name = "AllowCIRRUSSource")
    protected String allowCIRRUSSource;
    @XmlElement(name = "AllowPHX_SW1LinkSource")
    protected String allowPHXSW1LinkSource;
    @XmlElement(name = "AllowEFTPOSSource")
    protected String allowEFTPOSSource;
    @XmlElement(name = "AllowVISASource")
    protected String allowVISASource;
    @XmlElement(name = "AllowAMEXSource")
    protected String allowAMEXSource;
    @XmlElement(name = "AllowMNETSource")
    protected String allowMNETSource;
    @XmlElement(name = "AllowIVRSource")
    protected String allowIVRSource;
    @XmlElement(name = "AllowMobileSource")
    protected String allowMobileSource;
    @XmlElement(name = "AllowCardProSource")
    protected String allowCardProSource;

    /**
     * Gets the value of the debitCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitCardNumber() {
        return debitCardNumber;
    }

    /**
     * Sets the value of the debitCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitCardNumber(String value) {
        this.debitCardNumber = value;
    }

    /**
     * Gets the value of the cardExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardExpiry() {
        return cardExpiry;
    }

    /**
     * Sets the value of the cardExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardExpiry(String value) {
        this.cardExpiry = value;
    }

    /**
     * Gets the value of the procCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcCode() {
        return procCode;
    }

    /**
     * Sets the value of the procCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcCode(String value) {
        this.procCode = value;
    }

    /**
     * Gets the value of the withdrawalCycleBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithdrawalCycleBeginDate() {
        return withdrawalCycleBeginDate;
    }

    /**
     * Sets the value of the withdrawalCycleBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithdrawalCycleBeginDate(String value) {
        this.withdrawalCycleBeginDate = value;
    }

    /**
     * Gets the value of the withdrawalCycleLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithdrawalCycleLength() {
        return withdrawalCycleLength;
    }

    /**
     * Sets the value of the withdrawalCycleLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithdrawalCycleLength(String value) {
        this.withdrawalCycleLength = value;
    }

    /**
     * Gets the value of the withdrawalLimits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithdrawalLimits() {
        return withdrawalLimits;
    }

    /**
     * Sets the value of the withdrawalLimits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithdrawalLimits(String value) {
        this.withdrawalLimits = value;
    }

    /**
     * Gets the value of the availableWithdrawalLimits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailableWithdrawalLimits() {
        return availableWithdrawalLimits;
    }

    /**
     * Sets the value of the availableWithdrawalLimits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailableWithdrawalLimits(String value) {
        this.availableWithdrawalLimits = value;
    }

    /**
     * Gets the value of the purchaseFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseFlag() {
        return purchaseFlag;
    }

    /**
     * Sets the value of the purchaseFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseFlag(String value) {
        this.purchaseFlag = value;
    }

    /**
     * Gets the value of the purchaseCycleBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseCycleBeginDate() {
        return purchaseCycleBeginDate;
    }

    /**
     * Sets the value of the purchaseCycleBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseCycleBeginDate(String value) {
        this.purchaseCycleBeginDate = value;
    }

    /**
     * Gets the value of the purchaseCycleLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseCycleLength() {
        return purchaseCycleLength;
    }

    /**
     * Sets the value of the purchaseCycleLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseCycleLength(String value) {
        this.purchaseCycleLength = value;
    }

    /**
     * Gets the value of the purchaseLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseLimit() {
        return purchaseLimit;
    }

    /**
     * Sets the value of the purchaseLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseLimit(String value) {
        this.purchaseLimit = value;
    }

    /**
     * Gets the value of the availablePurchaseLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailablePurchaseLimit() {
        return availablePurchaseLimit;
    }

    /**
     * Sets the value of the availablePurchaseLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailablePurchaseLimit(String value) {
        this.availablePurchaseLimit = value;
    }

    /**
     * Gets the value of the fundFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundFlag() {
        return fundFlag;
    }

    /**
     * Sets the value of the fundFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundFlag(String value) {
        this.fundFlag = value;
    }

    /**
     * Gets the value of the fundsTransCycleBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundsTransCycleBeginDate() {
        return fundsTransCycleBeginDate;
    }

    /**
     * Sets the value of the fundsTransCycleBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundsTransCycleBeginDate(String value) {
        this.fundsTransCycleBeginDate = value;
    }

    /**
     * Gets the value of the fundsTransCycleLen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundsTransCycleLen() {
        return fundsTransCycleLen;
    }

    /**
     * Sets the value of the fundsTransCycleLen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundsTransCycleLen(String value) {
        this.fundsTransCycleLen = value;
    }

    /**
     * Gets the value of the fundsTransLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundsTransLimit() {
        return fundsTransLimit;
    }

    /**
     * Sets the value of the fundsTransLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundsTransLimit(String value) {
        this.fundsTransLimit = value;
    }

    /**
     * Gets the value of the availableFundsTransLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailableFundsTransLimit() {
        return availableFundsTransLimit;
    }

    /**
     * Sets the value of the availableFundsTransLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailableFundsTransLimit(String value) {
        this.availableFundsTransLimit = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the maxPINRetries property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxPINRetries() {
        return maxPINRetries;
    }

    /**
     * Sets the value of the maxPINRetries property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxPINRetries(String value) {
        this.maxPINRetries = value;
    }

    /**
     * Gets the value of the pinRetriesRemaining property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPINRetriesRemaining() {
        return pinRetriesRemaining;
    }

    /**
     * Sets the value of the pinRetriesRemaining property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPINRetriesRemaining(String value) {
        this.pinRetriesRemaining = value;
    }

    /**
     * Gets the value of the pinExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPINExpiryDate() {
        return pinExpiryDate;
    }

    /**
     * Sets the value of the pinExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPINExpiryDate(String value) {
        this.pinExpiryDate = value;
    }

    /**
     * Gets the value of the cardStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardStatusCode() {
        return cardStatusCode;
    }

    /**
     * Sets the value of the cardStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardStatusCode(String value) {
        this.cardStatusCode = value;
    }

    /**
     * Gets the value of the allowATMSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowATMSource() {
        return allowATMSource;
    }

    /**
     * Sets the value of the allowATMSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowATMSource(String value) {
        this.allowATMSource = value;
    }

    /**
     * Gets the value of the allowCIRRUSSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowCIRRUSSource() {
        return allowCIRRUSSource;
    }

    /**
     * Sets the value of the allowCIRRUSSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowCIRRUSSource(String value) {
        this.allowCIRRUSSource = value;
    }

    /**
     * Gets the value of the allowPHXSW1LinkSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowPHXSW1LinkSource() {
        return allowPHXSW1LinkSource;
    }

    /**
     * Sets the value of the allowPHXSW1LinkSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowPHXSW1LinkSource(String value) {
        this.allowPHXSW1LinkSource = value;
    }

    /**
     * Gets the value of the allowEFTPOSSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowEFTPOSSource() {
        return allowEFTPOSSource;
    }

    /**
     * Sets the value of the allowEFTPOSSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowEFTPOSSource(String value) {
        this.allowEFTPOSSource = value;
    }

    /**
     * Gets the value of the allowVISASource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowVISASource() {
        return allowVISASource;
    }

    /**
     * Sets the value of the allowVISASource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowVISASource(String value) {
        this.allowVISASource = value;
    }

    /**
     * Gets the value of the allowAMEXSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowAMEXSource() {
        return allowAMEXSource;
    }

    /**
     * Sets the value of the allowAMEXSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowAMEXSource(String value) {
        this.allowAMEXSource = value;
    }

    /**
     * Gets the value of the allowMNETSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowMNETSource() {
        return allowMNETSource;
    }

    /**
     * Sets the value of the allowMNETSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowMNETSource(String value) {
        this.allowMNETSource = value;
    }

    /**
     * Gets the value of the allowIVRSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowIVRSource() {
        return allowIVRSource;
    }

    /**
     * Sets the value of the allowIVRSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowIVRSource(String value) {
        this.allowIVRSource = value;
    }

    /**
     * Gets the value of the allowMobileSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowMobileSource() {
        return allowMobileSource;
    }

    /**
     * Sets the value of the allowMobileSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowMobileSource(String value) {
        this.allowMobileSource = value;
    }

    /**
     * Gets the value of the allowCardProSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowCardProSource() {
        return allowCardProSource;
    }

    /**
     * Sets the value of the allowCardProSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowCardProSource(String value) {
        this.allowCardProSource = value;
    }

}
