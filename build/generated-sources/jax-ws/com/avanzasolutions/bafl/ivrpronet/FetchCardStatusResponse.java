
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchCardStatusResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}FetchCardStatusResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchCardStatusResult"
})
@XmlRootElement(name = "FetchCardStatusResponse")
public class FetchCardStatusResponse {

    @XmlElement(name = "FetchCardStatusResult")
    protected FetchCardStatusResponseTransaction fetchCardStatusResult;

    /**
     * Gets the value of the fetchCardStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link FetchCardStatusResponseTransaction }
     *     
     */
    public FetchCardStatusResponseTransaction getFetchCardStatusResult() {
        return fetchCardStatusResult;
    }

    /**
     * Sets the value of the fetchCardStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link FetchCardStatusResponseTransaction }
     *     
     */
    public void setFetchCardStatusResult(FetchCardStatusResponseTransaction value) {
        this.fetchCardStatusResult = value;
    }

}
