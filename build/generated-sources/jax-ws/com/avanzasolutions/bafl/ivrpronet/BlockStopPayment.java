
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockStopPayment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockStopPayment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StChequeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StChequeStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockStopPayment", propOrder = {
    "stChequeNumber",
    "stChequeStatus"
})
public class BlockStopPayment {

    @XmlElement(name = "StChequeNumber")
    protected String stChequeNumber;
    @XmlElement(name = "StChequeStatus")
    protected String stChequeStatus;

    /**
     * Gets the value of the stChequeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStChequeNumber() {
        return stChequeNumber;
    }

    /**
     * Sets the value of the stChequeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStChequeNumber(String value) {
        this.stChequeNumber = value;
    }

    /**
     * Gets the value of the stChequeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStChequeStatus() {
        return stChequeStatus;
    }

    /**
     * Sets the value of the stChequeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStChequeStatus(String value) {
        this.stChequeStatus = value;
    }

}
