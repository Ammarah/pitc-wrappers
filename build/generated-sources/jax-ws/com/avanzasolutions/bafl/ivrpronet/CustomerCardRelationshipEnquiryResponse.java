
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerCardRelationshipEnquiryResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}CustomerCardRelationshipEnquiryResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerCardRelationshipEnquiryResult"
})
@XmlRootElement(name = "CustomerCardRelationshipEnquiryResponse")
public class CustomerCardRelationshipEnquiryResponse {

    @XmlElement(name = "CustomerCardRelationshipEnquiryResult")
    protected CustomerCardRelationshipEnquiryResponseTransaction customerCardRelationshipEnquiryResult;

    /**
     * Gets the value of the customerCardRelationshipEnquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerCardRelationshipEnquiryResponseTransaction }
     *     
     */
    public CustomerCardRelationshipEnquiryResponseTransaction getCustomerCardRelationshipEnquiryResult() {
        return customerCardRelationshipEnquiryResult;
    }

    /**
     * Sets the value of the customerCardRelationshipEnquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerCardRelationshipEnquiryResponseTransaction }
     *     
     */
    public void setCustomerCardRelationshipEnquiryResult(CustomerCardRelationshipEnquiryResponseTransaction value) {
        this.customerCardRelationshipEnquiryResult = value;
    }

}
