
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockAccountDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockAccountDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExAccountCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExAccountTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExAccountBankIMD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExAccountNature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExAccountDefault" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExWithdrawalFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExPurchaseFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExTransferToFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExTransferFromFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExMiniStatementFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExBalanceInquiryFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExChequeBookFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExStatementFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExBillPaymentFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExChequeDepositFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExCashDepositFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExReserved1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExReservedPermissions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExAccountMapCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExReserved2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockAccountDetail", propOrder = {
    "exAccountNumber",
    "exAccountType",
    "exAccountCurrency",
    "exAccountTitle",
    "exAccountBankIMD",
    "exBranchCode",
    "exAccountNature",
    "exAccountDefault",
    "exWithdrawalFlag",
    "exPurchaseFlag",
    "exTransferToFlag",
    "exTransferFromFlag",
    "exMiniStatementFlag",
    "exBalanceInquiryFlag",
    "exChequeBookFlag",
    "exStatementFlag",
    "exBillPaymentFlag",
    "exChequeDepositFlag",
    "exCashDepositFlag",
    "exReserved1",
    "exReservedPermissions",
    "exAccountMapCode",
    "exReserved2"
})
public class BlockAccountDetail {

    @XmlElement(name = "ExAccountNumber")
    protected String exAccountNumber;
    @XmlElement(name = "ExAccountType")
    protected String exAccountType;
    @XmlElement(name = "ExAccountCurrency")
    protected String exAccountCurrency;
    @XmlElement(name = "ExAccountTitle")
    protected String exAccountTitle;
    @XmlElement(name = "ExAccountBankIMD")
    protected String exAccountBankIMD;
    @XmlElement(name = "ExBranchCode")
    protected String exBranchCode;
    @XmlElement(name = "ExAccountNature")
    protected String exAccountNature;
    @XmlElement(name = "ExAccountDefault")
    protected String exAccountDefault;
    @XmlElement(name = "ExWithdrawalFlag")
    protected String exWithdrawalFlag;
    @XmlElement(name = "ExPurchaseFlag")
    protected String exPurchaseFlag;
    @XmlElement(name = "ExTransferToFlag")
    protected String exTransferToFlag;
    @XmlElement(name = "ExTransferFromFlag")
    protected String exTransferFromFlag;
    @XmlElement(name = "ExMiniStatementFlag")
    protected String exMiniStatementFlag;
    @XmlElement(name = "ExBalanceInquiryFlag")
    protected String exBalanceInquiryFlag;
    @XmlElement(name = "ExChequeBookFlag")
    protected String exChequeBookFlag;
    @XmlElement(name = "ExStatementFlag")
    protected String exStatementFlag;
    @XmlElement(name = "ExBillPaymentFlag")
    protected String exBillPaymentFlag;
    @XmlElement(name = "ExChequeDepositFlag")
    protected String exChequeDepositFlag;
    @XmlElement(name = "ExCashDepositFlag")
    protected String exCashDepositFlag;
    @XmlElement(name = "ExReserved1")
    protected String exReserved1;
    @XmlElement(name = "ExReservedPermissions")
    protected String exReservedPermissions;
    @XmlElement(name = "ExAccountMapCode")
    protected String exAccountMapCode;
    @XmlElement(name = "ExReserved2")
    protected String exReserved2;

    /**
     * Gets the value of the exAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountNumber() {
        return exAccountNumber;
    }

    /**
     * Sets the value of the exAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountNumber(String value) {
        this.exAccountNumber = value;
    }

    /**
     * Gets the value of the exAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountType() {
        return exAccountType;
    }

    /**
     * Sets the value of the exAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountType(String value) {
        this.exAccountType = value;
    }

    /**
     * Gets the value of the exAccountCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountCurrency() {
        return exAccountCurrency;
    }

    /**
     * Sets the value of the exAccountCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountCurrency(String value) {
        this.exAccountCurrency = value;
    }

    /**
     * Gets the value of the exAccountTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountTitle() {
        return exAccountTitle;
    }

    /**
     * Sets the value of the exAccountTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountTitle(String value) {
        this.exAccountTitle = value;
    }

    /**
     * Gets the value of the exAccountBankIMD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountBankIMD() {
        return exAccountBankIMD;
    }

    /**
     * Sets the value of the exAccountBankIMD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountBankIMD(String value) {
        this.exAccountBankIMD = value;
    }

    /**
     * Gets the value of the exBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExBranchCode() {
        return exBranchCode;
    }

    /**
     * Sets the value of the exBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExBranchCode(String value) {
        this.exBranchCode = value;
    }

    /**
     * Gets the value of the exAccountNature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountNature() {
        return exAccountNature;
    }

    /**
     * Sets the value of the exAccountNature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountNature(String value) {
        this.exAccountNature = value;
    }

    /**
     * Gets the value of the exAccountDefault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountDefault() {
        return exAccountDefault;
    }

    /**
     * Sets the value of the exAccountDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountDefault(String value) {
        this.exAccountDefault = value;
    }

    /**
     * Gets the value of the exWithdrawalFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExWithdrawalFlag() {
        return exWithdrawalFlag;
    }

    /**
     * Sets the value of the exWithdrawalFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExWithdrawalFlag(String value) {
        this.exWithdrawalFlag = value;
    }

    /**
     * Gets the value of the exPurchaseFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExPurchaseFlag() {
        return exPurchaseFlag;
    }

    /**
     * Sets the value of the exPurchaseFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExPurchaseFlag(String value) {
        this.exPurchaseFlag = value;
    }

    /**
     * Gets the value of the exTransferToFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExTransferToFlag() {
        return exTransferToFlag;
    }

    /**
     * Sets the value of the exTransferToFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExTransferToFlag(String value) {
        this.exTransferToFlag = value;
    }

    /**
     * Gets the value of the exTransferFromFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExTransferFromFlag() {
        return exTransferFromFlag;
    }

    /**
     * Sets the value of the exTransferFromFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExTransferFromFlag(String value) {
        this.exTransferFromFlag = value;
    }

    /**
     * Gets the value of the exMiniStatementFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExMiniStatementFlag() {
        return exMiniStatementFlag;
    }

    /**
     * Sets the value of the exMiniStatementFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExMiniStatementFlag(String value) {
        this.exMiniStatementFlag = value;
    }

    /**
     * Gets the value of the exBalanceInquiryFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExBalanceInquiryFlag() {
        return exBalanceInquiryFlag;
    }

    /**
     * Sets the value of the exBalanceInquiryFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExBalanceInquiryFlag(String value) {
        this.exBalanceInquiryFlag = value;
    }

    /**
     * Gets the value of the exChequeBookFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExChequeBookFlag() {
        return exChequeBookFlag;
    }

    /**
     * Sets the value of the exChequeBookFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExChequeBookFlag(String value) {
        this.exChequeBookFlag = value;
    }

    /**
     * Gets the value of the exStatementFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExStatementFlag() {
        return exStatementFlag;
    }

    /**
     * Sets the value of the exStatementFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExStatementFlag(String value) {
        this.exStatementFlag = value;
    }

    /**
     * Gets the value of the exBillPaymentFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExBillPaymentFlag() {
        return exBillPaymentFlag;
    }

    /**
     * Sets the value of the exBillPaymentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExBillPaymentFlag(String value) {
        this.exBillPaymentFlag = value;
    }

    /**
     * Gets the value of the exChequeDepositFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExChequeDepositFlag() {
        return exChequeDepositFlag;
    }

    /**
     * Sets the value of the exChequeDepositFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExChequeDepositFlag(String value) {
        this.exChequeDepositFlag = value;
    }

    /**
     * Gets the value of the exCashDepositFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExCashDepositFlag() {
        return exCashDepositFlag;
    }

    /**
     * Sets the value of the exCashDepositFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExCashDepositFlag(String value) {
        this.exCashDepositFlag = value;
    }

    /**
     * Gets the value of the exReserved1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExReserved1() {
        return exReserved1;
    }

    /**
     * Sets the value of the exReserved1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExReserved1(String value) {
        this.exReserved1 = value;
    }

    /**
     * Gets the value of the exReservedPermissions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExReservedPermissions() {
        return exReservedPermissions;
    }

    /**
     * Sets the value of the exReservedPermissions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExReservedPermissions(String value) {
        this.exReservedPermissions = value;
    }

    /**
     * Gets the value of the exAccountMapCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExAccountMapCode() {
        return exAccountMapCode;
    }

    /**
     * Sets the value of the exAccountMapCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExAccountMapCode(String value) {
        this.exAccountMapCode = value;
    }

    /**
     * Gets the value of the exReserved2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExReserved2() {
        return exReserved2;
    }

    /**
     * Sets the value of the exReserved2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExReserved2(String value) {
        this.exReserved2 = value;
    }

}
