
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerSearchResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}CustomerSearchResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerSearchResult"
})
@XmlRootElement(name = "CustomerSearchResponse")
public class CustomerSearchResponse {

    @XmlElement(name = "CustomerSearchResult")
    protected CustomerSearchResponseTransaction customerSearchResult;

    /**
     * Gets the value of the customerSearchResult property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerSearchResponseTransaction }
     *     
     */
    public CustomerSearchResponseTransaction getCustomerSearchResult() {
        return customerSearchResult;
    }

    /**
     * Sets the value of the customerSearchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerSearchResponseTransaction }
     *     
     */
    public void setCustomerSearchResult(CustomerSearchResponseTransaction value) {
        this.customerSearchResult = value;
    }

}
