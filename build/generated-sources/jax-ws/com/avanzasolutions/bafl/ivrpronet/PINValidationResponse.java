
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PINValidationResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}PINValidationResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pinValidationResult"
})
@XmlRootElement(name = "PINValidationResponse")
public class PINValidationResponse {

    @XmlElement(name = "PINValidationResult")
    protected PINValidationResponseTransaction pinValidationResult;

    /**
     * Gets the value of the pinValidationResult property.
     * 
     * @return
     *     possible object is
     *     {@link PINValidationResponseTransaction }
     *     
     */
    public PINValidationResponseTransaction getPINValidationResult() {
        return pinValidationResult;
    }

    /**
     * Sets the value of the pinValidationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PINValidationResponseTransaction }
     *     
     */
    public void setPINValidationResult(PINValidationResponseTransaction value) {
        this.pinValidationResult = value;
    }

}
