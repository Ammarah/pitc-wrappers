
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardRelationshipInquiryResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardRelationshipInquiryResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BlockCCardsInfo" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockCCardsInfo" minOccurs="0"/>
 *         &lt;element name="BlockCCAppsInfo" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockCCAppsInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardRelationshipInquiryResponseBody", propOrder = {
    "blockCCardsInfo",
    "blockCCAppsInfo"
})
public class CreditCardRelationshipInquiryResponseBody {

    @XmlElement(name = "BlockCCardsInfo")
    protected BlockCCardsInfo blockCCardsInfo;
    @XmlElement(name = "BlockCCAppsInfo")
    protected BlockCCAppsInfo blockCCAppsInfo;

    /**
     * Gets the value of the blockCCardsInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BlockCCardsInfo }
     *     
     */
    public BlockCCardsInfo getBlockCCardsInfo() {
        return blockCCardsInfo;
    }

    /**
     * Sets the value of the blockCCardsInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockCCardsInfo }
     *     
     */
    public void setBlockCCardsInfo(BlockCCardsInfo value) {
        this.blockCCardsInfo = value;
    }

    /**
     * Gets the value of the blockCCAppsInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BlockCCAppsInfo }
     *     
     */
    public BlockCCAppsInfo getBlockCCAppsInfo() {
        return blockCCAppsInfo;
    }

    /**
     * Sets the value of the blockCCAppsInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockCCAppsInfo }
     *     
     */
    public void setBlockCCAppsInfo(BlockCCAppsInfo value) {
        this.blockCCAppsInfo = value;
    }

}
