
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillPaymentResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BillPaymentResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "billPaymentResult"
})
@XmlRootElement(name = "BillPaymentResponse")
public class BillPaymentResponse {

    @XmlElement(name = "BillPaymentResult")
    protected BillPaymentResponseTransaction billPaymentResult;

    /**
     * Gets the value of the billPaymentResult property.
     * 
     * @return
     *     possible object is
     *     {@link BillPaymentResponseTransaction }
     *     
     */
    public BillPaymentResponseTransaction getBillPaymentResult() {
        return billPaymentResult;
    }

    /**
     * Sets the value of the billPaymentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillPaymentResponseTransaction }
     *     
     */
    public void setBillPaymentResult(BillPaymentResponseTransaction value) {
        this.billPaymentResult = value;
    }

}
