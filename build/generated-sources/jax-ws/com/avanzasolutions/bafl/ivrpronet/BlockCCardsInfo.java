
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockCCardsInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockCCardsInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCBillingCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditCHolderTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CredictCardHolderFullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditCardHolderDOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditCardHolderMMN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardHolderCompanyNameAndMobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockCCardsInfo", propOrder = {
    "creditCardNumber",
    "ccExpiryDate",
    "ccProductCode",
    "ccBillingCurrencyCode",
    "creditCHolderTitle",
    "credictCardHolderFullName",
    "creditCardHolderDOB",
    "creditCardHolderMMN",
    "cardHolderCompanyNameAndMobileNumber"
})
public class BlockCCardsInfo {

    @XmlElement(name = "CreditCardNumber")
    protected String creditCardNumber;
    @XmlElement(name = "CCExpiryDate")
    protected String ccExpiryDate;
    @XmlElement(name = "CCProductCode")
    protected String ccProductCode;
    @XmlElement(name = "CCBillingCurrencyCode")
    protected String ccBillingCurrencyCode;
    @XmlElement(name = "CreditCHolderTitle")
    protected String creditCHolderTitle;
    @XmlElement(name = "CredictCardHolderFullName")
    protected String credictCardHolderFullName;
    @XmlElement(name = "CreditCardHolderDOB")
    protected String creditCardHolderDOB;
    @XmlElement(name = "CreditCardHolderMMN")
    protected String creditCardHolderMMN;
    @XmlElement(name = "CardHolderCompanyNameAndMobileNumber")
    protected String cardHolderCompanyNameAndMobileNumber;

    /**
     * Gets the value of the creditCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    /**
     * Sets the value of the creditCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardNumber(String value) {
        this.creditCardNumber = value;
    }

    /**
     * Gets the value of the ccExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCExpiryDate() {
        return ccExpiryDate;
    }

    /**
     * Sets the value of the ccExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCExpiryDate(String value) {
        this.ccExpiryDate = value;
    }

    /**
     * Gets the value of the ccProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCProductCode() {
        return ccProductCode;
    }

    /**
     * Sets the value of the ccProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCProductCode(String value) {
        this.ccProductCode = value;
    }

    /**
     * Gets the value of the ccBillingCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCBillingCurrencyCode() {
        return ccBillingCurrencyCode;
    }

    /**
     * Sets the value of the ccBillingCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCBillingCurrencyCode(String value) {
        this.ccBillingCurrencyCode = value;
    }

    /**
     * Gets the value of the creditCHolderTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCHolderTitle() {
        return creditCHolderTitle;
    }

    /**
     * Sets the value of the creditCHolderTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCHolderTitle(String value) {
        this.creditCHolderTitle = value;
    }

    /**
     * Gets the value of the credictCardHolderFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCredictCardHolderFullName() {
        return credictCardHolderFullName;
    }

    /**
     * Sets the value of the credictCardHolderFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCredictCardHolderFullName(String value) {
        this.credictCardHolderFullName = value;
    }

    /**
     * Gets the value of the creditCardHolderDOB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardHolderDOB() {
        return creditCardHolderDOB;
    }

    /**
     * Sets the value of the creditCardHolderDOB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardHolderDOB(String value) {
        this.creditCardHolderDOB = value;
    }

    /**
     * Gets the value of the creditCardHolderMMN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardHolderMMN() {
        return creditCardHolderMMN;
    }

    /**
     * Sets the value of the creditCardHolderMMN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardHolderMMN(String value) {
        this.creditCardHolderMMN = value;
    }

    /**
     * Gets the value of the cardHolderCompanyNameAndMobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardHolderCompanyNameAndMobileNumber() {
        return cardHolderCompanyNameAndMobileNumber;
    }

    /**
     * Sets the value of the cardHolderCompanyNameAndMobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardHolderCompanyNameAndMobileNumber(String value) {
        this.cardHolderCompanyNameAndMobileNumber = value;
    }

}
