
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockCCAppsInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockCCAppsInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCAppDecisionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCAppDecisionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantFullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantDob" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantMMN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockCCAppsInfo", propOrder = {
    "applicationNumber",
    "ccAppDecisionCode",
    "ccAppDecisionDate",
    "applicantTitle",
    "applicantFullName",
    "applicantDob",
    "applicantMMN",
    "applicantCompanyName"
})
public class BlockCCAppsInfo {

    @XmlElement(name = "ApplicationNumber")
    protected String applicationNumber;
    @XmlElement(name = "CCAppDecisionCode")
    protected String ccAppDecisionCode;
    @XmlElement(name = "CCAppDecisionDate")
    protected String ccAppDecisionDate;
    @XmlElement(name = "ApplicantTitle")
    protected String applicantTitle;
    @XmlElement(name = "ApplicantFullName")
    protected String applicantFullName;
    @XmlElement(name = "ApplicantDob")
    protected String applicantDob;
    @XmlElement(name = "ApplicantMMN")
    protected String applicantMMN;
    @XmlElement(name = "ApplicantCompanyName")
    protected String applicantCompanyName;

    /**
     * Gets the value of the applicationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationNumber() {
        return applicationNumber;
    }

    /**
     * Sets the value of the applicationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationNumber(String value) {
        this.applicationNumber = value;
    }

    /**
     * Gets the value of the ccAppDecisionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCAppDecisionCode() {
        return ccAppDecisionCode;
    }

    /**
     * Sets the value of the ccAppDecisionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCAppDecisionCode(String value) {
        this.ccAppDecisionCode = value;
    }

    /**
     * Gets the value of the ccAppDecisionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCAppDecisionDate() {
        return ccAppDecisionDate;
    }

    /**
     * Sets the value of the ccAppDecisionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCAppDecisionDate(String value) {
        this.ccAppDecisionDate = value;
    }

    /**
     * Gets the value of the applicantTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantTitle() {
        return applicantTitle;
    }

    /**
     * Sets the value of the applicantTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantTitle(String value) {
        this.applicantTitle = value;
    }

    /**
     * Gets the value of the applicantFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantFullName() {
        return applicantFullName;
    }

    /**
     * Sets the value of the applicantFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantFullName(String value) {
        this.applicantFullName = value;
    }

    /**
     * Gets the value of the applicantDob property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantDob() {
        return applicantDob;
    }

    /**
     * Sets the value of the applicantDob property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantDob(String value) {
        this.applicantDob = value;
    }

    /**
     * Gets the value of the applicantMMN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantMMN() {
        return applicantMMN;
    }

    /**
     * Sets the value of the applicantMMN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantMMN(String value) {
        this.applicantMMN = value;
    }

    /**
     * Gets the value of the applicantCompanyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantCompanyName() {
        return applicantCompanyName;
    }

    /**
     * Sets the value of the applicantCompanyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantCompanyName(String value) {
        this.applicantCompanyName = value;
    }

}
