
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerCardRelationshipEnquiryResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerCardRelationshipEnquiryResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumberOfCards" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BlockCustCardRelationShip" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockCustCardRelationShip" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerCardRelationshipEnquiryResponseBody", propOrder = {
    "numberOfCards",
    "blockCustCardRelationShip"
})
public class CustomerCardRelationshipEnquiryResponseBody {

    @XmlElement(name = "NumberOfCards")
    protected String numberOfCards;
    @XmlElement(name = "BlockCustCardRelationShip")
    protected BlockCustCardRelationShip blockCustCardRelationShip;

    /**
     * Gets the value of the numberOfCards property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfCards() {
        return numberOfCards;
    }

    /**
     * Sets the value of the numberOfCards property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfCards(String value) {
        this.numberOfCards = value;
    }

    /**
     * Gets the value of the blockCustCardRelationShip property.
     * 
     * @return
     *     possible object is
     *     {@link BlockCustCardRelationShip }
     *     
     */
    public BlockCustCardRelationShip getBlockCustCardRelationShip() {
        return blockCustCardRelationShip;
    }

    /**
     * Sets the value of the blockCustCardRelationShip property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockCustCardRelationShip }
     *     
     */
    public void setBlockCustCardRelationShip(BlockCustCardRelationShip value) {
        this.blockCustCardRelationShip = value;
    }

}
