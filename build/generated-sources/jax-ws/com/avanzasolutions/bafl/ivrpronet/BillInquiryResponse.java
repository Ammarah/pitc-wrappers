
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillInquiryResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BillInquiryResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "billInquiryResult"
})
@XmlRootElement(name = "BillInquiryResponse")
public class BillInquiryResponse {

    @XmlElement(name = "BillInquiryResult")
    protected BillInquiryResponseTransaction billInquiryResult;

    /**
     * Gets the value of the billInquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link BillInquiryResponseTransaction }
     *     
     */
    public BillInquiryResponseTransaction getBillInquiryResult() {
        return billInquiryResult;
    }

    /**
     * Sets the value of the billInquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillInquiryResponseTransaction }
     *     
     */
    public void setBillInquiryResult(BillInquiryResponseTransaction value) {
        this.billInquiryResult = value;
    }

}
