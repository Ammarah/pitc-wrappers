
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WalletCardPinGenerationResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}WalletCardPinGenerationResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "walletCardPinGenerationResult"
})
@XmlRootElement(name = "WalletCardPinGenerationResponse")
public class WalletCardPinGenerationResponse {

    @XmlElement(name = "WalletCardPinGenerationResult")
    protected WalletCardPinGenerationResponseTransaction walletCardPinGenerationResult;

    /**
     * Gets the value of the walletCardPinGenerationResult property.
     * 
     * @return
     *     possible object is
     *     {@link WalletCardPinGenerationResponseTransaction }
     *     
     */
    public WalletCardPinGenerationResponseTransaction getWalletCardPinGenerationResult() {
        return walletCardPinGenerationResult;
    }

    /**
     * Sets the value of the walletCardPinGenerationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WalletCardPinGenerationResponseTransaction }
     *     
     */
    public void setWalletCardPinGenerationResult(WalletCardPinGenerationResponseTransaction value) {
        this.walletCardPinGenerationResult = value;
    }

}
