
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardDetailResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}CardDetailResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cardDetailResult"
})
@XmlRootElement(name = "CardDetailResponse")
public class CardDetailResponse {

    @XmlElement(name = "CardDetailResult")
    protected CardDetailResponseTransaction cardDetailResult;

    /**
     * Gets the value of the cardDetailResult property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetailResponseTransaction }
     *     
     */
    public CardDetailResponseTransaction getCardDetailResult() {
        return cardDetailResult;
    }

    /**
     * Sets the value of the cardDetailResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetailResponseTransaction }
     *     
     */
    public void setCardDetailResult(CardDetailResponseTransaction value) {
        this.cardDetailResult = value;
    }

}
