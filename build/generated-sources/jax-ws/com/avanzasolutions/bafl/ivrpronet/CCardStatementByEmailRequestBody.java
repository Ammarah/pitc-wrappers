
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CCardStatementByEmailRequestBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CCardStatementByEmailRequestBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardExpiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardBlocking" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequestStatementmonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequestStatementDelivery" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementDeliveryMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CCardStatementByEmailRequestBody", propOrder = {
    "cardNo",
    "cardExpiry",
    "cardBlocking",
    "requestStatementmonth",
    "requestStatementDelivery",
    "statementDeliveryMode",
    "email"
})
public class CCardStatementByEmailRequestBody {

    @XmlElement(name = "CardNo")
    protected String cardNo;
    @XmlElement(name = "CardExpiry")
    protected String cardExpiry;
    @XmlElement(name = "CardBlocking")
    protected String cardBlocking;
    @XmlElement(name = "RequestStatementmonth")
    protected String requestStatementmonth;
    @XmlElement(name = "RequestStatementDelivery")
    protected String requestStatementDelivery;
    @XmlElement(name = "StatementDeliveryMode")
    protected String statementDeliveryMode;
    @XmlElement(name = "Email")
    protected String email;

    /**
     * Gets the value of the cardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * Sets the value of the cardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNo(String value) {
        this.cardNo = value;
    }

    /**
     * Gets the value of the cardExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardExpiry() {
        return cardExpiry;
    }

    /**
     * Sets the value of the cardExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardExpiry(String value) {
        this.cardExpiry = value;
    }

    /**
     * Gets the value of the cardBlocking property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardBlocking() {
        return cardBlocking;
    }

    /**
     * Sets the value of the cardBlocking property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardBlocking(String value) {
        this.cardBlocking = value;
    }

    /**
     * Gets the value of the requestStatementmonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestStatementmonth() {
        return requestStatementmonth;
    }

    /**
     * Sets the value of the requestStatementmonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestStatementmonth(String value) {
        this.requestStatementmonth = value;
    }

    /**
     * Gets the value of the requestStatementDelivery property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestStatementDelivery() {
        return requestStatementDelivery;
    }

    /**
     * Sets the value of the requestStatementDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestStatementDelivery(String value) {
        this.requestStatementDelivery = value;
    }

    /**
     * Gets the value of the statementDeliveryMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementDeliveryMode() {
        return statementDeliveryMode;
    }

    /**
     * Sets the value of the statementDeliveryMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementDeliveryMode(String value) {
        this.statementDeliveryMode = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

}
