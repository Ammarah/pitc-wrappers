
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MiniStatementBlock complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MiniStatementBlock">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MiniTranDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiniTranDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiniCDSign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiniTranAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MiniStatementBlock", propOrder = {
    "miniTranDate",
    "miniTranDescription",
    "miniCDSign",
    "miniTranAmount"
})
public class MiniStatementBlock {

    @XmlElement(name = "MiniTranDate")
    protected String miniTranDate;
    @XmlElement(name = "MiniTranDescription")
    protected String miniTranDescription;
    @XmlElement(name = "MiniCDSign")
    protected String miniCDSign;
    @XmlElement(name = "MiniTranAmount")
    protected String miniTranAmount;

    /**
     * Gets the value of the miniTranDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiniTranDate() {
        return miniTranDate;
    }

    /**
     * Sets the value of the miniTranDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiniTranDate(String value) {
        this.miniTranDate = value;
    }

    /**
     * Gets the value of the miniTranDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiniTranDescription() {
        return miniTranDescription;
    }

    /**
     * Sets the value of the miniTranDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiniTranDescription(String value) {
        this.miniTranDescription = value;
    }

    /**
     * Gets the value of the miniCDSign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiniCDSign() {
        return miniCDSign;
    }

    /**
     * Sets the value of the miniCDSign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiniCDSign(String value) {
        this.miniCDSign = value;
    }

    /**
     * Gets the value of the miniTranAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiniTranAmount() {
        return miniTranAmount;
    }

    /**
     * Sets the value of the miniTranAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiniTranAmount(String value) {
        this.miniTranAmount = value;
    }

}
