
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockCustCardRelationShip complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockCustCardRelationShip">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustAccountCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockCustCardRelationShip", propOrder = {
    "custAccountNumber",
    "custAccountType",
    "custAccountCurrency"
})
public class BlockCustCardRelationShip {

    @XmlElement(name = "CustAccountNumber")
    protected String custAccountNumber;
    @XmlElement(name = "CustAccountType")
    protected String custAccountType;
    @XmlElement(name = "CustAccountCurrency")
    protected String custAccountCurrency;

    /**
     * Gets the value of the custAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustAccountNumber() {
        return custAccountNumber;
    }

    /**
     * Sets the value of the custAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustAccountNumber(String value) {
        this.custAccountNumber = value;
    }

    /**
     * Gets the value of the custAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustAccountType() {
        return custAccountType;
    }

    /**
     * Sets the value of the custAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustAccountType(String value) {
        this.custAccountType = value;
    }

    /**
     * Gets the value of the custAccountCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustAccountCurrency() {
        return custAccountCurrency;
    }

    /**
     * Sets the value of the custAccountCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustAccountCurrency(String value) {
        this.custAccountCurrency = value;
    }

}
