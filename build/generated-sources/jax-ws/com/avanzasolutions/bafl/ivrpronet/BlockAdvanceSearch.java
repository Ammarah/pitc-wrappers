
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockAdvanceSearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockAdvanceSearch">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchCNIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchCustName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchAccTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockAdvanceSearch", propOrder = {
    "searchCNIC",
    "searchBranchCode",
    "searchCustName",
    "searchAccTitle"
})
public class BlockAdvanceSearch {

    @XmlElement(name = "SearchCNIC")
    protected String searchCNIC;
    @XmlElement(name = "SearchBranchCode")
    protected String searchBranchCode;
    @XmlElement(name = "SearchCustName")
    protected String searchCustName;
    @XmlElement(name = "SearchAccTitle")
    protected String searchAccTitle;

    /**
     * Gets the value of the searchCNIC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchCNIC() {
        return searchCNIC;
    }

    /**
     * Sets the value of the searchCNIC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchCNIC(String value) {
        this.searchCNIC = value;
    }

    /**
     * Gets the value of the searchBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchBranchCode() {
        return searchBranchCode;
    }

    /**
     * Sets the value of the searchBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchBranchCode(String value) {
        this.searchBranchCode = value;
    }

    /**
     * Gets the value of the searchCustName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchCustName() {
        return searchCustName;
    }

    /**
     * Sets the value of the searchCustName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchCustName(String value) {
        this.searchCustName = value;
    }

    /**
     * Gets the value of the searchAccTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchAccTitle() {
        return searchAccTitle;
    }

    /**
     * Sets the value of the searchAccTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchAccTitle(String value) {
        this.searchAccTitle = value;
    }

}
