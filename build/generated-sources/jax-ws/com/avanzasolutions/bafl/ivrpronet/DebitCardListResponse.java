
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitCardListResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}DebitCardListResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "debitCardListResult"
})
@XmlRootElement(name = "DebitCardListResponse")
public class DebitCardListResponse {

    @XmlElement(name = "DebitCardListResult")
    protected DebitCardListResponseTransaction debitCardListResult;

    /**
     * Gets the value of the debitCardListResult property.
     * 
     * @return
     *     possible object is
     *     {@link DebitCardListResponseTransaction }
     *     
     */
    public DebitCardListResponseTransaction getDebitCardListResult() {
        return debitCardListResult;
    }

    /**
     * Sets the value of the debitCardListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitCardListResponseTransaction }
     *     
     */
    public void setDebitCardListResult(DebitCardListResponseTransaction value) {
        this.debitCardListResult = value;
    }

}
