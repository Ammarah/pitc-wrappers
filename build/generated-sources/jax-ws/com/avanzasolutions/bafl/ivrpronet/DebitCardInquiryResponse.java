
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitCardInquiryResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}DebitCardInquiryResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "debitCardInquiryResult"
})
@XmlRootElement(name = "DebitCardInquiryResponse")
public class DebitCardInquiryResponse {

    @XmlElement(name = "DebitCardInquiryResult")
    protected DebitCardInquiryResponseTransaction debitCardInquiryResult;

    /**
     * Gets the value of the debitCardInquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link DebitCardInquiryResponseTransaction }
     *     
     */
    public DebitCardInquiryResponseTransaction getDebitCardInquiryResult() {
        return debitCardInquiryResult;
    }

    /**
     * Sets the value of the debitCardInquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitCardInquiryResponseTransaction }
     *     
     */
    public void setDebitCardInquiryResult(DebitCardInquiryResponseTransaction value) {
        this.debitCardInquiryResult = value;
    }

}
