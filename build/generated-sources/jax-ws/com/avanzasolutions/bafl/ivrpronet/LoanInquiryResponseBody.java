
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanInquiryResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanInquiryResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoanType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutstandingBal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberInstallmentsRemaining" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MonthlyInstallmentAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NextPaymentDueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanInquiryResponseBody", propOrder = {
    "loanType",
    "outstandingBal",
    "numberInstallmentsRemaining",
    "monthlyInstallmentAmount",
    "nextPaymentDueDate"
})
public class LoanInquiryResponseBody {

    @XmlElement(name = "LoanType")
    protected String loanType;
    @XmlElement(name = "OutstandingBal")
    protected String outstandingBal;
    @XmlElement(name = "NumberInstallmentsRemaining")
    protected String numberInstallmentsRemaining;
    @XmlElement(name = "MonthlyInstallmentAmount")
    protected String monthlyInstallmentAmount;
    @XmlElement(name = "NextPaymentDueDate")
    protected String nextPaymentDueDate;

    /**
     * Gets the value of the loanType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanType() {
        return loanType;
    }

    /**
     * Sets the value of the loanType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanType(String value) {
        this.loanType = value;
    }

    /**
     * Gets the value of the outstandingBal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutstandingBal() {
        return outstandingBal;
    }

    /**
     * Sets the value of the outstandingBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutstandingBal(String value) {
        this.outstandingBal = value;
    }

    /**
     * Gets the value of the numberInstallmentsRemaining property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberInstallmentsRemaining() {
        return numberInstallmentsRemaining;
    }

    /**
     * Sets the value of the numberInstallmentsRemaining property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberInstallmentsRemaining(String value) {
        this.numberInstallmentsRemaining = value;
    }

    /**
     * Gets the value of the monthlyInstallmentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthlyInstallmentAmount() {
        return monthlyInstallmentAmount;
    }

    /**
     * Sets the value of the monthlyInstallmentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthlyInstallmentAmount(String value) {
        this.monthlyInstallmentAmount = value;
    }

    /**
     * Gets the value of the nextPaymentDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextPaymentDueDate() {
        return nextPaymentDueDate;
    }

    /**
     * Sets the value of the nextPaymentDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextPaymentDueDate(String value) {
        this.nextPaymentDueDate = value;
    }

}
