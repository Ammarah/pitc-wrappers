
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnBlockGDCIServiceRequestBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnBlockGDCIServiceRequestBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebiCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DCType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnBlockGDCIServiceRequestBody", propOrder = {
    "debiCardNumber",
    "dcType"
})
public class UnBlockGDCIServiceRequestBody {

    @XmlElement(name = "DebiCardNumber")
    protected String debiCardNumber;
    @XmlElement(name = "DCType")
    protected String dcType;

    /**
     * Gets the value of the debiCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebiCardNumber() {
        return debiCardNumber;
    }

    /**
     * Sets the value of the debiCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebiCardNumber(String value) {
        this.debiCardNumber = value;
    }

    /**
     * Gets the value of the dcType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDCType() {
        return dcType;
    }

    /**
     * Sets the value of the dcType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDCType(String value) {
        this.dcType = value;
    }

}
