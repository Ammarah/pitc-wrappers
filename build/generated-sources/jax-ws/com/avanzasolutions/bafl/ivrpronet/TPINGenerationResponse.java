
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TPINGenerationResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}TPINGenerationResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tpinGenerationResult"
})
@XmlRootElement(name = "TPINGenerationResponse")
public class TPINGenerationResponse {

    @XmlElement(name = "TPINGenerationResult")
    protected TPINGenerationResponseTransaction tpinGenerationResult;

    /**
     * Gets the value of the tpinGenerationResult property.
     * 
     * @return
     *     possible object is
     *     {@link TPINGenerationResponseTransaction }
     *     
     */
    public TPINGenerationResponseTransaction getTPINGenerationResult() {
        return tpinGenerationResult;
    }

    /**
     * Sets the value of the tpinGenerationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPINGenerationResponseTransaction }
     *     
     */
    public void setTPINGenerationResult(TPINGenerationResponseTransaction value) {
        this.tpinGenerationResult = value;
    }

}
