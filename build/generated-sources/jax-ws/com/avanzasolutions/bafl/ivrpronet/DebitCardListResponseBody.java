
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DebitCardListResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DebitCardListResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitCardBlock" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}DebitCardBlock" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitCardListResponseBody", propOrder = {
    "debitCardBlock"
})
public class DebitCardListResponseBody {

    @XmlElement(name = "DebitCardBlock")
    protected DebitCardBlock debitCardBlock;

    /**
     * Gets the value of the debitCardBlock property.
     * 
     * @return
     *     possible object is
     *     {@link DebitCardBlock }
     *     
     */
    public DebitCardBlock getDebitCardBlock() {
        return debitCardBlock;
    }

    /**
     * Sets the value of the debitCardBlock property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitCardBlock }
     *     
     */
    public void setDebitCardBlock(DebitCardBlock value) {
        this.debitCardBlock = value;
    }

}
