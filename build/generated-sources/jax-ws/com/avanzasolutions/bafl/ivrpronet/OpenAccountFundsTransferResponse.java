
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OpenAccountFundsTransferResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}OpenAccountFundsTransferResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "openAccountFundsTransferResult"
})
@XmlRootElement(name = "OpenAccountFundsTransferResponse")
public class OpenAccountFundsTransferResponse {

    @XmlElement(name = "OpenAccountFundsTransferResult")
    protected OpenAccountFundsTransferResponseTransaction openAccountFundsTransferResult;

    /**
     * Gets the value of the openAccountFundsTransferResult property.
     * 
     * @return
     *     possible object is
     *     {@link OpenAccountFundsTransferResponseTransaction }
     *     
     */
    public OpenAccountFundsTransferResponseTransaction getOpenAccountFundsTransferResult() {
        return openAccountFundsTransferResult;
    }

    /**
     * Sets the value of the openAccountFundsTransferResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpenAccountFundsTransferResponseTransaction }
     *     
     */
    public void setOpenAccountFundsTransferResult(OpenAccountFundsTransferResponseTransaction value) {
        this.openAccountFundsTransferResult = value;
    }

}
