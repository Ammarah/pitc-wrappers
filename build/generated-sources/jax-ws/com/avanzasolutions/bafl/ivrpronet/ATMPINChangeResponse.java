
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ATMPINChangeResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}ATMPINChangeResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "atmpinChangeResult"
})
@XmlRootElement(name = "ATMPINChangeResponse")
public class ATMPINChangeResponse {

    @XmlElement(name = "ATMPINChangeResult")
    protected ATMPINChangeResponseTransaction atmpinChangeResult;

    /**
     * Gets the value of the atmpinChangeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ATMPINChangeResponseTransaction }
     *     
     */
    public ATMPINChangeResponseTransaction getATMPINChangeResult() {
        return atmpinChangeResult;
    }

    /**
     * Sets the value of the atmpinChangeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ATMPINChangeResponseTransaction }
     *     
     */
    public void setATMPINChangeResult(ATMPINChangeResponseTransaction value) {
        this.atmpinChangeResult = value;
    }

}
