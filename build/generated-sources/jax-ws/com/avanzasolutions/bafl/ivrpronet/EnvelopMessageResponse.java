
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EnvelopMessageResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}EnvelopMessageResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "envelopMessageResult"
})
@XmlRootElement(name = "EnvelopMessageResponse")
public class EnvelopMessageResponse {

    @XmlElement(name = "EnvelopMessageResult")
    protected EnvelopMessageResponseTransaction envelopMessageResult;

    /**
     * Gets the value of the envelopMessageResult property.
     * 
     * @return
     *     possible object is
     *     {@link EnvelopMessageResponseTransaction }
     *     
     */
    public EnvelopMessageResponseTransaction getEnvelopMessageResult() {
        return envelopMessageResult;
    }

    /**
     * Sets the value of the envelopMessageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvelopMessageResponseTransaction }
     *     
     */
    public void setEnvelopMessageResult(EnvelopMessageResponseTransaction value) {
        this.envelopMessageResult = value;
    }

}
