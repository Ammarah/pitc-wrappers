
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenericEformTrxnRequestBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericEformTrxnRequestBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Reserved1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved21" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved22" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved23" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved24" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved25" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved26" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved27" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved28" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved29" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved30" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved31" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved32" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved33" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved34" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved35" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reserved36" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericEformTrxnRequestBody", propOrder = {
    "reserved1",
    "reserved2",
    "reserved3",
    "reserved4",
    "reserved5",
    "reserved6",
    "reserved7",
    "reserved8",
    "reserved9",
    "reserved10",
    "reserved11",
    "reserved12",
    "reserved13",
    "reserved14",
    "reserved15",
    "reserved16",
    "reserved17",
    "reserved18",
    "reserved19",
    "reserved20",
    "reserved21",
    "reserved22",
    "reserved23",
    "reserved24",
    "reserved25",
    "reserved26",
    "reserved27",
    "reserved28",
    "reserved29",
    "reserved30",
    "reserved31",
    "reserved32",
    "reserved33",
    "reserved34",
    "reserved35",
    "reserved36"
})
public class GenericEformTrxnRequestBody {

    @XmlElement(name = "Reserved1")
    protected String reserved1;
    @XmlElement(name = "Reserved2")
    protected String reserved2;
    @XmlElement(name = "Reserved3")
    protected String reserved3;
    @XmlElement(name = "Reserved4")
    protected String reserved4;
    @XmlElement(name = "Reserved5")
    protected String reserved5;
    @XmlElement(name = "Reserved6")
    protected String reserved6;
    @XmlElement(name = "Reserved7")
    protected String reserved7;
    @XmlElement(name = "Reserved8")
    protected String reserved8;
    @XmlElement(name = "Reserved9")
    protected String reserved9;
    @XmlElement(name = "Reserved10")
    protected String reserved10;
    @XmlElement(name = "Reserved11")
    protected String reserved11;
    @XmlElement(name = "Reserved12")
    protected String reserved12;
    @XmlElement(name = "Reserved13")
    protected String reserved13;
    @XmlElement(name = "Reserved14")
    protected String reserved14;
    @XmlElement(name = "Reserved15")
    protected String reserved15;
    @XmlElement(name = "Reserved16")
    protected String reserved16;
    @XmlElement(name = "Reserved17")
    protected String reserved17;
    @XmlElement(name = "Reserved18")
    protected String reserved18;
    @XmlElement(name = "Reserved19")
    protected String reserved19;
    @XmlElement(name = "Reserved20")
    protected String reserved20;
    @XmlElement(name = "Reserved21")
    protected String reserved21;
    @XmlElement(name = "Reserved22")
    protected String reserved22;
    @XmlElement(name = "Reserved23")
    protected String reserved23;
    @XmlElement(name = "Reserved24")
    protected String reserved24;
    @XmlElement(name = "Reserved25")
    protected String reserved25;
    @XmlElement(name = "Reserved26")
    protected String reserved26;
    @XmlElement(name = "Reserved27")
    protected String reserved27;
    @XmlElement(name = "Reserved28")
    protected String reserved28;
    @XmlElement(name = "Reserved29")
    protected String reserved29;
    @XmlElement(name = "Reserved30")
    protected String reserved30;
    @XmlElement(name = "Reserved31")
    protected String reserved31;
    @XmlElement(name = "Reserved32")
    protected String reserved32;
    @XmlElement(name = "Reserved33")
    protected String reserved33;
    @XmlElement(name = "Reserved34")
    protected String reserved34;
    @XmlElement(name = "Reserved35")
    protected String reserved35;
    @XmlElement(name = "Reserved36")
    protected String reserved36;

    /**
     * Gets the value of the reserved1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved1() {
        return reserved1;
    }

    /**
     * Sets the value of the reserved1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved1(String value) {
        this.reserved1 = value;
    }

    /**
     * Gets the value of the reserved2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved2() {
        return reserved2;
    }

    /**
     * Sets the value of the reserved2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved2(String value) {
        this.reserved2 = value;
    }

    /**
     * Gets the value of the reserved3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved3() {
        return reserved3;
    }

    /**
     * Sets the value of the reserved3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved3(String value) {
        this.reserved3 = value;
    }

    /**
     * Gets the value of the reserved4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved4() {
        return reserved4;
    }

    /**
     * Sets the value of the reserved4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved4(String value) {
        this.reserved4 = value;
    }

    /**
     * Gets the value of the reserved5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved5() {
        return reserved5;
    }

    /**
     * Sets the value of the reserved5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved5(String value) {
        this.reserved5 = value;
    }

    /**
     * Gets the value of the reserved6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved6() {
        return reserved6;
    }

    /**
     * Sets the value of the reserved6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved6(String value) {
        this.reserved6 = value;
    }

    /**
     * Gets the value of the reserved7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved7() {
        return reserved7;
    }

    /**
     * Sets the value of the reserved7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved7(String value) {
        this.reserved7 = value;
    }

    /**
     * Gets the value of the reserved8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved8() {
        return reserved8;
    }

    /**
     * Sets the value of the reserved8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved8(String value) {
        this.reserved8 = value;
    }

    /**
     * Gets the value of the reserved9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved9() {
        return reserved9;
    }

    /**
     * Sets the value of the reserved9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved9(String value) {
        this.reserved9 = value;
    }

    /**
     * Gets the value of the reserved10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved10() {
        return reserved10;
    }

    /**
     * Sets the value of the reserved10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved10(String value) {
        this.reserved10 = value;
    }

    /**
     * Gets the value of the reserved11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved11() {
        return reserved11;
    }

    /**
     * Sets the value of the reserved11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved11(String value) {
        this.reserved11 = value;
    }

    /**
     * Gets the value of the reserved12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved12() {
        return reserved12;
    }

    /**
     * Sets the value of the reserved12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved12(String value) {
        this.reserved12 = value;
    }

    /**
     * Gets the value of the reserved13 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved13() {
        return reserved13;
    }

    /**
     * Sets the value of the reserved13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved13(String value) {
        this.reserved13 = value;
    }

    /**
     * Gets the value of the reserved14 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved14() {
        return reserved14;
    }

    /**
     * Sets the value of the reserved14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved14(String value) {
        this.reserved14 = value;
    }

    /**
     * Gets the value of the reserved15 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved15() {
        return reserved15;
    }

    /**
     * Sets the value of the reserved15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved15(String value) {
        this.reserved15 = value;
    }

    /**
     * Gets the value of the reserved16 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved16() {
        return reserved16;
    }

    /**
     * Sets the value of the reserved16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved16(String value) {
        this.reserved16 = value;
    }

    /**
     * Gets the value of the reserved17 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved17() {
        return reserved17;
    }

    /**
     * Sets the value of the reserved17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved17(String value) {
        this.reserved17 = value;
    }

    /**
     * Gets the value of the reserved18 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved18() {
        return reserved18;
    }

    /**
     * Sets the value of the reserved18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved18(String value) {
        this.reserved18 = value;
    }

    /**
     * Gets the value of the reserved19 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved19() {
        return reserved19;
    }

    /**
     * Sets the value of the reserved19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved19(String value) {
        this.reserved19 = value;
    }

    /**
     * Gets the value of the reserved20 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved20() {
        return reserved20;
    }

    /**
     * Sets the value of the reserved20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved20(String value) {
        this.reserved20 = value;
    }

    /**
     * Gets the value of the reserved21 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved21() {
        return reserved21;
    }

    /**
     * Sets the value of the reserved21 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved21(String value) {
        this.reserved21 = value;
    }

    /**
     * Gets the value of the reserved22 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved22() {
        return reserved22;
    }

    /**
     * Sets the value of the reserved22 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved22(String value) {
        this.reserved22 = value;
    }

    /**
     * Gets the value of the reserved23 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved23() {
        return reserved23;
    }

    /**
     * Sets the value of the reserved23 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved23(String value) {
        this.reserved23 = value;
    }

    /**
     * Gets the value of the reserved24 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved24() {
        return reserved24;
    }

    /**
     * Sets the value of the reserved24 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved24(String value) {
        this.reserved24 = value;
    }

    /**
     * Gets the value of the reserved25 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved25() {
        return reserved25;
    }

    /**
     * Sets the value of the reserved25 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved25(String value) {
        this.reserved25 = value;
    }

    /**
     * Gets the value of the reserved26 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved26() {
        return reserved26;
    }

    /**
     * Sets the value of the reserved26 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved26(String value) {
        this.reserved26 = value;
    }

    /**
     * Gets the value of the reserved27 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved27() {
        return reserved27;
    }

    /**
     * Sets the value of the reserved27 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved27(String value) {
        this.reserved27 = value;
    }

    /**
     * Gets the value of the reserved28 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved28() {
        return reserved28;
    }

    /**
     * Sets the value of the reserved28 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved28(String value) {
        this.reserved28 = value;
    }

    /**
     * Gets the value of the reserved29 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved29() {
        return reserved29;
    }

    /**
     * Sets the value of the reserved29 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved29(String value) {
        this.reserved29 = value;
    }

    /**
     * Gets the value of the reserved30 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved30() {
        return reserved30;
    }

    /**
     * Sets the value of the reserved30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved30(String value) {
        this.reserved30 = value;
    }

    /**
     * Gets the value of the reserved31 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved31() {
        return reserved31;
    }

    /**
     * Sets the value of the reserved31 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved31(String value) {
        this.reserved31 = value;
    }

    /**
     * Gets the value of the reserved32 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved32() {
        return reserved32;
    }

    /**
     * Sets the value of the reserved32 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved32(String value) {
        this.reserved32 = value;
    }

    /**
     * Gets the value of the reserved33 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved33() {
        return reserved33;
    }

    /**
     * Sets the value of the reserved33 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved33(String value) {
        this.reserved33 = value;
    }

    /**
     * Gets the value of the reserved34 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved34() {
        return reserved34;
    }

    /**
     * Sets the value of the reserved34 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved34(String value) {
        this.reserved34 = value;
    }

    /**
     * Gets the value of the reserved35 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved35() {
        return reserved35;
    }

    /**
     * Sets the value of the reserved35 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved35(String value) {
        this.reserved35 = value;
    }

    /**
     * Gets the value of the reserved36 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserved36() {
        return reserved36;
    }

    /**
     * Sets the value of the reserved36 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserved36(String value) {
        this.reserved36 = value;
    }

}
