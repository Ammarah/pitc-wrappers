
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CCAccountInquiryWithCustomerInfoResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}CCAccountInquiryWithCustomerInfoResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ccAccountInquiryWithCustomerInfoResult"
})
@XmlRootElement(name = "CCAccountInquiryWithCustomerInfoResponse")
public class CCAccountInquiryWithCustomerInfoResponse {

    @XmlElement(name = "CCAccountInquiryWithCustomerInfoResult")
    protected CCAccountInquiryWithCustomerInfoResponseTransaction ccAccountInquiryWithCustomerInfoResult;

    /**
     * Gets the value of the ccAccountInquiryWithCustomerInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link CCAccountInquiryWithCustomerInfoResponseTransaction }
     *     
     */
    public CCAccountInquiryWithCustomerInfoResponseTransaction getCCAccountInquiryWithCustomerInfoResult() {
        return ccAccountInquiryWithCustomerInfoResult;
    }

    /**
     * Sets the value of the ccAccountInquiryWithCustomerInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCAccountInquiryWithCustomerInfoResponseTransaction }
     *     
     */
    public void setCCAccountInquiryWithCustomerInfoResult(CCAccountInquiryWithCustomerInfoResponseTransaction value) {
        this.ccAccountInquiryWithCustomerInfoResult = value;
    }

}
