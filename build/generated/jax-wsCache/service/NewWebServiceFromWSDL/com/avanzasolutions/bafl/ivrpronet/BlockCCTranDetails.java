
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockCCTranDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockCCTranDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PostDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillingAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranCurrMinorUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockCCTranDetails", propOrder = {
    "postDate",
    "tranDate",
    "tranCode",
    "tranDesc",
    "billingAmt",
    "tranCurrencyCode",
    "tranCurrMinorUnits",
    "tranAmt",
    "tranStatus"
})
public class BlockCCTranDetails {

    @XmlElement(name = "PostDate")
    protected String postDate;
    @XmlElement(name = "TranDate")
    protected String tranDate;
    @XmlElement(name = "TranCode")
    protected String tranCode;
    @XmlElement(name = "TranDesc")
    protected String tranDesc;
    @XmlElement(name = "BillingAmt")
    protected String billingAmt;
    @XmlElement(name = "TranCurrencyCode")
    protected String tranCurrencyCode;
    @XmlElement(name = "TranCurrMinorUnits")
    protected String tranCurrMinorUnits;
    @XmlElement(name = "TranAmt")
    protected String tranAmt;
    @XmlElement(name = "TranStatus")
    protected String tranStatus;

    /**
     * Gets the value of the postDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostDate() {
        return postDate;
    }

    /**
     * Sets the value of the postDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostDate(String value) {
        this.postDate = value;
    }

    /**
     * Gets the value of the tranDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranDate() {
        return tranDate;
    }

    /**
     * Sets the value of the tranDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranDate(String value) {
        this.tranDate = value;
    }

    /**
     * Gets the value of the tranCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranCode() {
        return tranCode;
    }

    /**
     * Sets the value of the tranCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranCode(String value) {
        this.tranCode = value;
    }

    /**
     * Gets the value of the tranDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranDesc() {
        return tranDesc;
    }

    /**
     * Sets the value of the tranDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranDesc(String value) {
        this.tranDesc = value;
    }

    /**
     * Gets the value of the billingAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAmt() {
        return billingAmt;
    }

    /**
     * Sets the value of the billingAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAmt(String value) {
        this.billingAmt = value;
    }

    /**
     * Gets the value of the tranCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranCurrencyCode() {
        return tranCurrencyCode;
    }

    /**
     * Sets the value of the tranCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranCurrencyCode(String value) {
        this.tranCurrencyCode = value;
    }

    /**
     * Gets the value of the tranCurrMinorUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranCurrMinorUnits() {
        return tranCurrMinorUnits;
    }

    /**
     * Sets the value of the tranCurrMinorUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranCurrMinorUnits(String value) {
        this.tranCurrMinorUnits = value;
    }

    /**
     * Gets the value of the tranAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranAmt() {
        return tranAmt;
    }

    /**
     * Sets the value of the tranAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranAmt(String value) {
        this.tranAmt = value;
    }

    /**
     * Gets the value of the tranStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranStatus() {
        return tranStatus;
    }

    /**
     * Sets the value of the tranStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranStatus(String value) {
        this.tranStatus = value;
    }

}
