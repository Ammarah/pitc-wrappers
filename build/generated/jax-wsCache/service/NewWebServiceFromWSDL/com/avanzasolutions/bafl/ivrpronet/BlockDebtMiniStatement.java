
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockDebtMiniStatement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockDebtMiniStatement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitTransactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitTransactionDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitTransactionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitTransactionTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAccountCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAmount1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAccountCurrency1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAmount2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAccountCurrency2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitAmount3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitCurrency3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitMerchantType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitMerchantName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitIMDCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockDebtMiniStatement", propOrder = {
    "debitTransactionType",
    "debitTransactionDescription",
    "debitTransactionDate",
    "debitTransactionTime",
    "debitAccountNo",
    "debitAccountType",
    "debitAccountCurrency",
    "debitAmount1",
    "debitAccountCurrency1",
    "debitAmount2",
    "debitAccountCurrency2",
    "debitAmount3",
    "debitCurrency3",
    "debitMerchantType",
    "debitMerchantName",
    "debitIMDCode"
})
public class BlockDebtMiniStatement {

    @XmlElement(name = "DebitTransactionType")
    protected String debitTransactionType;
    @XmlElement(name = "DebitTransactionDescription")
    protected String debitTransactionDescription;
    @XmlElement(name = "DebitTransactionDate")
    protected String debitTransactionDate;
    @XmlElement(name = "DebitTransactionTime")
    protected String debitTransactionTime;
    @XmlElement(name = "DebitAccountNo")
    protected String debitAccountNo;
    @XmlElement(name = "DebitAccountType")
    protected String debitAccountType;
    @XmlElement(name = "DebitAccountCurrency")
    protected String debitAccountCurrency;
    @XmlElement(name = "DebitAmount1")
    protected String debitAmount1;
    @XmlElement(name = "DebitAccountCurrency1")
    protected String debitAccountCurrency1;
    @XmlElement(name = "DebitAmount2")
    protected String debitAmount2;
    @XmlElement(name = "DebitAccountCurrency2")
    protected String debitAccountCurrency2;
    @XmlElement(name = "DebitAmount3")
    protected String debitAmount3;
    @XmlElement(name = "DebitCurrency3")
    protected String debitCurrency3;
    @XmlElement(name = "DebitMerchantType")
    protected String debitMerchantType;
    @XmlElement(name = "DebitMerchantName")
    protected String debitMerchantName;
    @XmlElement(name = "DebitIMDCode")
    protected String debitIMDCode;

    /**
     * Gets the value of the debitTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitTransactionType() {
        return debitTransactionType;
    }

    /**
     * Sets the value of the debitTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitTransactionType(String value) {
        this.debitTransactionType = value;
    }

    /**
     * Gets the value of the debitTransactionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitTransactionDescription() {
        return debitTransactionDescription;
    }

    /**
     * Sets the value of the debitTransactionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitTransactionDescription(String value) {
        this.debitTransactionDescription = value;
    }

    /**
     * Gets the value of the debitTransactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitTransactionDate() {
        return debitTransactionDate;
    }

    /**
     * Sets the value of the debitTransactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitTransactionDate(String value) {
        this.debitTransactionDate = value;
    }

    /**
     * Gets the value of the debitTransactionTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitTransactionTime() {
        return debitTransactionTime;
    }

    /**
     * Sets the value of the debitTransactionTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitTransactionTime(String value) {
        this.debitTransactionTime = value;
    }

    /**
     * Gets the value of the debitAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAccountNo() {
        return debitAccountNo;
    }

    /**
     * Sets the value of the debitAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAccountNo(String value) {
        this.debitAccountNo = value;
    }

    /**
     * Gets the value of the debitAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAccountType() {
        return debitAccountType;
    }

    /**
     * Sets the value of the debitAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAccountType(String value) {
        this.debitAccountType = value;
    }

    /**
     * Gets the value of the debitAccountCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAccountCurrency() {
        return debitAccountCurrency;
    }

    /**
     * Sets the value of the debitAccountCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAccountCurrency(String value) {
        this.debitAccountCurrency = value;
    }

    /**
     * Gets the value of the debitAmount1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAmount1() {
        return debitAmount1;
    }

    /**
     * Sets the value of the debitAmount1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAmount1(String value) {
        this.debitAmount1 = value;
    }

    /**
     * Gets the value of the debitAccountCurrency1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAccountCurrency1() {
        return debitAccountCurrency1;
    }

    /**
     * Sets the value of the debitAccountCurrency1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAccountCurrency1(String value) {
        this.debitAccountCurrency1 = value;
    }

    /**
     * Gets the value of the debitAmount2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAmount2() {
        return debitAmount2;
    }

    /**
     * Sets the value of the debitAmount2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAmount2(String value) {
        this.debitAmount2 = value;
    }

    /**
     * Gets the value of the debitAccountCurrency2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAccountCurrency2() {
        return debitAccountCurrency2;
    }

    /**
     * Sets the value of the debitAccountCurrency2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAccountCurrency2(String value) {
        this.debitAccountCurrency2 = value;
    }

    /**
     * Gets the value of the debitAmount3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitAmount3() {
        return debitAmount3;
    }

    /**
     * Sets the value of the debitAmount3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitAmount3(String value) {
        this.debitAmount3 = value;
    }

    /**
     * Gets the value of the debitCurrency3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitCurrency3() {
        return debitCurrency3;
    }

    /**
     * Sets the value of the debitCurrency3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitCurrency3(String value) {
        this.debitCurrency3 = value;
    }

    /**
     * Gets the value of the debitMerchantType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitMerchantType() {
        return debitMerchantType;
    }

    /**
     * Sets the value of the debitMerchantType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitMerchantType(String value) {
        this.debitMerchantType = value;
    }

    /**
     * Gets the value of the debitMerchantName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitMerchantName() {
        return debitMerchantName;
    }

    /**
     * Sets the value of the debitMerchantName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitMerchantName(String value) {
        this.debitMerchantName = value;
    }

    /**
     * Gets the value of the debitIMDCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitIMDCode() {
        return debitIMDCode;
    }

    /**
     * Sets the value of the debitIMDCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitIMDCode(String value) {
        this.debitIMDCode = value;
    }

}
