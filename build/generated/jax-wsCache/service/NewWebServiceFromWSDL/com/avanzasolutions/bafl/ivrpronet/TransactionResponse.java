
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}TransactionResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionResult"
})
@XmlRootElement(name = "TransactionResponse")
public class TransactionResponse {

    @XmlElement(name = "TransactionResult")
    protected TransactionResponseTransaction transactionResult;

    /**
     * Gets the value of the transactionResult property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionResponseTransaction }
     *     
     */
    public TransactionResponseTransaction getTransactionResult() {
        return transactionResult;
    }

    /**
     * Sets the value of the transactionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionResponseTransaction }
     *     
     */
    public void setTransactionResult(TransactionResponseTransaction value) {
        this.transactionResult = value;
    }

}
