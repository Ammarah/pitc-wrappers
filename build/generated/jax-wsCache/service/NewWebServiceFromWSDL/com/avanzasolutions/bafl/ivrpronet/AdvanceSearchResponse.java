
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdvanceSearchResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}AdvanceSearchResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "advanceSearchResult"
})
@XmlRootElement(name = "AdvanceSearchResponse")
public class AdvanceSearchResponse {

    @XmlElement(name = "AdvanceSearchResult")
    protected AdvanceSearchResponseTransaction advanceSearchResult;

    /**
     * Gets the value of the advanceSearchResult property.
     * 
     * @return
     *     possible object is
     *     {@link AdvanceSearchResponseTransaction }
     *     
     */
    public AdvanceSearchResponseTransaction getAdvanceSearchResult() {
        return advanceSearchResult;
    }

    /**
     * Sets the value of the advanceSearchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvanceSearchResponseTransaction }
     *     
     */
    public void setAdvanceSearchResult(AdvanceSearchResponseTransaction value) {
        this.advanceSearchResult = value;
    }

}
