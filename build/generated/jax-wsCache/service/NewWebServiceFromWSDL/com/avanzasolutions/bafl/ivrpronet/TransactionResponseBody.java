
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PrevBal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MinPayDue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalCashAdvLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailCashAdvLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutstandingBal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RewardPointsEarned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RewardPointsRedeemed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RewardsPointAvail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailCreditLmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditLmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastPayAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastPayDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalCurrentSpending" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillingCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlasticCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeewaiverSpending" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionResponseBody", propOrder = {
    "prevBal",
    "dueDate",
    "minPayDue",
    "totalCashAdvLimit",
    "availCashAdvLimit",
    "outstandingBal",
    "rewardPointsEarned",
    "rewardPointsRedeemed",
    "rewardsPointAvail",
    "availCreditLmt",
    "creditLmt",
    "lastPayAmt",
    "lastPayDate",
    "totalCurrentSpending",
    "billingCurrencyCode",
    "statusCode",
    "plasticCode",
    "feewaiverSpending"
})
public class TransactionResponseBody {

    @XmlElement(name = "PrevBal")
    protected String prevBal;
    @XmlElement(name = "DueDate")
    protected String dueDate;
    @XmlElement(name = "MinPayDue")
    protected String minPayDue;
    @XmlElement(name = "TotalCashAdvLimit")
    protected String totalCashAdvLimit;
    @XmlElement(name = "AvailCashAdvLimit")
    protected String availCashAdvLimit;
    @XmlElement(name = "OutstandingBal")
    protected String outstandingBal;
    @XmlElement(name = "RewardPointsEarned")
    protected String rewardPointsEarned;
    @XmlElement(name = "RewardPointsRedeemed")
    protected String rewardPointsRedeemed;
    @XmlElement(name = "RewardsPointAvail")
    protected String rewardsPointAvail;
    @XmlElement(name = "AvailCreditLmt")
    protected String availCreditLmt;
    @XmlElement(name = "CreditLmt")
    protected String creditLmt;
    @XmlElement(name = "LastPayAmt")
    protected String lastPayAmt;
    @XmlElement(name = "LastPayDate")
    protected String lastPayDate;
    @XmlElement(name = "TotalCurrentSpending")
    protected String totalCurrentSpending;
    @XmlElement(name = "BillingCurrencyCode")
    protected String billingCurrencyCode;
    @XmlElement(name = "StatusCode")
    protected String statusCode;
    @XmlElement(name = "PlasticCode")
    protected String plasticCode;
    @XmlElement(name = "FeewaiverSpending")
    protected String feewaiverSpending;

    /**
     * Gets the value of the prevBal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevBal() {
        return prevBal;
    }

    /**
     * Sets the value of the prevBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevBal(String value) {
        this.prevBal = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueDate(String value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the minPayDue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinPayDue() {
        return minPayDue;
    }

    /**
     * Sets the value of the minPayDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinPayDue(String value) {
        this.minPayDue = value;
    }

    /**
     * Gets the value of the totalCashAdvLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalCashAdvLimit() {
        return totalCashAdvLimit;
    }

    /**
     * Sets the value of the totalCashAdvLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalCashAdvLimit(String value) {
        this.totalCashAdvLimit = value;
    }

    /**
     * Gets the value of the availCashAdvLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailCashAdvLimit() {
        return availCashAdvLimit;
    }

    /**
     * Sets the value of the availCashAdvLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailCashAdvLimit(String value) {
        this.availCashAdvLimit = value;
    }

    /**
     * Gets the value of the outstandingBal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutstandingBal() {
        return outstandingBal;
    }

    /**
     * Sets the value of the outstandingBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutstandingBal(String value) {
        this.outstandingBal = value;
    }

    /**
     * Gets the value of the rewardPointsEarned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRewardPointsEarned() {
        return rewardPointsEarned;
    }

    /**
     * Sets the value of the rewardPointsEarned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRewardPointsEarned(String value) {
        this.rewardPointsEarned = value;
    }

    /**
     * Gets the value of the rewardPointsRedeemed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRewardPointsRedeemed() {
        return rewardPointsRedeemed;
    }

    /**
     * Sets the value of the rewardPointsRedeemed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRewardPointsRedeemed(String value) {
        this.rewardPointsRedeemed = value;
    }

    /**
     * Gets the value of the rewardsPointAvail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRewardsPointAvail() {
        return rewardsPointAvail;
    }

    /**
     * Sets the value of the rewardsPointAvail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRewardsPointAvail(String value) {
        this.rewardsPointAvail = value;
    }

    /**
     * Gets the value of the availCreditLmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailCreditLmt() {
        return availCreditLmt;
    }

    /**
     * Sets the value of the availCreditLmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailCreditLmt(String value) {
        this.availCreditLmt = value;
    }

    /**
     * Gets the value of the creditLmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditLmt() {
        return creditLmt;
    }

    /**
     * Sets the value of the creditLmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditLmt(String value) {
        this.creditLmt = value;
    }

    /**
     * Gets the value of the lastPayAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastPayAmt() {
        return lastPayAmt;
    }

    /**
     * Sets the value of the lastPayAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastPayAmt(String value) {
        this.lastPayAmt = value;
    }

    /**
     * Gets the value of the lastPayDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastPayDate() {
        return lastPayDate;
    }

    /**
     * Sets the value of the lastPayDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastPayDate(String value) {
        this.lastPayDate = value;
    }

    /**
     * Gets the value of the totalCurrentSpending property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalCurrentSpending() {
        return totalCurrentSpending;
    }

    /**
     * Sets the value of the totalCurrentSpending property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalCurrentSpending(String value) {
        this.totalCurrentSpending = value;
    }

    /**
     * Gets the value of the billingCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCurrencyCode() {
        return billingCurrencyCode;
    }

    /**
     * Sets the value of the billingCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCurrencyCode(String value) {
        this.billingCurrencyCode = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the plasticCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlasticCode() {
        return plasticCode;
    }

    /**
     * Sets the value of the plasticCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlasticCode(String value) {
        this.plasticCode = value;
    }

    /**
     * Gets the value of the feewaiverSpending property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeewaiverSpending() {
        return feewaiverSpending;
    }

    /**
     * Sets the value of the feewaiverSpending property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeewaiverSpending(String value) {
        this.feewaiverSpending = value;
    }

}
