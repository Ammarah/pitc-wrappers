
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Request" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}CCAccountInquiryWithCustomerInfoRequestTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request"
})
@XmlRootElement(name = "CCAccountInquiryWithCustomerInfo")
public class CCAccountInquiryWithCustomerInfo {

    @XmlElement(name = "Request")
    protected CCAccountInquiryWithCustomerInfoRequestTransaction request;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link CCAccountInquiryWithCustomerInfoRequestTransaction }
     *     
     */
    public CCAccountInquiryWithCustomerInfoRequestTransaction getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCAccountInquiryWithCustomerInfoRequestTransaction }
     *     
     */
    public void setRequest(CCAccountInquiryWithCustomerInfoRequestTransaction value) {
        this.request = value;
    }

}
