
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SMSRegisteredUserListResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}SMSRegisteredUserListResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "smsRegisteredUserListResult"
})
@XmlRootElement(name = "SMSRegisteredUserListResponse")
public class SMSRegisteredUserListResponse {

    @XmlElement(name = "SMSRegisteredUserListResult")
    protected SMSRegisteredUserListResponseTransaction smsRegisteredUserListResult;

    /**
     * Gets the value of the smsRegisteredUserListResult property.
     * 
     * @return
     *     possible object is
     *     {@link SMSRegisteredUserListResponseTransaction }
     *     
     */
    public SMSRegisteredUserListResponseTransaction getSMSRegisteredUserListResult() {
        return smsRegisteredUserListResult;
    }

    /**
     * Sets the value of the smsRegisteredUserListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SMSRegisteredUserListResponseTransaction }
     *     
     */
    public void setSMSRegisteredUserListResult(SMSRegisteredUserListResponseTransaction value) {
        this.smsRegisteredUserListResult = value;
    }

}
