
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WalletCardChangeStatusResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}WalletCardChangeStatusResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "walletCardChangeStatusResult"
})
@XmlRootElement(name = "WalletCardChangeStatusResponse")
public class WalletCardChangeStatusResponse {

    @XmlElement(name = "WalletCardChangeStatusResult")
    protected WalletCardChangeStatusResponseTransaction walletCardChangeStatusResult;

    /**
     * Gets the value of the walletCardChangeStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link WalletCardChangeStatusResponseTransaction }
     *     
     */
    public WalletCardChangeStatusResponseTransaction getWalletCardChangeStatusResult() {
        return walletCardChangeStatusResult;
    }

    /**
     * Sets the value of the walletCardChangeStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WalletCardChangeStatusResponseTransaction }
     *     
     */
    public void setWalletCardChangeStatusResult(WalletCardChangeStatusResponseTransaction value) {
        this.walletCardChangeStatusResult = value;
    }

}
