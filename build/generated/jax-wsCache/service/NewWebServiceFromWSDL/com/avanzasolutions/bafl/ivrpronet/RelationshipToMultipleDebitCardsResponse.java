
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RelationshipToMultipleDebitCardsResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}RelationshipToMultipleDebitCardsResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "relationshipToMultipleDebitCardsResult"
})
@XmlRootElement(name = "RelationshipToMultipleDebitCardsResponse")
public class RelationshipToMultipleDebitCardsResponse {

    @XmlElement(name = "RelationshipToMultipleDebitCardsResult")
    protected RelationshipToMultipleDebitCardsResponseTransaction relationshipToMultipleDebitCardsResult;

    /**
     * Gets the value of the relationshipToMultipleDebitCardsResult property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipToMultipleDebitCardsResponseTransaction }
     *     
     */
    public RelationshipToMultipleDebitCardsResponseTransaction getRelationshipToMultipleDebitCardsResult() {
        return relationshipToMultipleDebitCardsResult;
    }

    /**
     * Sets the value of the relationshipToMultipleDebitCardsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipToMultipleDebitCardsResponseTransaction }
     *     
     */
    public void setRelationshipToMultipleDebitCardsResult(RelationshipToMultipleDebitCardsResponseTransaction value) {
        this.relationshipToMultipleDebitCardsResult = value;
    }

}
