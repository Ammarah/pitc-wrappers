
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BlockGDCIServiceResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockGDCIServiceResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "blockGDCIServiceResult"
})
@XmlRootElement(name = "BlockGDCIServiceResponse")
public class BlockGDCIServiceResponse {

    @XmlElement(name = "BlockGDCIServiceResult")
    protected BlockGDCIServiceResponseTransaction blockGDCIServiceResult;

    /**
     * Gets the value of the blockGDCIServiceResult property.
     * 
     * @return
     *     possible object is
     *     {@link BlockGDCIServiceResponseTransaction }
     *     
     */
    public BlockGDCIServiceResponseTransaction getBlockGDCIServiceResult() {
        return blockGDCIServiceResult;
    }

    /**
     * Sets the value of the blockGDCIServiceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockGDCIServiceResponseTransaction }
     *     
     */
    public void setBlockGDCIServiceResult(BlockGDCIServiceResponseTransaction value) {
        this.blockGDCIServiceResult = value;
    }

}
