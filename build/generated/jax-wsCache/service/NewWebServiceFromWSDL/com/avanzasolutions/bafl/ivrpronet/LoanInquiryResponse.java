
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoanInquiryResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}LoanInquiryResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loanInquiryResult"
})
@XmlRootElement(name = "LoanInquiryResponse")
public class LoanInquiryResponse {

    @XmlElement(name = "LoanInquiryResult")
    protected LoanInquiryResponseTransaction loanInquiryResult;

    /**
     * Gets the value of the loanInquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link LoanInquiryResponseTransaction }
     *     
     */
    public LoanInquiryResponseTransaction getLoanInquiryResult() {
        return loanInquiryResult;
    }

    /**
     * Sets the value of the loanInquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanInquiryResponseTransaction }
     *     
     */
    public void setLoanInquiryResult(LoanInquiryResponseTransaction value) {
        this.loanInquiryResult = value;
    }

}
