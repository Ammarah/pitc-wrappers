
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenericEformTrxnResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}GenericEformTrxnResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "genericEformTrxnResult"
})
@XmlRootElement(name = "GenericEformTrxnResponse")
public class GenericEformTrxnResponse {

    @XmlElement(name = "GenericEformTrxnResult")
    protected GenericEformTrxnResponseTransaction genericEformTrxnResult;

    /**
     * Gets the value of the genericEformTrxnResult property.
     * 
     * @return
     *     possible object is
     *     {@link GenericEformTrxnResponseTransaction }
     *     
     */
    public GenericEformTrxnResponseTransaction getGenericEformTrxnResult() {
        return genericEformTrxnResult;
    }

    /**
     * Sets the value of the genericEformTrxnResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericEformTrxnResponseTransaction }
     *     
     */
    public void setGenericEformTrxnResult(GenericEformTrxnResponseTransaction value) {
        this.genericEformTrxnResult = value;
    }

}
