
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CardDetailResponseTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CardDetailResponseTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}Header" minOccurs="0"/>
 *         &lt;element name="CardDetailResponseTransactionBody" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}CardDetailResponseTransactionBody" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardDetailResponseTransaction", propOrder = {
    "header",
    "cardDetailResponseTransactionBody"
})
public class CardDetailResponseTransaction {

    @XmlElement(name = "Header")
    protected Header header;
    @XmlElement(name = "CardDetailResponseTransactionBody")
    protected CardDetailResponseTransactionBody cardDetailResponseTransactionBody;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the cardDetailResponseTransactionBody property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetailResponseTransactionBody }
     *     
     */
    public CardDetailResponseTransactionBody getCardDetailResponseTransactionBody() {
        return cardDetailResponseTransactionBody;
    }

    /**
     * Sets the value of the cardDetailResponseTransactionBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetailResponseTransactionBody }
     *     
     */
    public void setCardDetailResponseTransactionBody(CardDetailResponseTransactionBody value) {
        this.cardDetailResponseTransactionBody = value;
    }

}
