
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StopPaymentResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}StopPaymentResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "stopPaymentResult"
})
@XmlRootElement(name = "StopPaymentResponse")
public class StopPaymentResponse {

    @XmlElement(name = "StopPaymentResult")
    protected StopPaymentResponseTransaction stopPaymentResult;

    /**
     * Gets the value of the stopPaymentResult property.
     * 
     * @return
     *     possible object is
     *     {@link StopPaymentResponseTransaction }
     *     
     */
    public StopPaymentResponseTransaction getStopPaymentResult() {
        return stopPaymentResult;
    }

    /**
     * Sets the value of the stopPaymentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link StopPaymentResponseTransaction }
     *     
     */
    public void setStopPaymentResult(StopPaymentResponseTransaction value) {
        this.stopPaymentResult = value;
    }

}
