
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitCardMiniStatementResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}DebitCardMiniStatementResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "debitCardMiniStatementResult"
})
@XmlRootElement(name = "DebitCardMiniStatementResponse")
public class DebitCardMiniStatementResponse {

    @XmlElement(name = "DebitCardMiniStatementResult")
    protected DebitCardMiniStatementResponseTransaction debitCardMiniStatementResult;

    /**
     * Gets the value of the debitCardMiniStatementResult property.
     * 
     * @return
     *     possible object is
     *     {@link DebitCardMiniStatementResponseTransaction }
     *     
     */
    public DebitCardMiniStatementResponseTransaction getDebitCardMiniStatementResult() {
        return debitCardMiniStatementResult;
    }

    /**
     * Sets the value of the debitCardMiniStatementResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitCardMiniStatementResponseTransaction }
     *     
     */
    public void setDebitCardMiniStatementResult(DebitCardMiniStatementResponseTransaction value) {
        this.debitCardMiniStatementResult = value;
    }

}
