
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockSMSRegList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockSMSRegList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SmsListMobileNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmsListAcctNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmsListName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmsListMobilePAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmsListBrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmsListBrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmsListRegStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmsListRegDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockSMSRegList", propOrder = {
    "smsListMobileNum",
    "smsListAcctNum",
    "smsListName",
    "smsListMobilePAN",
    "smsListBrCode",
    "smsListBrName",
    "smsListRegStatus",
    "smsListRegDate"
})
public class BlockSMSRegList {

    @XmlElement(name = "SmsListMobileNum")
    protected String smsListMobileNum;
    @XmlElement(name = "SmsListAcctNum")
    protected String smsListAcctNum;
    @XmlElement(name = "SmsListName")
    protected String smsListName;
    @XmlElement(name = "SmsListMobilePAN")
    protected String smsListMobilePAN;
    @XmlElement(name = "SmsListBrCode")
    protected String smsListBrCode;
    @XmlElement(name = "SmsListBrName")
    protected String smsListBrName;
    @XmlElement(name = "SmsListRegStatus")
    protected String smsListRegStatus;
    @XmlElement(name = "SmsListRegDate")
    protected String smsListRegDate;

    /**
     * Gets the value of the smsListMobileNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListMobileNum() {
        return smsListMobileNum;
    }

    /**
     * Sets the value of the smsListMobileNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListMobileNum(String value) {
        this.smsListMobileNum = value;
    }

    /**
     * Gets the value of the smsListAcctNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListAcctNum() {
        return smsListAcctNum;
    }

    /**
     * Sets the value of the smsListAcctNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListAcctNum(String value) {
        this.smsListAcctNum = value;
    }

    /**
     * Gets the value of the smsListName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListName() {
        return smsListName;
    }

    /**
     * Sets the value of the smsListName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListName(String value) {
        this.smsListName = value;
    }

    /**
     * Gets the value of the smsListMobilePAN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListMobilePAN() {
        return smsListMobilePAN;
    }

    /**
     * Sets the value of the smsListMobilePAN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListMobilePAN(String value) {
        this.smsListMobilePAN = value;
    }

    /**
     * Gets the value of the smsListBrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListBrCode() {
        return smsListBrCode;
    }

    /**
     * Sets the value of the smsListBrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListBrCode(String value) {
        this.smsListBrCode = value;
    }

    /**
     * Gets the value of the smsListBrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListBrName() {
        return smsListBrName;
    }

    /**
     * Sets the value of the smsListBrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListBrName(String value) {
        this.smsListBrName = value;
    }

    /**
     * Gets the value of the smsListRegStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListRegStatus() {
        return smsListRegStatus;
    }

    /**
     * Sets the value of the smsListRegStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListRegStatus(String value) {
        this.smsListRegStatus = value;
    }

    /**
     * Gets the value of the smsListRegDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsListRegDate() {
        return smsListRegDate;
    }

    /**
     * Sets the value of the smsListRegDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsListRegDate(String value) {
        this.smsListRegDate = value;
    }

}
