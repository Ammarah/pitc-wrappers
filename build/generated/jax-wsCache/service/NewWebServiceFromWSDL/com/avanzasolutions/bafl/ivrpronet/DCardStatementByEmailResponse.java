
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DCardStatementByEmailResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}DCardStatementByEmailResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dCardStatementByEmailResult"
})
@XmlRootElement(name = "DCardStatementByEmailResponse")
public class DCardStatementByEmailResponse {

    @XmlElement(name = "DCardStatementByEmailResult")
    protected DCardStatementByEmailResponseTransaction dCardStatementByEmailResult;

    /**
     * Gets the value of the dCardStatementByEmailResult property.
     * 
     * @return
     *     possible object is
     *     {@link DCardStatementByEmailResponseTransaction }
     *     
     */
    public DCardStatementByEmailResponseTransaction getDCardStatementByEmailResult() {
        return dCardStatementByEmailResult;
    }

    /**
     * Sets the value of the dCardStatementByEmailResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DCardStatementByEmailResponseTransaction }
     *     
     */
    public void setDCardStatementByEmailResult(DCardStatementByEmailResponseTransaction value) {
        this.dCardStatementByEmailResult = value;
    }

}
