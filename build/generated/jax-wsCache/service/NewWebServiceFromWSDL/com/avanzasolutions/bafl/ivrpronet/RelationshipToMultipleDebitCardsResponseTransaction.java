
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RelationshipToMultipleDebitCardsResponseTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationshipToMultipleDebitCardsResponseTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}Header" minOccurs="0"/>
 *         &lt;element name="BlockRelationshipToMultipleDebitCards" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockRelationshipToMultipleDebitCards" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationshipToMultipleDebitCardsResponseTransaction", propOrder = {
    "header",
    "blockRelationshipToMultipleDebitCards"
})
public class RelationshipToMultipleDebitCardsResponseTransaction {

    @XmlElement(name = "Header")
    protected Header header;
    @XmlElement(name = "BlockRelationshipToMultipleDebitCards")
    protected BlockRelationshipToMultipleDebitCards blockRelationshipToMultipleDebitCards;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the blockRelationshipToMultipleDebitCards property.
     * 
     * @return
     *     possible object is
     *     {@link BlockRelationshipToMultipleDebitCards }
     *     
     */
    public BlockRelationshipToMultipleDebitCards getBlockRelationshipToMultipleDebitCards() {
        return blockRelationshipToMultipleDebitCards;
    }

    /**
     * Sets the value of the blockRelationshipToMultipleDebitCards property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockRelationshipToMultipleDebitCards }
     *     
     */
    public void setBlockRelationshipToMultipleDebitCards(BlockRelationshipToMultipleDebitCards value) {
        this.blockRelationshipToMultipleDebitCards = value;
    }

}
