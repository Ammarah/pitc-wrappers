
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditCardTransactionInquiryResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}CreditCardTransactionInquiryResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "creditCardTransactionInquiryResult"
})
@XmlRootElement(name = "CreditCardTransactionInquiryResponse")
public class CreditCardTransactionInquiryResponse {

    @XmlElement(name = "CreditCardTransactionInquiryResult")
    protected CreditCardTransactionInquiryResponseTransaction creditCardTransactionInquiryResult;

    /**
     * Gets the value of the creditCardTransactionInquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardTransactionInquiryResponseTransaction }
     *     
     */
    public CreditCardTransactionInquiryResponseTransaction getCreditCardTransactionInquiryResult() {
        return creditCardTransactionInquiryResult;
    }

    /**
     * Sets the value of the creditCardTransactionInquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardTransactionInquiryResponseTransaction }
     *     
     */
    public void setCreditCardTransactionInquiryResult(CreditCardTransactionInquiryResponseTransaction value) {
        this.creditCardTransactionInquiryResult = value;
    }

}
