
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountDetailResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountDetailResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BlockAccountDetail" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockAccountDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountDetailResponseBody", propOrder = {
    "blockAccountDetail"
})
public class AccountDetailResponseBody {

    @XmlElement(name = "BlockAccountDetail")
    protected BlockAccountDetail blockAccountDetail;

    /**
     * Gets the value of the blockAccountDetail property.
     * 
     * @return
     *     possible object is
     *     {@link BlockAccountDetail }
     *     
     */
    public BlockAccountDetail getBlockAccountDetail() {
        return blockAccountDetail;
    }

    /**
     * Sets the value of the blockAccountDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockAccountDetail }
     *     
     */
    public void setBlockAccountDetail(BlockAccountDetail value) {
        this.blockAccountDetail = value;
    }

}
