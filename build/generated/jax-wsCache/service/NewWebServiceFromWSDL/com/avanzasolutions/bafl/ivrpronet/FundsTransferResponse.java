
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FundsTransferResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}FundsTransferResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fundsTransferResult"
})
@XmlRootElement(name = "FundsTransferResponse")
public class FundsTransferResponse {

    @XmlElement(name = "FundsTransferResult")
    protected FundsTransferResponseTransaction fundsTransferResult;

    /**
     * Gets the value of the fundsTransferResult property.
     * 
     * @return
     *     possible object is
     *     {@link FundsTransferResponseTransaction }
     *     
     */
    public FundsTransferResponseTransaction getFundsTransferResult() {
        return fundsTransferResult;
    }

    /**
     * Sets the value of the fundsTransferResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link FundsTransferResponseTransaction }
     *     
     */
    public void setFundsTransferResult(FundsTransferResponseTransaction value) {
        this.fundsTransferResult = value;
    }

}
