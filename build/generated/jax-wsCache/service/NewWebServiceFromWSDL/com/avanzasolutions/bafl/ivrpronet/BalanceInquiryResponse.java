
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BalanceInquiryResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BalanceInquiryResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "balanceInquiryResult"
})
@XmlRootElement(name = "BalanceInquiryResponse")
public class BalanceInquiryResponse {

    @XmlElement(name = "BalanceInquiryResult")
    protected BalanceInquiryResponseTransaction balanceInquiryResult;

    /**
     * Gets the value of the balanceInquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link BalanceInquiryResponseTransaction }
     *     
     */
    public BalanceInquiryResponseTransaction getBalanceInquiryResult() {
        return balanceInquiryResult;
    }

    /**
     * Sets the value of the balanceInquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BalanceInquiryResponseTransaction }
     *     
     */
    public void setBalanceInquiryResult(BalanceInquiryResponseTransaction value) {
        this.balanceInquiryResult = value;
    }

}
