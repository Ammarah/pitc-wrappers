
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SMSRegisteredUserListResponseTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SMSRegisteredUserListResponseTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}Header" minOccurs="0"/>
 *         &lt;element name="Body" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}SMSRegisteredUserListResponseBody" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SMSRegisteredUserListResponseTransaction", propOrder = {
    "header",
    "body"
})
public class SMSRegisteredUserListResponseTransaction {

    @XmlElement(name = "Header")
    protected Header header;
    @XmlElement(name = "Body")
    protected SMSRegisteredUserListResponseBody body;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link SMSRegisteredUserListResponseBody }
     *     
     */
    public SMSRegisteredUserListResponseBody getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link SMSRegisteredUserListResponseBody }
     *     
     */
    public void setBody(SMSRegisteredUserListResponseBody value) {
        this.body = value;
    }

}
