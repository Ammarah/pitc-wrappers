
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Header complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Header">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MessageProtocol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FieldInError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MessageType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryChannelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerIdentification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InternalCustomerIdentification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PINData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChannelSpecificDataField" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChannelPrivateData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuthorizationResponseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Header", propOrder = {
    "messageProtocol",
    "version",
    "fieldInError",
    "messageType",
    "tranDateTime",
    "deliveryChannelType",
    "deliveryChannelID",
    "customerIdentification",
    "internalCustomerIdentification",
    "transactionCode",
    "transactionDate",
    "transactionTime",
    "rrn",
    "pinData",
    "agentID",
    "channelSpecificDataField",
    "channelPrivateData",
    "authorizationResponseId",
    "responseCode"
})
public class Header {

    @XmlElement(name = "MessageProtocol")
    protected String messageProtocol;
    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "FieldInError")
    protected String fieldInError;
    @XmlElement(name = "MessageType")
    protected String messageType;
    @XmlElement(name = "TranDateTime")
    protected String tranDateTime;
    @XmlElement(name = "DeliveryChannelType")
    protected String deliveryChannelType;
    @XmlElement(name = "DeliveryChannelID")
    protected String deliveryChannelID;
    @XmlElement(name = "CustomerIdentification")
    protected String customerIdentification;
    @XmlElement(name = "InternalCustomerIdentification")
    protected String internalCustomerIdentification;
    @XmlElement(name = "TransactionCode")
    protected String transactionCode;
    @XmlElement(name = "TransactionDate")
    protected String transactionDate;
    @XmlElement(name = "TransactionTime")
    protected String transactionTime;
    @XmlElement(name = "RRN")
    protected String rrn;
    @XmlElement(name = "PINData")
    protected String pinData;
    @XmlElement(name = "AgentID")
    protected String agentID;
    @XmlElement(name = "ChannelSpecificDataField")
    protected String channelSpecificDataField;
    @XmlElement(name = "ChannelPrivateData")
    protected String channelPrivateData;
    @XmlElement(name = "AuthorizationResponseId")
    protected String authorizationResponseId;
    @XmlElement(name = "ResponseCode")
    protected String responseCode;

    /**
     * Gets the value of the messageProtocol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageProtocol() {
        return messageProtocol;
    }

    /**
     * Sets the value of the messageProtocol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageProtocol(String value) {
        this.messageProtocol = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the fieldInError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldInError() {
        return fieldInError;
    }

    /**
     * Sets the value of the fieldInError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldInError(String value) {
        this.fieldInError = value;
    }

    /**
     * Gets the value of the messageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Sets the value of the messageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageType(String value) {
        this.messageType = value;
    }

    /**
     * Gets the value of the tranDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranDateTime() {
        return tranDateTime;
    }

    /**
     * Sets the value of the tranDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranDateTime(String value) {
        this.tranDateTime = value;
    }

    /**
     * Gets the value of the deliveryChannelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryChannelType() {
        return deliveryChannelType;
    }

    /**
     * Sets the value of the deliveryChannelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryChannelType(String value) {
        this.deliveryChannelType = value;
    }

    /**
     * Gets the value of the deliveryChannelID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryChannelID() {
        return deliveryChannelID;
    }

    /**
     * Sets the value of the deliveryChannelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryChannelID(String value) {
        this.deliveryChannelID = value;
    }

    /**
     * Gets the value of the customerIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerIdentification() {
        return customerIdentification;
    }

    /**
     * Sets the value of the customerIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerIdentification(String value) {
        this.customerIdentification = value;
    }

    /**
     * Gets the value of the internalCustomerIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalCustomerIdentification() {
        return internalCustomerIdentification;
    }

    /**
     * Sets the value of the internalCustomerIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalCustomerIdentification(String value) {
        this.internalCustomerIdentification = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDate(String value) {
        this.transactionDate = value;
    }

    /**
     * Gets the value of the transactionTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionTime() {
        return transactionTime;
    }

    /**
     * Sets the value of the transactionTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionTime(String value) {
        this.transactionTime = value;
    }

    /**
     * Gets the value of the rrn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRRN() {
        return rrn;
    }

    /**
     * Sets the value of the rrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRRN(String value) {
        this.rrn = value;
    }

    /**
     * Gets the value of the pinData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPINData() {
        return pinData;
    }

    /**
     * Sets the value of the pinData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPINData(String value) {
        this.pinData = value;
    }

    /**
     * Gets the value of the agentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentID() {
        return agentID;
    }

    /**
     * Sets the value of the agentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentID(String value) {
        this.agentID = value;
    }

    /**
     * Gets the value of the channelSpecificDataField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelSpecificDataField() {
        return channelSpecificDataField;
    }

    /**
     * Sets the value of the channelSpecificDataField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelSpecificDataField(String value) {
        this.channelSpecificDataField = value;
    }

    /**
     * Gets the value of the channelPrivateData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelPrivateData() {
        return channelPrivateData;
    }

    /**
     * Sets the value of the channelPrivateData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelPrivateData(String value) {
        this.channelPrivateData = value;
    }

    /**
     * Gets the value of the authorizationResponseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationResponseId() {
        return authorizationResponseId;
    }

    /**
     * Sets the value of the authorizationResponseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationResponseId(String value) {
        this.authorizationResponseId = value;
    }

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

}
