
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SMSRegisteredUserListResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SMSRegisteredUserListResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BlockSMSRegList" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockSMSRegList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SMSRegisteredUserListResponseBody", propOrder = {
    "blockSMSRegList"
})
public class SMSRegisteredUserListResponseBody {

    @XmlElement(name = "BlockSMSRegList")
    protected BlockSMSRegList blockSMSRegList;

    /**
     * Gets the value of the blockSMSRegList property.
     * 
     * @return
     *     possible object is
     *     {@link BlockSMSRegList }
     *     
     */
    public BlockSMSRegList getBlockSMSRegList() {
        return blockSMSRegList;
    }

    /**
     * Sets the value of the blockSMSRegList property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockSMSRegList }
     *     
     */
    public void setBlockSMSRegList(BlockSMSRegList value) {
        this.blockSMSRegList = value;
    }

}
