
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DebitCardDetailsResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DebitCardDetailsResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardExpiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WithdrawalCycleBeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WithdrawalCycleLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WithdrawalLimits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailableWithdrawalLimits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseCycleBeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseCycleLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurchaseLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailablePurchaseLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundsTransCycleBeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundsTransCycleLen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FundsTransLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailableFundsTransLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxPINRetries" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PINRetriesRemaining" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PINExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DebitDetailActType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BiBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BiBrCodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BiBrCodeAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BiBrCodeContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BiBrCodeFax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountIMD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitCardDetailsResponseBody", propOrder = {
    "debitCardNumber",
    "cardExpiry",
    "procCode",
    "withdrawalCycleBeginDate",
    "withdrawalCycleLength",
    "withdrawalLimits",
    "availableWithdrawalLimits",
    "purchaseFlag",
    "purchaseCycleBeginDate",
    "purchaseCycleLength",
    "purchaseLimit",
    "availablePurchaseLimit",
    "fundFlag",
    "fundsTransCycleBeginDate",
    "fundsTransCycleLen",
    "fundsTransLimit",
    "availableFundsTransLimit",
    "name",
    "address1",
    "address2",
    "maxPINRetries",
    "pinRetriesRemaining",
    "pinExpiryDate",
    "cardStatusCode",
    "accountNumber",
    "debitDetailActType",
    "biBranchCode",
    "biBrCodeName",
    "biBrCodeAddress",
    "biBrCodeContact",
    "biBrCodeFax",
    "accountIMD"
})
public class DebitCardDetailsResponseBody {

    @XmlElement(name = "DebitCardNumber")
    protected String debitCardNumber;
    @XmlElement(name = "CardExpiry")
    protected String cardExpiry;
    @XmlElement(name = "ProcCode")
    protected String procCode;
    @XmlElement(name = "WithdrawalCycleBeginDate")
    protected String withdrawalCycleBeginDate;
    @XmlElement(name = "WithdrawalCycleLength")
    protected String withdrawalCycleLength;
    @XmlElement(name = "WithdrawalLimits")
    protected String withdrawalLimits;
    @XmlElement(name = "AvailableWithdrawalLimits")
    protected String availableWithdrawalLimits;
    @XmlElement(name = "PurchaseFlag")
    protected String purchaseFlag;
    @XmlElement(name = "PurchaseCycleBeginDate")
    protected String purchaseCycleBeginDate;
    @XmlElement(name = "PurchaseCycleLength")
    protected String purchaseCycleLength;
    @XmlElement(name = "PurchaseLimit")
    protected String purchaseLimit;
    @XmlElement(name = "AvailablePurchaseLimit")
    protected String availablePurchaseLimit;
    @XmlElement(name = "FundFlag")
    protected String fundFlag;
    @XmlElement(name = "FundsTransCycleBeginDate")
    protected String fundsTransCycleBeginDate;
    @XmlElement(name = "FundsTransCycleLen")
    protected String fundsTransCycleLen;
    @XmlElement(name = "FundsTransLimit")
    protected String fundsTransLimit;
    @XmlElement(name = "AvailableFundsTransLimit")
    protected String availableFundsTransLimit;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Address1")
    protected String address1;
    @XmlElement(name = "Address2")
    protected String address2;
    @XmlElement(name = "MaxPINRetries")
    protected String maxPINRetries;
    @XmlElement(name = "PINRetriesRemaining")
    protected String pinRetriesRemaining;
    @XmlElement(name = "PINExpiryDate")
    protected String pinExpiryDate;
    @XmlElement(name = "CardStatusCode")
    protected String cardStatusCode;
    @XmlElement(name = "AccountNumber")
    protected String accountNumber;
    @XmlElement(name = "DebitDetailActType")
    protected String debitDetailActType;
    @XmlElement(name = "BiBranchCode")
    protected String biBranchCode;
    @XmlElement(name = "BiBrCodeName")
    protected String biBrCodeName;
    @XmlElement(name = "BiBrCodeAddress")
    protected String biBrCodeAddress;
    @XmlElement(name = "BiBrCodeContact")
    protected String biBrCodeContact;
    @XmlElement(name = "BiBrCodeFax")
    protected String biBrCodeFax;
    @XmlElement(name = "AccountIMD")
    protected String accountIMD;

    /**
     * Gets the value of the debitCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitCardNumber() {
        return debitCardNumber;
    }

    /**
     * Sets the value of the debitCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitCardNumber(String value) {
        this.debitCardNumber = value;
    }

    /**
     * Gets the value of the cardExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardExpiry() {
        return cardExpiry;
    }

    /**
     * Sets the value of the cardExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardExpiry(String value) {
        this.cardExpiry = value;
    }

    /**
     * Gets the value of the procCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcCode() {
        return procCode;
    }

    /**
     * Sets the value of the procCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcCode(String value) {
        this.procCode = value;
    }

    /**
     * Gets the value of the withdrawalCycleBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithdrawalCycleBeginDate() {
        return withdrawalCycleBeginDate;
    }

    /**
     * Sets the value of the withdrawalCycleBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithdrawalCycleBeginDate(String value) {
        this.withdrawalCycleBeginDate = value;
    }

    /**
     * Gets the value of the withdrawalCycleLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithdrawalCycleLength() {
        return withdrawalCycleLength;
    }

    /**
     * Sets the value of the withdrawalCycleLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithdrawalCycleLength(String value) {
        this.withdrawalCycleLength = value;
    }

    /**
     * Gets the value of the withdrawalLimits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithdrawalLimits() {
        return withdrawalLimits;
    }

    /**
     * Sets the value of the withdrawalLimits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithdrawalLimits(String value) {
        this.withdrawalLimits = value;
    }

    /**
     * Gets the value of the availableWithdrawalLimits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailableWithdrawalLimits() {
        return availableWithdrawalLimits;
    }

    /**
     * Sets the value of the availableWithdrawalLimits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailableWithdrawalLimits(String value) {
        this.availableWithdrawalLimits = value;
    }

    /**
     * Gets the value of the purchaseFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseFlag() {
        return purchaseFlag;
    }

    /**
     * Sets the value of the purchaseFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseFlag(String value) {
        this.purchaseFlag = value;
    }

    /**
     * Gets the value of the purchaseCycleBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseCycleBeginDate() {
        return purchaseCycleBeginDate;
    }

    /**
     * Sets the value of the purchaseCycleBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseCycleBeginDate(String value) {
        this.purchaseCycleBeginDate = value;
    }

    /**
     * Gets the value of the purchaseCycleLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseCycleLength() {
        return purchaseCycleLength;
    }

    /**
     * Sets the value of the purchaseCycleLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseCycleLength(String value) {
        this.purchaseCycleLength = value;
    }

    /**
     * Gets the value of the purchaseLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseLimit() {
        return purchaseLimit;
    }

    /**
     * Sets the value of the purchaseLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseLimit(String value) {
        this.purchaseLimit = value;
    }

    /**
     * Gets the value of the availablePurchaseLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailablePurchaseLimit() {
        return availablePurchaseLimit;
    }

    /**
     * Sets the value of the availablePurchaseLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailablePurchaseLimit(String value) {
        this.availablePurchaseLimit = value;
    }

    /**
     * Gets the value of the fundFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundFlag() {
        return fundFlag;
    }

    /**
     * Sets the value of the fundFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundFlag(String value) {
        this.fundFlag = value;
    }

    /**
     * Gets the value of the fundsTransCycleBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundsTransCycleBeginDate() {
        return fundsTransCycleBeginDate;
    }

    /**
     * Sets the value of the fundsTransCycleBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundsTransCycleBeginDate(String value) {
        this.fundsTransCycleBeginDate = value;
    }

    /**
     * Gets the value of the fundsTransCycleLen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundsTransCycleLen() {
        return fundsTransCycleLen;
    }

    /**
     * Sets the value of the fundsTransCycleLen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundsTransCycleLen(String value) {
        this.fundsTransCycleLen = value;
    }

    /**
     * Gets the value of the fundsTransLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundsTransLimit() {
        return fundsTransLimit;
    }

    /**
     * Sets the value of the fundsTransLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundsTransLimit(String value) {
        this.fundsTransLimit = value;
    }

    /**
     * Gets the value of the availableFundsTransLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailableFundsTransLimit() {
        return availableFundsTransLimit;
    }

    /**
     * Sets the value of the availableFundsTransLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailableFundsTransLimit(String value) {
        this.availableFundsTransLimit = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the maxPINRetries property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxPINRetries() {
        return maxPINRetries;
    }

    /**
     * Sets the value of the maxPINRetries property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxPINRetries(String value) {
        this.maxPINRetries = value;
    }

    /**
     * Gets the value of the pinRetriesRemaining property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPINRetriesRemaining() {
        return pinRetriesRemaining;
    }

    /**
     * Sets the value of the pinRetriesRemaining property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPINRetriesRemaining(String value) {
        this.pinRetriesRemaining = value;
    }

    /**
     * Gets the value of the pinExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPINExpiryDate() {
        return pinExpiryDate;
    }

    /**
     * Sets the value of the pinExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPINExpiryDate(String value) {
        this.pinExpiryDate = value;
    }

    /**
     * Gets the value of the cardStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardStatusCode() {
        return cardStatusCode;
    }

    /**
     * Sets the value of the cardStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardStatusCode(String value) {
        this.cardStatusCode = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the debitDetailActType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitDetailActType() {
        return debitDetailActType;
    }

    /**
     * Sets the value of the debitDetailActType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitDetailActType(String value) {
        this.debitDetailActType = value;
    }

    /**
     * Gets the value of the biBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBiBranchCode() {
        return biBranchCode;
    }

    /**
     * Sets the value of the biBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBiBranchCode(String value) {
        this.biBranchCode = value;
    }

    /**
     * Gets the value of the biBrCodeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBiBrCodeName() {
        return biBrCodeName;
    }

    /**
     * Sets the value of the biBrCodeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBiBrCodeName(String value) {
        this.biBrCodeName = value;
    }

    /**
     * Gets the value of the biBrCodeAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBiBrCodeAddress() {
        return biBrCodeAddress;
    }

    /**
     * Sets the value of the biBrCodeAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBiBrCodeAddress(String value) {
        this.biBrCodeAddress = value;
    }

    /**
     * Gets the value of the biBrCodeContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBiBrCodeContact() {
        return biBrCodeContact;
    }

    /**
     * Sets the value of the biBrCodeContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBiBrCodeContact(String value) {
        this.biBrCodeContact = value;
    }

    /**
     * Gets the value of the biBrCodeFax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBiBrCodeFax() {
        return biBrCodeFax;
    }

    /**
     * Sets the value of the biBrCodeFax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBiBrCodeFax(String value) {
        this.biBrCodeFax = value;
    }

    /**
     * Gets the value of the accountIMD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountIMD() {
        return accountIMD;
    }

    /**
     * Sets the value of the accountIMD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountIMD(String value) {
        this.accountIMD = value;
    }

}
