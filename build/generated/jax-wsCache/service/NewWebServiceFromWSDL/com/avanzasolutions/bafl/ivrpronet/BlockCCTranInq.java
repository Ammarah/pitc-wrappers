
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlockCCTranInq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BlockCCTranInq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CCPostDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCBilllingCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCBillingAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranCurrencyMinorUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranCurrentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlockCCTranInq", propOrder = {
    "ccPostDate",
    "ccTranDate",
    "ccTranCode",
    "ccTranDesc",
    "ccBilllingCurrencyCode",
    "ccBillingAmt",
    "ccTranCurrencyCode",
    "ccTranCurrencyMinorUnits",
    "ccTranAmt",
    "ccTranCurrentStatus"
})
public class BlockCCTranInq {

    @XmlElement(name = "CCPostDate")
    protected String ccPostDate;
    @XmlElement(name = "CCTranDate")
    protected String ccTranDate;
    @XmlElement(name = "CCTranCode")
    protected String ccTranCode;
    @XmlElement(name = "CCTranDesc")
    protected String ccTranDesc;
    @XmlElement(name = "CCBilllingCurrencyCode")
    protected String ccBilllingCurrencyCode;
    @XmlElement(name = "CCBillingAmt")
    protected String ccBillingAmt;
    @XmlElement(name = "CCTranCurrencyCode")
    protected String ccTranCurrencyCode;
    @XmlElement(name = "CCTranCurrencyMinorUnits")
    protected String ccTranCurrencyMinorUnits;
    @XmlElement(name = "CCTranAmt")
    protected String ccTranAmt;
    @XmlElement(name = "CCTranCurrentStatus")
    protected String ccTranCurrentStatus;

    /**
     * Gets the value of the ccPostDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCPostDate() {
        return ccPostDate;
    }

    /**
     * Sets the value of the ccPostDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCPostDate(String value) {
        this.ccPostDate = value;
    }

    /**
     * Gets the value of the ccTranDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTranDate() {
        return ccTranDate;
    }

    /**
     * Sets the value of the ccTranDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTranDate(String value) {
        this.ccTranDate = value;
    }

    /**
     * Gets the value of the ccTranCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTranCode() {
        return ccTranCode;
    }

    /**
     * Sets the value of the ccTranCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTranCode(String value) {
        this.ccTranCode = value;
    }

    /**
     * Gets the value of the ccTranDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTranDesc() {
        return ccTranDesc;
    }

    /**
     * Sets the value of the ccTranDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTranDesc(String value) {
        this.ccTranDesc = value;
    }

    /**
     * Gets the value of the ccBilllingCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCBilllingCurrencyCode() {
        return ccBilllingCurrencyCode;
    }

    /**
     * Sets the value of the ccBilllingCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCBilllingCurrencyCode(String value) {
        this.ccBilllingCurrencyCode = value;
    }

    /**
     * Gets the value of the ccBillingAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCBillingAmt() {
        return ccBillingAmt;
    }

    /**
     * Sets the value of the ccBillingAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCBillingAmt(String value) {
        this.ccBillingAmt = value;
    }

    /**
     * Gets the value of the ccTranCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTranCurrencyCode() {
        return ccTranCurrencyCode;
    }

    /**
     * Sets the value of the ccTranCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTranCurrencyCode(String value) {
        this.ccTranCurrencyCode = value;
    }

    /**
     * Gets the value of the ccTranCurrencyMinorUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTranCurrencyMinorUnits() {
        return ccTranCurrencyMinorUnits;
    }

    /**
     * Sets the value of the ccTranCurrencyMinorUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTranCurrencyMinorUnits(String value) {
        this.ccTranCurrencyMinorUnits = value;
    }

    /**
     * Gets the value of the ccTranAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTranAmt() {
        return ccTranAmt;
    }

    /**
     * Sets the value of the ccTranAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTranAmt(String value) {
        this.ccTranAmt = value;
    }

    /**
     * Gets the value of the ccTranCurrentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTranCurrentStatus() {
        return ccTranCurrentStatus;
    }

    /**
     * Sets the value of the ccTranCurrentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTranCurrentStatus(String value) {
        this.ccTranCurrentStatus = value;
    }

}
