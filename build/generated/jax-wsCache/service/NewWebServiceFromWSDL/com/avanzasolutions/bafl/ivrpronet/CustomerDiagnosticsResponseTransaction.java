
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerDiagnosticsResponseTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerDiagnosticsResponseTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}Header" minOccurs="0"/>
 *         &lt;element name="CustomerDiagnostics" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}BlockCustomerDiagnostics" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDiagnosticsResponseTransaction", propOrder = {
    "header",
    "customerDiagnostics"
})
public class CustomerDiagnosticsResponseTransaction {

    @XmlElement(name = "Header")
    protected Header header;
    @XmlElement(name = "CustomerDiagnostics")
    protected BlockCustomerDiagnostics customerDiagnostics;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the customerDiagnostics property.
     * 
     * @return
     *     possible object is
     *     {@link BlockCustomerDiagnostics }
     *     
     */
    public BlockCustomerDiagnostics getCustomerDiagnostics() {
        return customerDiagnostics;
    }

    /**
     * Sets the value of the customerDiagnostics property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockCustomerDiagnostics }
     *     
     */
    public void setCustomerDiagnostics(BlockCustomerDiagnostics value) {
        this.customerDiagnostics = value;
    }

}
