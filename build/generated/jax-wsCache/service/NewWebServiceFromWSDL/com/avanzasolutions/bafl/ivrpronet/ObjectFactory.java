
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.avanzasolutions.bafl.ivrpronet package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.avanzasolutions.bafl.ivrpronet
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FetchCardStatusResponse }
     * 
     */
    public FetchCardStatusResponse createFetchCardStatusResponse() {
        return new FetchCardStatusResponse();
    }

    /**
     * Create an instance of {@link FetchCardStatusResponseTransaction }
     * 
     */
    public FetchCardStatusResponseTransaction createFetchCardStatusResponseTransaction() {
        return new FetchCardStatusResponseTransaction();
    }

    /**
     * Create an instance of {@link DebitCardInquiryResponse }
     * 
     */
    public DebitCardInquiryResponse createDebitCardInquiryResponse() {
        return new DebitCardInquiryResponse();
    }

    /**
     * Create an instance of {@link DebitCardInquiryResponseTransaction }
     * 
     */
    public DebitCardInquiryResponseTransaction createDebitCardInquiryResponseTransaction() {
        return new DebitCardInquiryResponseTransaction();
    }

    /**
     * Create an instance of {@link TPINGeneration }
     * 
     */
    public TPINGeneration createTPINGeneration() {
        return new TPINGeneration();
    }

    /**
     * Create an instance of {@link TPINGenerationRequestTransaction }
     * 
     */
    public TPINGenerationRequestTransaction createTPINGenerationRequestTransaction() {
        return new TPINGenerationRequestTransaction();
    }

    /**
     * Create an instance of {@link CCardStatementByEmailResponse }
     * 
     */
    public CCardStatementByEmailResponse createCCardStatementByEmailResponse() {
        return new CCardStatementByEmailResponse();
    }

    /**
     * Create an instance of {@link CCardStatementByEmailResponseTransaction }
     * 
     */
    public CCardStatementByEmailResponseTransaction createCCardStatementByEmailResponseTransaction() {
        return new CCardStatementByEmailResponseTransaction();
    }

    /**
     * Create an instance of {@link AccountDetailResponse }
     * 
     */
    public AccountDetailResponse createAccountDetailResponse() {
        return new AccountDetailResponse();
    }

    /**
     * Create an instance of {@link AccountDetailResponseTransaction }
     * 
     */
    public AccountDetailResponseTransaction createAccountDetailResponseTransaction() {
        return new AccountDetailResponseTransaction();
    }

    /**
     * Create an instance of {@link CustomerAuthentication }
     * 
     */
    public CustomerAuthentication createCustomerAuthentication() {
        return new CustomerAuthentication();
    }

    /**
     * Create an instance of {@link CustomerAuthenticationRequestTransaction }
     * 
     */
    public CustomerAuthenticationRequestTransaction createCustomerAuthenticationRequestTransaction() {
        return new CustomerAuthenticationRequestTransaction();
    }

    /**
     * Create an instance of {@link DebitCardMiniStatementResponse }
     * 
     */
    public DebitCardMiniStatementResponse createDebitCardMiniStatementResponse() {
        return new DebitCardMiniStatementResponse();
    }

    /**
     * Create an instance of {@link DebitCardMiniStatementResponseTransaction }
     * 
     */
    public DebitCardMiniStatementResponseTransaction createDebitCardMiniStatementResponseTransaction() {
        return new DebitCardMiniStatementResponseTransaction();
    }

    /**
     * Create an instance of {@link RelationshipToMultipleDebitCards }
     * 
     */
    public RelationshipToMultipleDebitCards createRelationshipToMultipleDebitCards() {
        return new RelationshipToMultipleDebitCards();
    }

    /**
     * Create an instance of {@link RelationshipToMultipleDebitCardsRequestTransaction }
     * 
     */
    public RelationshipToMultipleDebitCardsRequestTransaction createRelationshipToMultipleDebitCardsRequestTransaction() {
        return new RelationshipToMultipleDebitCardsRequestTransaction();
    }

    /**
     * Create an instance of {@link DebitCardDetails }
     * 
     */
    public DebitCardDetails createDebitCardDetails() {
        return new DebitCardDetails();
    }

    /**
     * Create an instance of {@link DebitCardDetailsRequestTransaction }
     * 
     */
    public DebitCardDetailsRequestTransaction createDebitCardDetailsRequestTransaction() {
        return new DebitCardDetailsRequestTransaction();
    }

    /**
     * Create an instance of {@link CardDetailResponse }
     * 
     */
    public CardDetailResponse createCardDetailResponse() {
        return new CardDetailResponse();
    }

    /**
     * Create an instance of {@link CardDetailResponseTransaction }
     * 
     */
    public CardDetailResponseTransaction createCardDetailResponseTransaction() {
        return new CardDetailResponseTransaction();
    }

    /**
     * Create an instance of {@link PINValidationResponse }
     * 
     */
    public PINValidationResponse createPINValidationResponse() {
        return new PINValidationResponse();
    }

    /**
     * Create an instance of {@link PINValidationResponseTransaction }
     * 
     */
    public PINValidationResponseTransaction createPINValidationResponseTransaction() {
        return new PINValidationResponseTransaction();
    }

    /**
     * Create an instance of {@link TPINGenerationResponse }
     * 
     */
    public TPINGenerationResponse createTPINGenerationResponse() {
        return new TPINGenerationResponse();
    }

    /**
     * Create an instance of {@link TPINGenerationResponseTransaction }
     * 
     */
    public TPINGenerationResponseTransaction createTPINGenerationResponseTransaction() {
        return new TPINGenerationResponseTransaction();
    }

    /**
     * Create an instance of {@link DebitCardMiniStatement }
     * 
     */
    public DebitCardMiniStatement createDebitCardMiniStatement() {
        return new DebitCardMiniStatement();
    }

    /**
     * Create an instance of {@link DebitCardMiniStatementRequestTransaction }
     * 
     */
    public DebitCardMiniStatementRequestTransaction createDebitCardMiniStatementRequestTransaction() {
        return new DebitCardMiniStatementRequestTransaction();
    }

    /**
     * Create an instance of {@link UnBlockGDCIServiceResponse }
     * 
     */
    public UnBlockGDCIServiceResponse createUnBlockGDCIServiceResponse() {
        return new UnBlockGDCIServiceResponse();
    }

    /**
     * Create an instance of {@link UnBlockGDCIServiceResponseTransaction }
     * 
     */
    public UnBlockGDCIServiceResponseTransaction createUnBlockGDCIServiceResponseTransaction() {
        return new UnBlockGDCIServiceResponseTransaction();
    }

    /**
     * Create an instance of {@link CustomerSearchResponse }
     * 
     */
    public CustomerSearchResponse createCustomerSearchResponse() {
        return new CustomerSearchResponse();
    }

    /**
     * Create an instance of {@link CustomerSearchResponseTransaction }
     * 
     */
    public CustomerSearchResponseTransaction createCustomerSearchResponseTransaction() {
        return new CustomerSearchResponseTransaction();
    }

    /**
     * Create an instance of {@link CardDetail }
     * 
     */
    public CardDetail createCardDetail() {
        return new CardDetail();
    }

    /**
     * Create an instance of {@link CardDetailRequestTransaction }
     * 
     */
    public CardDetailRequestTransaction createCardDetailRequestTransaction() {
        return new CardDetailRequestTransaction();
    }

    /**
     * Create an instance of {@link SMSRegisteredUserListResponse }
     * 
     */
    public SMSRegisteredUserListResponse createSMSRegisteredUserListResponse() {
        return new SMSRegisteredUserListResponse();
    }

    /**
     * Create an instance of {@link SMSRegisteredUserListResponseTransaction }
     * 
     */
    public SMSRegisteredUserListResponseTransaction createSMSRegisteredUserListResponseTransaction() {
        return new SMSRegisteredUserListResponseTransaction();
    }

    /**
     * Create an instance of {@link CreditCardRelationshipInquiry }
     * 
     */
    public CreditCardRelationshipInquiry createCreditCardRelationshipInquiry() {
        return new CreditCardRelationshipInquiry();
    }

    /**
     * Create an instance of {@link CreditCardRelationshipInquiryRequestTransaction }
     * 
     */
    public CreditCardRelationshipInquiryRequestTransaction createCreditCardRelationshipInquiryRequestTransaction() {
        return new CreditCardRelationshipInquiryRequestTransaction();
    }

    /**
     * Create an instance of {@link CCAccountInquiryWithCustomerInfo }
     * 
     */
    public CCAccountInquiryWithCustomerInfo createCCAccountInquiryWithCustomerInfo() {
        return new CCAccountInquiryWithCustomerInfo();
    }

    /**
     * Create an instance of {@link CCAccountInquiryWithCustomerInfoRequestTransaction }
     * 
     */
    public CCAccountInquiryWithCustomerInfoRequestTransaction createCCAccountInquiryWithCustomerInfoRequestTransaction() {
        return new CCAccountInquiryWithCustomerInfoRequestTransaction();
    }

    /**
     * Create an instance of {@link CCAccountInquiryWithCustomerInfoResponse }
     * 
     */
    public CCAccountInquiryWithCustomerInfoResponse createCCAccountInquiryWithCustomerInfoResponse() {
        return new CCAccountInquiryWithCustomerInfoResponse();
    }

    /**
     * Create an instance of {@link CCAccountInquiryWithCustomerInfoResponseTransaction }
     * 
     */
    public CCAccountInquiryWithCustomerInfoResponseTransaction createCCAccountInquiryWithCustomerInfoResponseTransaction() {
        return new CCAccountInquiryWithCustomerInfoResponseTransaction();
    }

    /**
     * Create an instance of {@link CreditCardAccountPaymentResponse }
     * 
     */
    public CreditCardAccountPaymentResponse createCreditCardAccountPaymentResponse() {
        return new CreditCardAccountPaymentResponse();
    }

    /**
     * Create an instance of {@link CreditCardAccountPaymentResponseTransaction }
     * 
     */
    public CreditCardAccountPaymentResponseTransaction createCreditCardAccountPaymentResponseTransaction() {
        return new CreditCardAccountPaymentResponseTransaction();
    }

    /**
     * Create an instance of {@link CustomerCardRelationshipEnquiry }
     * 
     */
    public CustomerCardRelationshipEnquiry createCustomerCardRelationshipEnquiry() {
        return new CustomerCardRelationshipEnquiry();
    }

    /**
     * Create an instance of {@link CustomerCardRelationshipEnquiryRequestTransaction }
     * 
     */
    public CustomerCardRelationshipEnquiryRequestTransaction createCustomerCardRelationshipEnquiryRequestTransaction() {
        return new CustomerCardRelationshipEnquiryRequestTransaction();
    }

    /**
     * Create an instance of {@link ATMPINGenerationResponse }
     * 
     */
    public ATMPINGenerationResponse createATMPINGenerationResponse() {
        return new ATMPINGenerationResponse();
    }

    /**
     * Create an instance of {@link ATMPINGenerationResponseTransaction }
     * 
     */
    public ATMPINGenerationResponseTransaction createATMPINGenerationResponseTransaction() {
        return new ATMPINGenerationResponseTransaction();
    }

    /**
     * Create an instance of {@link BillPayment }
     * 
     */
    public BillPayment createBillPayment() {
        return new BillPayment();
    }

    /**
     * Create an instance of {@link BillPaymentRequestTransaction }
     * 
     */
    public BillPaymentRequestTransaction createBillPaymentRequestTransaction() {
        return new BillPaymentRequestTransaction();
    }

    /**
     * Create an instance of {@link ChequeBook }
     * 
     */
    public ChequeBook createChequeBook() {
        return new ChequeBook();
    }

    /**
     * Create an instance of {@link ChequeBookRequestTransaction }
     * 
     */
    public ChequeBookRequestTransaction createChequeBookRequestTransaction() {
        return new ChequeBookRequestTransaction();
    }

    /**
     * Create an instance of {@link EnvelopMessageResponse }
     * 
     */
    public EnvelopMessageResponse createEnvelopMessageResponse() {
        return new EnvelopMessageResponse();
    }

    /**
     * Create an instance of {@link EnvelopMessageResponseTransaction }
     * 
     */
    public EnvelopMessageResponseTransaction createEnvelopMessageResponseTransaction() {
        return new EnvelopMessageResponseTransaction();
    }

    /**
     * Create an instance of {@link DCardStatementByEmail }
     * 
     */
    public DCardStatementByEmail createDCardStatementByEmail() {
        return new DCardStatementByEmail();
    }

    /**
     * Create an instance of {@link DCardStatementByEmailRequestTransaction }
     * 
     */
    public DCardStatementByEmailRequestTransaction createDCardStatementByEmailRequestTransaction() {
        return new DCardStatementByEmailRequestTransaction();
    }

    /**
     * Create an instance of {@link CustomerDiagnostics }
     * 
     */
    public CustomerDiagnostics createCustomerDiagnostics() {
        return new CustomerDiagnostics();
    }

    /**
     * Create an instance of {@link CustomerDiagnosticsRequestTransaction }
     * 
     */
    public CustomerDiagnosticsRequestTransaction createCustomerDiagnosticsRequestTransaction() {
        return new CustomerDiagnosticsRequestTransaction();
    }

    /**
     * Create an instance of {@link WalletCardPinGeneration }
     * 
     */
    public WalletCardPinGeneration createWalletCardPinGeneration() {
        return new WalletCardPinGeneration();
    }

    /**
     * Create an instance of {@link WalletCardPinGenerationRequestTransaction }
     * 
     */
    public WalletCardPinGenerationRequestTransaction createWalletCardPinGenerationRequestTransaction() {
        return new WalletCardPinGenerationRequestTransaction();
    }

    /**
     * Create an instance of {@link BillInquiry }
     * 
     */
    public BillInquiry createBillInquiry() {
        return new BillInquiry();
    }

    /**
     * Create an instance of {@link BillInquiryRequestTransaction }
     * 
     */
    public BillInquiryRequestTransaction createBillInquiryRequestTransaction() {
        return new BillInquiryRequestTransaction();
    }

    /**
     * Create an instance of {@link CustomerAuthenticationResponse }
     * 
     */
    public CustomerAuthenticationResponse createCustomerAuthenticationResponse() {
        return new CustomerAuthenticationResponse();
    }

    /**
     * Create an instance of {@link CustomerAuthenticationResponseTransaction }
     * 
     */
    public CustomerAuthenticationResponseTransaction createCustomerAuthenticationResponseTransaction() {
        return new CustomerAuthenticationResponseTransaction();
    }

    /**
     * Create an instance of {@link LoanInquiry }
     * 
     */
    public LoanInquiry createLoanInquiry() {
        return new LoanInquiry();
    }

    /**
     * Create an instance of {@link LoanInquiryRequestTransaction }
     * 
     */
    public LoanInquiryRequestTransaction createLoanInquiryRequestTransaction() {
        return new LoanInquiryRequestTransaction();
    }

    /**
     * Create an instance of {@link SendSMSResponse }
     * 
     */
    public SendSMSResponse createSendSMSResponse() {
        return new SendSMSResponse();
    }

    /**
     * Create an instance of {@link SendSMSResponseTransaction }
     * 
     */
    public SendSMSResponseTransaction createSendSMSResponseTransaction() {
        return new SendSMSResponseTransaction();
    }

    /**
     * Create an instance of {@link ATMPINGeneration }
     * 
     */
    public ATMPINGeneration createATMPINGeneration() {
        return new ATMPINGeneration();
    }

    /**
     * Create an instance of {@link ATMPINGenerationRequestTransaction }
     * 
     */
    public ATMPINGenerationRequestTransaction createATMPINGenerationRequestTransaction() {
        return new ATMPINGenerationRequestTransaction();
    }

    /**
     * Create an instance of {@link TPINChange }
     * 
     */
    public TPINChange createTPINChange() {
        return new TPINChange();
    }

    /**
     * Create an instance of {@link TPINChangeRequestTransaction }
     * 
     */
    public TPINChangeRequestTransaction createTPINChangeRequestTransaction() {
        return new TPINChangeRequestTransaction();
    }

    /**
     * Create an instance of {@link CreditCardRelationshipInquiryResponse }
     * 
     */
    public CreditCardRelationshipInquiryResponse createCreditCardRelationshipInquiryResponse() {
        return new CreditCardRelationshipInquiryResponse();
    }

    /**
     * Create an instance of {@link CreditCardRelationshipInquiryResponseTransaction }
     * 
     */
    public CreditCardRelationshipInquiryResponseTransaction createCreditCardRelationshipInquiryResponseTransaction() {
        return new CreditCardRelationshipInquiryResponseTransaction();
    }

    /**
     * Create an instance of {@link OpenAccountFundsTransfer }
     * 
     */
    public OpenAccountFundsTransfer createOpenAccountFundsTransfer() {
        return new OpenAccountFundsTransfer();
    }

    /**
     * Create an instance of {@link OpenAccountFundsTransferRequestTransaction }
     * 
     */
    public OpenAccountFundsTransferRequestTransaction createOpenAccountFundsTransferRequestTransaction() {
        return new OpenAccountFundsTransferRequestTransaction();
    }

    /**
     * Create an instance of {@link ChargeCustomer }
     * 
     */
    public ChargeCustomer createChargeCustomer() {
        return new ChargeCustomer();
    }

    /**
     * Create an instance of {@link ChargeCustomerRequestTransaction }
     * 
     */
    public ChargeCustomerRequestTransaction createChargeCustomerRequestTransaction() {
        return new ChargeCustomerRequestTransaction();
    }

    /**
     * Create an instance of {@link WalletCardPinChange }
     * 
     */
    public WalletCardPinChange createWalletCardPinChange() {
        return new WalletCardPinChange();
    }

    /**
     * Create an instance of {@link WalletCardPinChangeRequestTransaction }
     * 
     */
    public WalletCardPinChangeRequestTransaction createWalletCardPinChangeRequestTransaction() {
        return new WalletCardPinChangeRequestTransaction();
    }

    /**
     * Create an instance of {@link SendSMS }
     * 
     */
    public SendSMS createSendSMS() {
        return new SendSMS();
    }

    /**
     * Create an instance of {@link SendSMSRequestTransaction }
     * 
     */
    public SendSMSRequestTransaction createSendSMSRequestTransaction() {
        return new SendSMSRequestTransaction();
    }

    /**
     * Create an instance of {@link RelationshipToMultipleDebitCardsResponse }
     * 
     */
    public RelationshipToMultipleDebitCardsResponse createRelationshipToMultipleDebitCardsResponse() {
        return new RelationshipToMultipleDebitCardsResponse();
    }

    /**
     * Create an instance of {@link RelationshipToMultipleDebitCardsResponseTransaction }
     * 
     */
    public RelationshipToMultipleDebitCardsResponseTransaction createRelationshipToMultipleDebitCardsResponseTransaction() {
        return new RelationshipToMultipleDebitCardsResponseTransaction();
    }

    /**
     * Create an instance of {@link WalletCardChangeStatusResponse }
     * 
     */
    public WalletCardChangeStatusResponse createWalletCardChangeStatusResponse() {
        return new WalletCardChangeStatusResponse();
    }

    /**
     * Create an instance of {@link WalletCardChangeStatusResponseTransaction }
     * 
     */
    public WalletCardChangeStatusResponseTransaction createWalletCardChangeStatusResponseTransaction() {
        return new WalletCardChangeStatusResponseTransaction();
    }

    /**
     * Create an instance of {@link CreditCardTransactionInquiry }
     * 
     */
    public CreditCardTransactionInquiry createCreditCardTransactionInquiry() {
        return new CreditCardTransactionInquiry();
    }

    /**
     * Create an instance of {@link CreditCardTransactionInquiryRequestTransaction }
     * 
     */
    public CreditCardTransactionInquiryRequestTransaction createCreditCardTransactionInquiryRequestTransaction() {
        return new CreditCardTransactionInquiryRequestTransaction();
    }

    /**
     * Create an instance of {@link CCardStatementByEmail }
     * 
     */
    public CCardStatementByEmail createCCardStatementByEmail() {
        return new CCardStatementByEmail();
    }

    /**
     * Create an instance of {@link CCardStatementByEmailRequestTransaction }
     * 
     */
    public CCardStatementByEmailRequestTransaction createCCardStatementByEmailRequestTransaction() {
        return new CCardStatementByEmailRequestTransaction();
    }

    /**
     * Create an instance of {@link AdvanceSearch }
     * 
     */
    public AdvanceSearch createAdvanceSearch() {
        return new AdvanceSearch();
    }

    /**
     * Create an instance of {@link AdvanceSearchRequestTransaction }
     * 
     */
    public AdvanceSearchRequestTransaction createAdvanceSearchRequestTransaction() {
        return new AdvanceSearchRequestTransaction();
    }

    /**
     * Create an instance of {@link ChequeBookResponse }
     * 
     */
    public ChequeBookResponse createChequeBookResponse() {
        return new ChequeBookResponse();
    }

    /**
     * Create an instance of {@link ChequeBookResponseTransaction }
     * 
     */
    public ChequeBookResponseTransaction createChequeBookResponseTransaction() {
        return new ChequeBookResponseTransaction();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link TransactionRequestTransaction }
     * 
     */
    public TransactionRequestTransaction createTransactionRequestTransaction() {
        return new TransactionRequestTransaction();
    }

    /**
     * Create an instance of {@link WalletCardPinGenerationResponse }
     * 
     */
    public WalletCardPinGenerationResponse createWalletCardPinGenerationResponse() {
        return new WalletCardPinGenerationResponse();
    }

    /**
     * Create an instance of {@link WalletCardPinGenerationResponseTransaction }
     * 
     */
    public WalletCardPinGenerationResponseTransaction createWalletCardPinGenerationResponseTransaction() {
        return new WalletCardPinGenerationResponseTransaction();
    }

    /**
     * Create an instance of {@link WalletCardChangeStatus }
     * 
     */
    public WalletCardChangeStatus createWalletCardChangeStatus() {
        return new WalletCardChangeStatus();
    }

    /**
     * Create an instance of {@link WalletCardChangeStatusRequestTransaction }
     * 
     */
    public WalletCardChangeStatusRequestTransaction createWalletCardChangeStatusRequestTransaction() {
        return new WalletCardChangeStatusRequestTransaction();
    }

    /**
     * Create an instance of {@link DebitCardStatusChange }
     * 
     */
    public DebitCardStatusChange createDebitCardStatusChange() {
        return new DebitCardStatusChange();
    }

    /**
     * Create an instance of {@link DebitCardStatusChangeRequestTransaction }
     * 
     */
    public DebitCardStatusChangeRequestTransaction createDebitCardStatusChangeRequestTransaction() {
        return new DebitCardStatusChangeRequestTransaction();
    }

    /**
     * Create an instance of {@link CreditCardAccountPayment }
     * 
     */
    public CreditCardAccountPayment createCreditCardAccountPayment() {
        return new CreditCardAccountPayment();
    }

    /**
     * Create an instance of {@link CreditCardAccountPaymentRequestTransaction }
     * 
     */
    public CreditCardAccountPaymentRequestTransaction createCreditCardAccountPaymentRequestTransaction() {
        return new CreditCardAccountPaymentRequestTransaction();
    }

    /**
     * Create an instance of {@link StopPaymentResponse }
     * 
     */
    public StopPaymentResponse createStopPaymentResponse() {
        return new StopPaymentResponse();
    }

    /**
     * Create an instance of {@link StopPaymentResponseTransaction }
     * 
     */
    public StopPaymentResponseTransaction createStopPaymentResponseTransaction() {
        return new StopPaymentResponseTransaction();
    }

    /**
     * Create an instance of {@link TPINValidation }
     * 
     */
    public TPINValidation createTPINValidation() {
        return new TPINValidation();
    }

    /**
     * Create an instance of {@link TPINValidationRequestTransaction }
     * 
     */
    public TPINValidationRequestTransaction createTPINValidationRequestTransaction() {
        return new TPINValidationRequestTransaction();
    }

    /**
     * Create an instance of {@link FundsTransferResponse }
     * 
     */
    public FundsTransferResponse createFundsTransferResponse() {
        return new FundsTransferResponse();
    }

    /**
     * Create an instance of {@link FundsTransferResponseTransaction }
     * 
     */
    public FundsTransferResponseTransaction createFundsTransferResponseTransaction() {
        return new FundsTransferResponseTransaction();
    }

    /**
     * Create an instance of {@link BillPaymentResponse }
     * 
     */
    public BillPaymentResponse createBillPaymentResponse() {
        return new BillPaymentResponse();
    }

    /**
     * Create an instance of {@link BillPaymentResponseTransaction }
     * 
     */
    public BillPaymentResponseTransaction createBillPaymentResponseTransaction() {
        return new BillPaymentResponseTransaction();
    }

    /**
     * Create an instance of {@link WalletCardPinChangeResponse }
     * 
     */
    public WalletCardPinChangeResponse createWalletCardPinChangeResponse() {
        return new WalletCardPinChangeResponse();
    }

    /**
     * Create an instance of {@link WalletCardPinChangeResponseTransaction }
     * 
     */
    public WalletCardPinChangeResponseTransaction createWalletCardPinChangeResponseTransaction() {
        return new WalletCardPinChangeResponseTransaction();
    }

    /**
     * Create an instance of {@link TitleFetchResponse }
     * 
     */
    public TitleFetchResponse createTitleFetchResponse() {
        return new TitleFetchResponse();
    }

    /**
     * Create an instance of {@link TitleFetchResponseTransaction }
     * 
     */
    public TitleFetchResponseTransaction createTitleFetchResponseTransaction() {
        return new TitleFetchResponseTransaction();
    }

    /**
     * Create an instance of {@link GenericEformTrxn }
     * 
     */
    public GenericEformTrxn createGenericEformTrxn() {
        return new GenericEformTrxn();
    }

    /**
     * Create an instance of {@link GenericEformTrxnRequestTransaction }
     * 
     */
    public GenericEformTrxnRequestTransaction createGenericEformTrxnRequestTransaction() {
        return new GenericEformTrxnRequestTransaction();
    }

    /**
     * Create an instance of {@link BillInquiryResponse }
     * 
     */
    public BillInquiryResponse createBillInquiryResponse() {
        return new BillInquiryResponse();
    }

    /**
     * Create an instance of {@link BillInquiryResponseTransaction }
     * 
     */
    public BillInquiryResponseTransaction createBillInquiryResponseTransaction() {
        return new BillInquiryResponseTransaction();
    }

    /**
     * Create an instance of {@link LoanInquiryResponse }
     * 
     */
    public LoanInquiryResponse createLoanInquiryResponse() {
        return new LoanInquiryResponse();
    }

    /**
     * Create an instance of {@link LoanInquiryResponseTransaction }
     * 
     */
    public LoanInquiryResponseTransaction createLoanInquiryResponseTransaction() {
        return new LoanInquiryResponseTransaction();
    }

    /**
     * Create an instance of {@link DebitCardDetailsResponse }
     * 
     */
    public DebitCardDetailsResponse createDebitCardDetailsResponse() {
        return new DebitCardDetailsResponse();
    }

    /**
     * Create an instance of {@link DebitCardDetailsResponseTransaction }
     * 
     */
    public DebitCardDetailsResponseTransaction createDebitCardDetailsResponseTransaction() {
        return new DebitCardDetailsResponseTransaction();
    }

    /**
     * Create an instance of {@link BlockGDCIServiceResponse }
     * 
     */
    public BlockGDCIServiceResponse createBlockGDCIServiceResponse() {
        return new BlockGDCIServiceResponse();
    }

    /**
     * Create an instance of {@link BlockGDCIServiceResponseTransaction }
     * 
     */
    public BlockGDCIServiceResponseTransaction createBlockGDCIServiceResponseTransaction() {
        return new BlockGDCIServiceResponseTransaction();
    }

    /**
     * Create an instance of {@link SMSRegisteredUserList }
     * 
     */
    public SMSRegisteredUserList createSMSRegisteredUserList() {
        return new SMSRegisteredUserList();
    }

    /**
     * Create an instance of {@link SMSRegisteredUserListRequestTransaction }
     * 
     */
    public SMSRegisteredUserListRequestTransaction createSMSRegisteredUserListRequestTransaction() {
        return new SMSRegisteredUserListRequestTransaction();
    }

    /**
     * Create an instance of {@link CreditCardTransactionInquiryResponse }
     * 
     */
    public CreditCardTransactionInquiryResponse createCreditCardTransactionInquiryResponse() {
        return new CreditCardTransactionInquiryResponse();
    }

    /**
     * Create an instance of {@link CreditCardTransactionInquiryResponseTransaction }
     * 
     */
    public CreditCardTransactionInquiryResponseTransaction createCreditCardTransactionInquiryResponseTransaction() {
        return new CreditCardTransactionInquiryResponseTransaction();
    }

    /**
     * Create an instance of {@link OpenAccountFundsTransferResponse }
     * 
     */
    public OpenAccountFundsTransferResponse createOpenAccountFundsTransferResponse() {
        return new OpenAccountFundsTransferResponse();
    }

    /**
     * Create an instance of {@link OpenAccountFundsTransferResponseTransaction }
     * 
     */
    public OpenAccountFundsTransferResponseTransaction createOpenAccountFundsTransferResponseTransaction() {
        return new OpenAccountFundsTransferResponseTransaction();
    }

    /**
     * Create an instance of {@link TPINChangeResponse }
     * 
     */
    public TPINChangeResponse createTPINChangeResponse() {
        return new TPINChangeResponse();
    }

    /**
     * Create an instance of {@link TPINChangeResponseTransaction }
     * 
     */
    public TPINChangeResponseTransaction createTPINChangeResponseTransaction() {
        return new TPINChangeResponseTransaction();
    }

    /**
     * Create an instance of {@link BalanceInquiryResponse }
     * 
     */
    public BalanceInquiryResponse createBalanceInquiryResponse() {
        return new BalanceInquiryResponse();
    }

    /**
     * Create an instance of {@link BalanceInquiryResponseTransaction }
     * 
     */
    public BalanceInquiryResponseTransaction createBalanceInquiryResponseTransaction() {
        return new BalanceInquiryResponseTransaction();
    }

    /**
     * Create an instance of {@link EnvelopMessage }
     * 
     */
    public EnvelopMessage createEnvelopMessage() {
        return new EnvelopMessage();
    }

    /**
     * Create an instance of {@link EnvelopMessageRequestTransaction }
     * 
     */
    public EnvelopMessageRequestTransaction createEnvelopMessageRequestTransaction() {
        return new EnvelopMessageRequestTransaction();
    }

    /**
     * Create an instance of {@link ATMPINChangeResponse }
     * 
     */
    public ATMPINChangeResponse createATMPINChangeResponse() {
        return new ATMPINChangeResponse();
    }

    /**
     * Create an instance of {@link ATMPINChangeResponseTransaction }
     * 
     */
    public ATMPINChangeResponseTransaction createATMPINChangeResponseTransaction() {
        return new ATMPINChangeResponseTransaction();
    }

    /**
     * Create an instance of {@link PINValidation }
     * 
     */
    public PINValidation createPINValidation() {
        return new PINValidation();
    }

    /**
     * Create an instance of {@link PINValidationRequestTransaction }
     * 
     */
    public PINValidationRequestTransaction createPINValidationRequestTransaction() {
        return new PINValidationRequestTransaction();
    }

    /**
     * Create an instance of {@link AdvanceSearchResponse }
     * 
     */
    public AdvanceSearchResponse createAdvanceSearchResponse() {
        return new AdvanceSearchResponse();
    }

    /**
     * Create an instance of {@link AdvanceSearchResponseTransaction }
     * 
     */
    public AdvanceSearchResponseTransaction createAdvanceSearchResponseTransaction() {
        return new AdvanceSearchResponseTransaction();
    }

    /**
     * Create an instance of {@link DebitCardList }
     * 
     */
    public DebitCardList createDebitCardList() {
        return new DebitCardList();
    }

    /**
     * Create an instance of {@link DebitCardListRequestTransaction }
     * 
     */
    public DebitCardListRequestTransaction createDebitCardListRequestTransaction() {
        return new DebitCardListRequestTransaction();
    }

    /**
     * Create an instance of {@link TPINValidationResponse }
     * 
     */
    public TPINValidationResponse createTPINValidationResponse() {
        return new TPINValidationResponse();
    }

    /**
     * Create an instance of {@link TPINValidationResponseTransaction }
     * 
     */
    public TPINValidationResponseTransaction createTPINValidationResponseTransaction() {
        return new TPINValidationResponseTransaction();
    }

    /**
     * Create an instance of {@link DCardStatementByEmailResponse }
     * 
     */
    public DCardStatementByEmailResponse createDCardStatementByEmailResponse() {
        return new DCardStatementByEmailResponse();
    }

    /**
     * Create an instance of {@link DCardStatementByEmailResponseTransaction }
     * 
     */
    public DCardStatementByEmailResponseTransaction createDCardStatementByEmailResponseTransaction() {
        return new DCardStatementByEmailResponseTransaction();
    }

    /**
     * Create an instance of {@link FundsTransfer }
     * 
     */
    public FundsTransfer createFundsTransfer() {
        return new FundsTransfer();
    }

    /**
     * Create an instance of {@link FundsTransferRequestTransaction }
     * 
     */
    public FundsTransferRequestTransaction createFundsTransferRequestTransaction() {
        return new FundsTransferRequestTransaction();
    }

    /**
     * Create an instance of {@link TransactionResponse }
     * 
     */
    public TransactionResponse createTransactionResponse() {
        return new TransactionResponse();
    }

    /**
     * Create an instance of {@link TransactionResponseTransaction }
     * 
     */
    public TransactionResponseTransaction createTransactionResponseTransaction() {
        return new TransactionResponseTransaction();
    }

    /**
     * Create an instance of {@link AccountDetail }
     * 
     */
    public AccountDetail createAccountDetail() {
        return new AccountDetail();
    }

    /**
     * Create an instance of {@link AccountDetailRequestTransaction }
     * 
     */
    public AccountDetailRequestTransaction createAccountDetailRequestTransaction() {
        return new AccountDetailRequestTransaction();
    }

    /**
     * Create an instance of {@link FetchCardStatus }
     * 
     */
    public FetchCardStatus createFetchCardStatus() {
        return new FetchCardStatus();
    }

    /**
     * Create an instance of {@link FetchCardStatusRequestTransaction }
     * 
     */
    public FetchCardStatusRequestTransaction createFetchCardStatusRequestTransaction() {
        return new FetchCardStatusRequestTransaction();
    }

    /**
     * Create an instance of {@link CustomerSearch }
     * 
     */
    public CustomerSearch createCustomerSearch() {
        return new CustomerSearch();
    }

    /**
     * Create an instance of {@link CustomerSearchRequestTransaction }
     * 
     */
    public CustomerSearchRequestTransaction createCustomerSearchRequestTransaction() {
        return new CustomerSearchRequestTransaction();
    }

    /**
     * Create an instance of {@link TitleFetch }
     * 
     */
    public TitleFetch createTitleFetch() {
        return new TitleFetch();
    }

    /**
     * Create an instance of {@link TitleFetchRequestTransaction }
     * 
     */
    public TitleFetchRequestTransaction createTitleFetchRequestTransaction() {
        return new TitleFetchRequestTransaction();
    }

    /**
     * Create an instance of {@link ChargeCustomerResponse }
     * 
     */
    public ChargeCustomerResponse createChargeCustomerResponse() {
        return new ChargeCustomerResponse();
    }

    /**
     * Create an instance of {@link ChargeCustomerResponseTransaction }
     * 
     */
    public ChargeCustomerResponseTransaction createChargeCustomerResponseTransaction() {
        return new ChargeCustomerResponseTransaction();
    }

    /**
     * Create an instance of {@link DebitCardStatusChangeResponse }
     * 
     */
    public DebitCardStatusChangeResponse createDebitCardStatusChangeResponse() {
        return new DebitCardStatusChangeResponse();
    }

    /**
     * Create an instance of {@link DebitCardStatusChangeResponseTransaction }
     * 
     */
    public DebitCardStatusChangeResponseTransaction createDebitCardStatusChangeResponseTransaction() {
        return new DebitCardStatusChangeResponseTransaction();
    }

    /**
     * Create an instance of {@link UnBlockGDCIService }
     * 
     */
    public UnBlockGDCIService createUnBlockGDCIService() {
        return new UnBlockGDCIService();
    }

    /**
     * Create an instance of {@link UnBlockGDCIServiceRequestTransaction }
     * 
     */
    public UnBlockGDCIServiceRequestTransaction createUnBlockGDCIServiceRequestTransaction() {
        return new UnBlockGDCIServiceRequestTransaction();
    }

    /**
     * Create an instance of {@link BlockGDCIService }
     * 
     */
    public BlockGDCIService createBlockGDCIService() {
        return new BlockGDCIService();
    }

    /**
     * Create an instance of {@link BlockGDCIServiceRequestTransaction }
     * 
     */
    public BlockGDCIServiceRequestTransaction createBlockGDCIServiceRequestTransaction() {
        return new BlockGDCIServiceRequestTransaction();
    }

    /**
     * Create an instance of {@link GenericEformTrxnResponse }
     * 
     */
    public GenericEformTrxnResponse createGenericEformTrxnResponse() {
        return new GenericEformTrxnResponse();
    }

    /**
     * Create an instance of {@link GenericEformTrxnResponseTransaction }
     * 
     */
    public GenericEformTrxnResponseTransaction createGenericEformTrxnResponseTransaction() {
        return new GenericEformTrxnResponseTransaction();
    }

    /**
     * Create an instance of {@link MiniStatement }
     * 
     */
    public MiniStatement createMiniStatement() {
        return new MiniStatement();
    }

    /**
     * Create an instance of {@link MiniStatementRequestTransaction }
     * 
     */
    public MiniStatementRequestTransaction createMiniStatementRequestTransaction() {
        return new MiniStatementRequestTransaction();
    }

    /**
     * Create an instance of {@link CustomerDiagnosticsResponse }
     * 
     */
    public CustomerDiagnosticsResponse createCustomerDiagnosticsResponse() {
        return new CustomerDiagnosticsResponse();
    }

    /**
     * Create an instance of {@link CustomerDiagnosticsResponseTransaction }
     * 
     */
    public CustomerDiagnosticsResponseTransaction createCustomerDiagnosticsResponseTransaction() {
        return new CustomerDiagnosticsResponseTransaction();
    }

    /**
     * Create an instance of {@link StopPayment }
     * 
     */
    public StopPayment createStopPayment() {
        return new StopPayment();
    }

    /**
     * Create an instance of {@link StopPaymentRequestTransaction }
     * 
     */
    public StopPaymentRequestTransaction createStopPaymentRequestTransaction() {
        return new StopPaymentRequestTransaction();
    }

    /**
     * Create an instance of {@link MiniStatementResponse }
     * 
     */
    public MiniStatementResponse createMiniStatementResponse() {
        return new MiniStatementResponse();
    }

    /**
     * Create an instance of {@link MiniStatementResponseTransaction }
     * 
     */
    public MiniStatementResponseTransaction createMiniStatementResponseTransaction() {
        return new MiniStatementResponseTransaction();
    }

    /**
     * Create an instance of {@link DebitCardInquiry }
     * 
     */
    public DebitCardInquiry createDebitCardInquiry() {
        return new DebitCardInquiry();
    }

    /**
     * Create an instance of {@link DebitCardInquiryRequestTransaction }
     * 
     */
    public DebitCardInquiryRequestTransaction createDebitCardInquiryRequestTransaction() {
        return new DebitCardInquiryRequestTransaction();
    }

    /**
     * Create an instance of {@link DebitCardListResponse }
     * 
     */
    public DebitCardListResponse createDebitCardListResponse() {
        return new DebitCardListResponse();
    }

    /**
     * Create an instance of {@link DebitCardListResponseTransaction }
     * 
     */
    public DebitCardListResponseTransaction createDebitCardListResponseTransaction() {
        return new DebitCardListResponseTransaction();
    }

    /**
     * Create an instance of {@link CustomerCardRelationshipEnquiryResponse }
     * 
     */
    public CustomerCardRelationshipEnquiryResponse createCustomerCardRelationshipEnquiryResponse() {
        return new CustomerCardRelationshipEnquiryResponse();
    }

    /**
     * Create an instance of {@link CustomerCardRelationshipEnquiryResponseTransaction }
     * 
     */
    public CustomerCardRelationshipEnquiryResponseTransaction createCustomerCardRelationshipEnquiryResponseTransaction() {
        return new CustomerCardRelationshipEnquiryResponseTransaction();
    }

    /**
     * Create an instance of {@link ATMPINChange }
     * 
     */
    public ATMPINChange createATMPINChange() {
        return new ATMPINChange();
    }

    /**
     * Create an instance of {@link ATMPINChangeRequestTransaction }
     * 
     */
    public ATMPINChangeRequestTransaction createATMPINChangeRequestTransaction() {
        return new ATMPINChangeRequestTransaction();
    }

    /**
     * Create an instance of {@link BalanceInquiry }
     * 
     */
    public BalanceInquiry createBalanceInquiry() {
        return new BalanceInquiry();
    }

    /**
     * Create an instance of {@link BalanceInquiryRequestTransaction }
     * 
     */
    public BalanceInquiryRequestTransaction createBalanceInquiryRequestTransaction() {
        return new BalanceInquiryRequestTransaction();
    }

    /**
     * Create an instance of {@link CustomerAuthenticationRequestBody }
     * 
     */
    public CustomerAuthenticationRequestBody createCustomerAuthenticationRequestBody() {
        return new CustomerAuthenticationRequestBody();
    }

    /**
     * Create an instance of {@link TitleFetchRequestBody }
     * 
     */
    public TitleFetchRequestBody createTitleFetchRequestBody() {
        return new TitleFetchRequestBody();
    }

    /**
     * Create an instance of {@link BlockGDCIServiceResponseBody }
     * 
     */
    public BlockGDCIServiceResponseBody createBlockGDCIServiceResponseBody() {
        return new BlockGDCIServiceResponseBody();
    }

    /**
     * Create an instance of {@link GenericEformTrxnRequestBody }
     * 
     */
    public GenericEformTrxnRequestBody createGenericEformTrxnRequestBody() {
        return new GenericEformTrxnRequestBody();
    }

    /**
     * Create an instance of {@link BlockAccountDetail }
     * 
     */
    public BlockAccountDetail createBlockAccountDetail() {
        return new BlockAccountDetail();
    }

    /**
     * Create an instance of {@link SendSMSRequestBody }
     * 
     */
    public SendSMSRequestBody createSendSMSRequestBody() {
        return new SendSMSRequestBody();
    }

    /**
     * Create an instance of {@link DebitCardInquiryResponseBody }
     * 
     */
    public DebitCardInquiryResponseBody createDebitCardInquiryResponseBody() {
        return new DebitCardInquiryResponseBody();
    }

    /**
     * Create an instance of {@link AccountDetailResponseBody }
     * 
     */
    public AccountDetailResponseBody createAccountDetailResponseBody() {
        return new AccountDetailResponseBody();
    }

    /**
     * Create an instance of {@link DebitCardBlock }
     * 
     */
    public DebitCardBlock createDebitCardBlock() {
        return new DebitCardBlock();
    }

    /**
     * Create an instance of {@link BlockDebtMiniStatement }
     * 
     */
    public BlockDebtMiniStatement createBlockDebtMiniStatement() {
        return new BlockDebtMiniStatement();
    }

    /**
     * Create an instance of {@link DebitCardDetailsRequestBody }
     * 
     */
    public DebitCardDetailsRequestBody createDebitCardDetailsRequestBody() {
        return new DebitCardDetailsRequestBody();
    }

    /**
     * Create an instance of {@link CustomerSearchResponseBody }
     * 
     */
    public CustomerSearchResponseBody createCustomerSearchResponseBody() {
        return new CustomerSearchResponseBody();
    }

    /**
     * Create an instance of {@link FetchCardStatusResponseBody }
     * 
     */
    public FetchCardStatusResponseBody createFetchCardStatusResponseBody() {
        return new FetchCardStatusResponseBody();
    }

    /**
     * Create an instance of {@link BillPaymentResponseBody }
     * 
     */
    public BillPaymentResponseBody createBillPaymentResponseBody() {
        return new BillPaymentResponseBody();
    }

    /**
     * Create an instance of {@link BlockCustomerDiagnostics }
     * 
     */
    public BlockCustomerDiagnostics createBlockCustomerDiagnostics() {
        return new BlockCustomerDiagnostics();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link CCardStatementByEmailRequestBody }
     * 
     */
    public CCardStatementByEmailRequestBody createCCardStatementByEmailRequestBody() {
        return new CCardStatementByEmailRequestBody();
    }

    /**
     * Create an instance of {@link BlockCCAppsInfo }
     * 
     */
    public BlockCCAppsInfo createBlockCCAppsInfo() {
        return new BlockCCAppsInfo();
    }

    /**
     * Create an instance of {@link CCAccountInquiryWithCustomerInfoRequestBody }
     * 
     */
    public CCAccountInquiryWithCustomerInfoRequestBody createCCAccountInquiryWithCustomerInfoRequestBody() {
        return new CCAccountInquiryWithCustomerInfoRequestBody();
    }

    /**
     * Create an instance of {@link SMSRegisteredUserListResponseBody }
     * 
     */
    public SMSRegisteredUserListResponseBody createSMSRegisteredUserListResponseBody() {
        return new SMSRegisteredUserListResponseBody();
    }

    /**
     * Create an instance of {@link CardDetailResponseTransactionBody }
     * 
     */
    public CardDetailResponseTransactionBody createCardDetailResponseTransactionBody() {
        return new CardDetailResponseTransactionBody();
    }

    /**
     * Create an instance of {@link StopPaymentRequestBody }
     * 
     */
    public StopPaymentRequestBody createStopPaymentRequestBody() {
        return new StopPaymentRequestBody();
    }

    /**
     * Create an instance of {@link DebitCardInquiryRequestBody }
     * 
     */
    public DebitCardInquiryRequestBody createDebitCardInquiryRequestBody() {
        return new DebitCardInquiryRequestBody();
    }

    /**
     * Create an instance of {@link FundsTransferRequestBody }
     * 
     */
    public FundsTransferRequestBody createFundsTransferRequestBody() {
        return new FundsTransferRequestBody();
    }

    /**
     * Create an instance of {@link DebitCardMiniStatementRequestBody }
     * 
     */
    public DebitCardMiniStatementRequestBody createDebitCardMiniStatementRequestBody() {
        return new DebitCardMiniStatementRequestBody();
    }

    /**
     * Create an instance of {@link TitleFetchResponseBody }
     * 
     */
    public TitleFetchResponseBody createTitleFetchResponseBody() {
        return new TitleFetchResponseBody();
    }

    /**
     * Create an instance of {@link BlockCCTranInq }
     * 
     */
    public BlockCCTranInq createBlockCCTranInq() {
        return new BlockCCTranInq();
    }

    /**
     * Create an instance of {@link AdvanceSearchRequestBody }
     * 
     */
    public AdvanceSearchRequestBody createAdvanceSearchRequestBody() {
        return new AdvanceSearchRequestBody();
    }

    /**
     * Create an instance of {@link ChargeCustomerRequestBody }
     * 
     */
    public ChargeCustomerRequestBody createChargeCustomerRequestBody() {
        return new ChargeCustomerRequestBody();
    }

    /**
     * Create an instance of {@link BlockCustCardRelationShip }
     * 
     */
    public BlockCustCardRelationShip createBlockCustCardRelationShip() {
        return new BlockCustCardRelationShip();
    }

    /**
     * Create an instance of {@link EnvelopMessageRequestBody }
     * 
     */
    public EnvelopMessageRequestBody createEnvelopMessageRequestBody() {
        return new EnvelopMessageRequestBody();
    }

    /**
     * Create an instance of {@link DebitCardDetailsResponseBody }
     * 
     */
    public DebitCardDetailsResponseBody createDebitCardDetailsResponseBody() {
        return new DebitCardDetailsResponseBody();
    }

    /**
     * Create an instance of {@link UnBlockGDCIServiceResponseBody }
     * 
     */
    public UnBlockGDCIServiceResponseBody createUnBlockGDCIServiceResponseBody() {
        return new UnBlockGDCIServiceResponseBody();
    }

    /**
     * Create an instance of {@link DebitCardMiniStatementResponseBody }
     * 
     */
    public DebitCardMiniStatementResponseBody createDebitCardMiniStatementResponseBody() {
        return new DebitCardMiniStatementResponseBody();
    }

    /**
     * Create an instance of {@link BlockGDCIServiceRequestBody }
     * 
     */
    public BlockGDCIServiceRequestBody createBlockGDCIServiceRequestBody() {
        return new BlockGDCIServiceRequestBody();
    }

    /**
     * Create an instance of {@link CreditCardTransactionInquiryResponseBody }
     * 
     */
    public CreditCardTransactionInquiryResponseBody createCreditCardTransactionInquiryResponseBody() {
        return new CreditCardTransactionInquiryResponseBody();
    }

    /**
     * Create an instance of {@link ATMPINChangeRequestBody }
     * 
     */
    public ATMPINChangeRequestBody createATMPINChangeRequestBody() {
        return new ATMPINChangeRequestBody();
    }

    /**
     * Create an instance of {@link UnBlockGDCIServiceRequestBody }
     * 
     */
    public UnBlockGDCIServiceRequestBody createUnBlockGDCIServiceRequestBody() {
        return new UnBlockGDCIServiceRequestBody();
    }

    /**
     * Create an instance of {@link TransactionRequestBody }
     * 
     */
    public TransactionRequestBody createTransactionRequestBody() {
        return new TransactionRequestBody();
    }

    /**
     * Create an instance of {@link FetchCardStatusRequestBody }
     * 
     */
    public FetchCardStatusRequestBody createFetchCardStatusRequestBody() {
        return new FetchCardStatusRequestBody();
    }

    /**
     * Create an instance of {@link OpenAccountFundsTransferResponseBody }
     * 
     */
    public OpenAccountFundsTransferResponseBody createOpenAccountFundsTransferResponseBody() {
        return new OpenAccountFundsTransferResponseBody();
    }

    /**
     * Create an instance of {@link MiniStatementResponseBody }
     * 
     */
    public MiniStatementResponseBody createMiniStatementResponseBody() {
        return new MiniStatementResponseBody();
    }

    /**
     * Create an instance of {@link BlockCCardsInfo }
     * 
     */
    public BlockCCardsInfo createBlockCCardsInfo() {
        return new BlockCCardsInfo();
    }

    /**
     * Create an instance of {@link BlockSMSRegList }
     * 
     */
    public BlockSMSRegList createBlockSMSRegList() {
        return new BlockSMSRegList();
    }

    /**
     * Create an instance of {@link TransactionResponseBody }
     * 
     */
    public TransactionResponseBody createTransactionResponseBody() {
        return new TransactionResponseBody();
    }

    /**
     * Create an instance of {@link BlockCCTranDetails }
     * 
     */
    public BlockCCTranDetails createBlockCCTranDetails() {
        return new BlockCCTranDetails();
    }

    /**
     * Create an instance of {@link FundsTransferResponseBody }
     * 
     */
    public FundsTransferResponseBody createFundsTransferResponseBody() {
        return new FundsTransferResponseBody();
    }

    /**
     * Create an instance of {@link BillPaymentRequestBody }
     * 
     */
    public BillPaymentRequestBody createBillPaymentRequestBody() {
        return new BillPaymentRequestBody();
    }

    /**
     * Create an instance of {@link CreditCardTransactionInquiryRequestBody }
     * 
     */
    public CreditCardTransactionInquiryRequestBody createCreditCardTransactionInquiryRequestBody() {
        return new CreditCardTransactionInquiryRequestBody();
    }

    /**
     * Create an instance of {@link StopPaymentResponseBody }
     * 
     */
    public StopPaymentResponseBody createStopPaymentResponseBody() {
        return new StopPaymentResponseBody();
    }

    /**
     * Create an instance of {@link ATMPINGenerationRequestBody }
     * 
     */
    public ATMPINGenerationRequestBody createATMPINGenerationRequestBody() {
        return new ATMPINGenerationRequestBody();
    }

    /**
     * Create an instance of {@link MiniStatementBlock }
     * 
     */
    public MiniStatementBlock createMiniStatementBlock() {
        return new MiniStatementBlock();
    }

    /**
     * Create an instance of {@link DebitCardStatusChangeRequestBody }
     * 
     */
    public DebitCardStatusChangeRequestBody createDebitCardStatusChangeRequestBody() {
        return new DebitCardStatusChangeRequestBody();
    }

    /**
     * Create an instance of {@link LoanInquiryRequestBody }
     * 
     */
    public LoanInquiryRequestBody createLoanInquiryRequestBody() {
        return new LoanInquiryRequestBody();
    }

    /**
     * Create an instance of {@link CustomerCardRelationshipEnquiryResponseBody }
     * 
     */
    public CustomerCardRelationshipEnquiryResponseBody createCustomerCardRelationshipEnquiryResponseBody() {
        return new CustomerCardRelationshipEnquiryResponseBody();
    }

    /**
     * Create an instance of {@link PINValidationRequestBody }
     * 
     */
    public PINValidationRequestBody createPINValidationRequestBody() {
        return new PINValidationRequestBody();
    }

    /**
     * Create an instance of {@link BalanceInquiryRequestBody }
     * 
     */
    public BalanceInquiryRequestBody createBalanceInquiryRequestBody() {
        return new BalanceInquiryRequestBody();
    }

    /**
     * Create an instance of {@link BlockStopPayment }
     * 
     */
    public BlockStopPayment createBlockStopPayment() {
        return new BlockStopPayment();
    }

    /**
     * Create an instance of {@link BillInquiryRequestBody }
     * 
     */
    public BillInquiryRequestBody createBillInquiryRequestBody() {
        return new BillInquiryRequestBody();
    }

    /**
     * Create an instance of {@link OpenAccountFundsTransferRequestBody }
     * 
     */
    public OpenAccountFundsTransferRequestBody createOpenAccountFundsTransferRequestBody() {
        return new OpenAccountFundsTransferRequestBody();
    }

    /**
     * Create an instance of {@link TPINValidationRequestBody }
     * 
     */
    public TPINValidationRequestBody createTPINValidationRequestBody() {
        return new TPINValidationRequestBody();
    }

    /**
     * Create an instance of {@link DCardStatementByEmailRequestBody }
     * 
     */
    public DCardStatementByEmailRequestBody createDCardStatementByEmailRequestBody() {
        return new DCardStatementByEmailRequestBody();
    }

    /**
     * Create an instance of {@link CreditCardAccountPaymentRequestBody }
     * 
     */
    public CreditCardAccountPaymentRequestBody createCreditCardAccountPaymentRequestBody() {
        return new CreditCardAccountPaymentRequestBody();
    }

    /**
     * Create an instance of {@link CCAccountInquiryWithCustomerInfoResponseBody }
     * 
     */
    public CCAccountInquiryWithCustomerInfoResponseBody createCCAccountInquiryWithCustomerInfoResponseBody() {
        return new CCAccountInquiryWithCustomerInfoResponseBody();
    }

    /**
     * Create an instance of {@link TPINChangeRequestBody }
     * 
     */
    public TPINChangeRequestBody createTPINChangeRequestBody() {
        return new TPINChangeRequestBody();
    }

    /**
     * Create an instance of {@link CreditCardAccountPaymentResponseBody }
     * 
     */
    public CreditCardAccountPaymentResponseBody createCreditCardAccountPaymentResponseBody() {
        return new CreditCardAccountPaymentResponseBody();
    }

    /**
     * Create an instance of {@link BillInquiryResponseBody }
     * 
     */
    public BillInquiryResponseBody createBillInquiryResponseBody() {
        return new BillInquiryResponseBody();
    }

    /**
     * Create an instance of {@link MiniStatementRequestBody }
     * 
     */
    public MiniStatementRequestBody createMiniStatementRequestBody() {
        return new MiniStatementRequestBody();
    }

    /**
     * Create an instance of {@link TPINGenerationRequestBody }
     * 
     */
    public TPINGenerationRequestBody createTPINGenerationRequestBody() {
        return new TPINGenerationRequestBody();
    }

    /**
     * Create an instance of {@link LoanInquiryResponseBody }
     * 
     */
    public LoanInquiryResponseBody createLoanInquiryResponseBody() {
        return new LoanInquiryResponseBody();
    }

    /**
     * Create an instance of {@link ChequeBookRequestBody }
     * 
     */
    public ChequeBookRequestBody createChequeBookRequestBody() {
        return new ChequeBookRequestBody();
    }

    /**
     * Create an instance of {@link BalanceInquiryResponseBody }
     * 
     */
    public BalanceInquiryResponseBody createBalanceInquiryResponseBody() {
        return new BalanceInquiryResponseBody();
    }

    /**
     * Create an instance of {@link DebitCardListResponseBody }
     * 
     */
    public DebitCardListResponseBody createDebitCardListResponseBody() {
        return new DebitCardListResponseBody();
    }

    /**
     * Create an instance of {@link BlockAdvanceSearch }
     * 
     */
    public BlockAdvanceSearch createBlockAdvanceSearch() {
        return new BlockAdvanceSearch();
    }

    /**
     * Create an instance of {@link CreditCardRelationshipInquiryResponseBody }
     * 
     */
    public CreditCardRelationshipInquiryResponseBody createCreditCardRelationshipInquiryResponseBody() {
        return new CreditCardRelationshipInquiryResponseBody();
    }

    /**
     * Create an instance of {@link BlockRelationshipToMultipleDebitCards }
     * 
     */
    public BlockRelationshipToMultipleDebitCards createBlockRelationshipToMultipleDebitCards() {
        return new BlockRelationshipToMultipleDebitCards();
    }

}
