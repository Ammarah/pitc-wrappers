
package com.avanzasolutions.bafl.ivrpronet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitCardDetailsResult" type="{http://www.avanzasolutions.com/BAFL/IVRPronet/}DebitCardDetailsResponseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "debitCardDetailsResult"
})
@XmlRootElement(name = "DebitCardDetailsResponse")
public class DebitCardDetailsResponse {

    @XmlElement(name = "DebitCardDetailsResult")
    protected DebitCardDetailsResponseTransaction debitCardDetailsResult;

    /**
     * Gets the value of the debitCardDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link DebitCardDetailsResponseTransaction }
     *     
     */
    public DebitCardDetailsResponseTransaction getDebitCardDetailsResult() {
        return debitCardDetailsResult;
    }

    /**
     * Sets the value of the debitCardDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitCardDetailsResponseTransaction }
     *     
     */
    public void setDebitCardDetailsResult(DebitCardDetailsResponseTransaction value) {
        this.debitCardDetailsResult = value;
    }

}
