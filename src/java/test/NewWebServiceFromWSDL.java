/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import javax.jws.WebService;

/**
 *
 * @author Faran
 */
@WebService(serviceName = "Service1", portName = "Service1Soap", endpointInterface = "com.avanzasolutions.bafl.ivrpronet.Service1Soap", targetNamespace = "http://www.avanzasolutions.com/BAFL/IVRPronet/", wsdlLocation = "WEB-INF/wsdl/NewWebServiceFromWSDL/IVRPronetWsdl.wsdl")
public class NewWebServiceFromWSDL {

    public com.avanzasolutions.bafl.ivrpronet.BalanceInquiryResponseTransaction balanceInquiry(com.avanzasolutions.bafl.ivrpronet.BalanceInquiryRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.ChequeBookResponseTransaction chequeBook(com.avanzasolutions.bafl.ivrpronet.ChequeBookRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.MiniStatementResponseTransaction miniStatement(com.avanzasolutions.bafl.ivrpronet.MiniStatementRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.FundsTransferResponseTransaction fundsTransfer(com.avanzasolutions.bafl.ivrpronet.FundsTransferRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.DebitCardStatusChangeResponseTransaction debitCardStatusChange(com.avanzasolutions.bafl.ivrpronet.DebitCardStatusChangeRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.DebitCardListResponseTransaction debitCardList(com.avanzasolutions.bafl.ivrpronet.DebitCardListRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CustomerCardRelationshipEnquiryResponseTransaction customerCardRelationshipEnquiry(com.avanzasolutions.bafl.ivrpronet.CustomerCardRelationshipEnquiryRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.AccountDetailResponseTransaction accountDetail(com.avanzasolutions.bafl.ivrpronet.AccountDetailRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.StopPaymentResponseTransaction stopPayment(com.avanzasolutions.bafl.ivrpronet.StopPaymentRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.DebitCardDetailsResponseTransaction debitCardDetails(com.avanzasolutions.bafl.ivrpronet.DebitCardDetailsRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.DebitCardMiniStatementResponseTransaction debitCardMiniStatement(com.avanzasolutions.bafl.ivrpronet.DebitCardMiniStatementRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CustomerSearchResponseTransaction customerSearch(com.avanzasolutions.bafl.ivrpronet.CustomerSearchRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.ChargeCustomerResponseTransaction chargeCustomer(com.avanzasolutions.bafl.ivrpronet.ChargeCustomerRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.AdvanceSearchResponseTransaction advanceSearch(com.avanzasolutions.bafl.ivrpronet.AdvanceSearchRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.SendSMSResponseTransaction sendSMS(com.avanzasolutions.bafl.ivrpronet.SendSMSRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.EnvelopMessageResponseTransaction envelopMessage(com.avanzasolutions.bafl.ivrpronet.EnvelopMessageRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.TitleFetchResponseTransaction titleFetch(com.avanzasolutions.bafl.ivrpronet.TitleFetchRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.GenericEformTrxnResponseTransaction genericEformTrxn(com.avanzasolutions.bafl.ivrpronet.GenericEformTrxnRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.SMSRegisteredUserListResponseTransaction smsRegisteredUserList(com.avanzasolutions.bafl.ivrpronet.SMSRegisteredUserListRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.FetchCardStatusResponseTransaction fetchCardStatus(com.avanzasolutions.bafl.ivrpronet.FetchCardStatusRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.DebitCardInquiryResponseTransaction debitCardInquiry(com.avanzasolutions.bafl.ivrpronet.DebitCardInquiryRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CreditCardRelationshipInquiryResponseTransaction creditCardRelationshipInquiry(com.avanzasolutions.bafl.ivrpronet.CreditCardRelationshipInquiryRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.BlockGDCIServiceResponseTransaction blockGDCIService(com.avanzasolutions.bafl.ivrpronet.BlockGDCIServiceRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.UnBlockGDCIServiceResponseTransaction unBlockGDCIService(com.avanzasolutions.bafl.ivrpronet.UnBlockGDCIServiceRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CreditCardTransactionInquiryResponseTransaction creditCardTransactionInquiry(com.avanzasolutions.bafl.ivrpronet.CreditCardTransactionInquiryRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CCAccountInquiryWithCustomerInfoResponseTransaction ccAccountInquiryWithCustomerInfo(com.avanzasolutions.bafl.ivrpronet.CCAccountInquiryWithCustomerInfoRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CreditCardAccountPaymentResponseTransaction creditCardAccountPayment(com.avanzasolutions.bafl.ivrpronet.CreditCardAccountPaymentRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.TransactionResponseTransaction transaction(com.avanzasolutions.bafl.ivrpronet.TransactionRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.BillInquiryResponseTransaction billInquiry(com.avanzasolutions.bafl.ivrpronet.BillInquiryRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.BillPaymentResponseTransaction billPayment(com.avanzasolutions.bafl.ivrpronet.BillPaymentRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.LoanInquiryResponseTransaction loanInquiry(com.avanzasolutions.bafl.ivrpronet.LoanInquiryRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.OpenAccountFundsTransferResponseTransaction openAccountFundsTransfer(com.avanzasolutions.bafl.ivrpronet.OpenAccountFundsTransferRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.ATMPINGenerationResponseTransaction atmpinGeneration(com.avanzasolutions.bafl.ivrpronet.ATMPINGenerationRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.TPINGenerationResponseTransaction tpinGeneration(com.avanzasolutions.bafl.ivrpronet.TPINGenerationRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.ATMPINChangeResponseTransaction atmpinChange(com.avanzasolutions.bafl.ivrpronet.ATMPINChangeRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.TPINChangeResponseTransaction tpinChange(com.avanzasolutions.bafl.ivrpronet.TPINChangeRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.PINValidationResponseTransaction pinValidation(com.avanzasolutions.bafl.ivrpronet.PINValidationRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.TPINValidationResponseTransaction tpinValidation(com.avanzasolutions.bafl.ivrpronet.TPINValidationRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.DCardStatementByEmailResponseTransaction dCardStatementByEmail(com.avanzasolutions.bafl.ivrpronet.DCardStatementByEmailRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CCardStatementByEmailResponseTransaction cCardStatementByEmail(com.avanzasolutions.bafl.ivrpronet.CCardStatementByEmailRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CustomerAuthenticationResponseTransaction customerAuthentication(com.avanzasolutions.bafl.ivrpronet.CustomerAuthenticationRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.RelationshipToMultipleDebitCardsResponseTransaction relationshipToMultipleDebitCards(com.avanzasolutions.bafl.ivrpronet.RelationshipToMultipleDebitCardsRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CardDetailResponseTransaction cardDetail(com.avanzasolutions.bafl.ivrpronet.CardDetailRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.CustomerDiagnosticsResponseTransaction customerDiagnostics(com.avanzasolutions.bafl.ivrpronet.CustomerDiagnosticsRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.WalletCardChangeStatusResponseTransaction walletCardChangeStatus(com.avanzasolutions.bafl.ivrpronet.WalletCardChangeStatusRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.WalletCardPinGenerationResponseTransaction walletCardPinGeneration(com.avanzasolutions.bafl.ivrpronet.WalletCardPinGenerationRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public com.avanzasolutions.bafl.ivrpronet.WalletCardPinChangeResponseTransaction walletCardPinChange(com.avanzasolutions.bafl.ivrpronet.WalletCardPinChangeRequestTransaction request) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Web service operation
     */
    public String operation() {
        //TODO write your implementation code here:
        return null;
    }
    
}
