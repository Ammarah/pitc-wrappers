package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Timestamp;
public class FileHandler {

    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private String IsPriorityCustomer;
    private String GetCustomerDetails;
    private String ChangeStatus;
    private String create;
    private String CheckCustomerOpenTickets;
    private String CheckCustomerResolvedTickets;
    private String SetFeedBack;
    private String GetFeedBack;
    private String passportStatus;
    private String BillandLoadshedingInfo;
    private String ComplaintStatus;
    private String RegisterComplaint;
    final String DATA_DIRECTORY = "WSURLS";
    private final String filename = "LMKTWS.txt";
    private String Filepath = "";

    public FileHandler(String Path) {
        Filepath = Path+"\\" + DATA_DIRECTORY + "\\" + filename;
    }

    public void WSURL() {
       
       // VxmlServlet.logger.info("Working Directory In FileHandler Class========================================================================" + Filepath + "\t \t \t" + new Timestamp(System.currentTimeMillis()));
        String toReturn = null;
        BufferedReader br = null;
        try {
            //VxmlServlet.logger.info("WSURL Method () is called IN FileHandler Class..." + "\t \t \t" + new Timestamp(System.currentTimeMillis()));
            br = new BufferedReader(new FileReader(Filepath));
            String line = br.readLine();
            while (line != null) {
               // VxmlServlet.logger.info("line read: " + line);
                if (line.startsWith("IsPriorityCustomer")) {
                    IsPriorityCustomer = line.substring(line.indexOf("=") + 1);
                } else if (line.startsWith("GetCustomerDetails")) {
                    GetCustomerDetails = line.substring(line.indexOf("=") + 1);
                } else if (line.startsWith("ChangeStatus")) {
                    ChangeStatus = line.substring(line.indexOf("=") + 1);
                } else if (line.startsWith("create")) {
                    create = line.substring(line.indexOf("=") + 1);
                } else if (line.startsWith("CheckCustomerOpenTickets")) {
                    CheckCustomerOpenTickets = line.substring(line.indexOf("=") + 1);
                } else if (line.startsWith("CheckCustomerResolvedTickets")) {
                    CheckCustomerResolvedTickets = line.substring(line.indexOf("=") + 1);
                } else if (line.startsWith("SetFeedBack")) {
                    SetFeedBack = line.substring(line.indexOf("=") + 1);
                }
                else if (line.startsWith("GetFeedBack")) {
                    GetFeedBack = line.substring(line.indexOf("=") + 1);
                }
                else if(line.startsWith("passportStatus")){
                    passportStatus=line.substring(line.indexOf("=") + 1);
                }
                else if(line.startsWith("getBillandLoadsheddingInformation")){
                    BillandLoadshedingInfo=line.substring(line.indexOf("=") + 1);
                }
                else if(line.startsWith("getComplaintStatus")){
                    ComplaintStatus=line.substring(line.indexOf("=") + 1);
                }
                else if(line.startsWith("RegisterComplaint")){
                    RegisterComplaint=line.substring(line.indexOf("=") + 1);
                }
                line = br.readLine();
            }
            br.close();
        } catch (Exception ex) {
          //  VxmlServlet.logger.info("Error in DB info: " + ex.getMessage());
        }
        try {
            br.close();
        } catch (Exception ex) {
        }

    }

    public String IsPriorityCustomer() {
        return IsPriorityCustomer;
    }

    public String GetCustomerDetails() {
        return GetCustomerDetails;
    }

    public String ChangeStatus() {
        return ChangeStatus;
    }

    public String create() {
        return create;
    }

    public String CheckCustomerOpenTickets() {
        return CheckCustomerOpenTickets;
    }

    public String CheckCustomerResolvedTickets() {
        return CheckCustomerResolvedTickets;
    }

    public String SetFeedBack() {
        return SetFeedBack;
    }
    public String GetFeedBack(){
    return GetFeedBack;
    }
    public String getPassportStatus(){
        return passportStatus;
    }
    public String getBillandLoadshedingInfo(){
        return BillandLoadshedingInfo;
    }
    public String getComplaintStatus(){
        return ComplaintStatus;
    }
    public String getRegisterComplaint(){
        return RegisterComplaint;
    }
}
