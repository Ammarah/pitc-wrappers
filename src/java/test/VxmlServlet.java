package test;

import com.avanzasolutions.bafl.ivrpronet.ATMPINChangeRequestBody;
import com.avanzasolutions.bafl.ivrpronet.ATMPINChangeRequestTransaction;
import com.avanzasolutions.bafl.ivrpronet.ATMPINGenerationRequestBody;
import com.avanzasolutions.bafl.ivrpronet.ATMPINGenerationRequestTransaction;
import com.avanzasolutions.bafl.ivrpronet.PINValidationRequestBody;
import com.avanzasolutions.bafl.ivrpronet.PINValidationRequestTransaction;
import com.avanzasolutions.bafl.ivrpronet.PINValidationResponse;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class VxmlServlet extends HttpServlet {

    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        System.setProperty("rootPath", context.getRealPath("/"));
    }

    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static Logger logger = Logger.getLogger(VxmlServlet.class);

    ;
        
    

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String PathInfo = getServletContext().getRealPath("");
        // logger.info("PathInfo : " + PathInfo);
        FileHandler fh = new FileHandler(PathInfo);
        fh.WSURL();
        String output = "";
        String jsonOutput = "";
        JSONObject json = null;
        RequestDispatcher rd = null;
        int timeout = 20000;

        String serviceName = request.getParameter("session.sce.serviceName");
        logger.info("ServiceName Send By IVR : " + serviceName);
        if (serviceName.equals("RegisterComplaint")) {
            String WSURL = "http://172.16.100.29/api/complaint_store_ivr?";
            JSONArray ja = new JSONArray();
            logger.info("WSURL FOR RegisterComplaint:" + WSURL);
            String refNo = request.getParameter("session.sce.refnumber");
            String type = request.getParameter("session.sce.typeID");
            String nature = request.getParameter("session.sce.natureID");
            String source = request.getParameter("session.sce.sourceID");
            logger.info("RefrenceNumber Send By IVR In RegisterComplaint :" + refNo);
            logger.info("TypeID Send By IVR In RegisterComplaint :" + type);
            logger.info("NatureID Send By IVR In RegisterComplaint :" + nature);
            logger.info("SourceID Send By IVR In RegisterComplaint :" + source);
            try {
                URL url = new URL(WSURL + "ref_no=" + refNo + "&type=" + type + "&source=" + source + "&nature=" + nature);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                System.out.println(conn.getResponseMessage());
                if (conn.getResponseMessage().equalsIgnoreCase("ok")) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
                    while ((output = br.readLine()) != null) {
                        if (output != null) {
                            jsonOutput += output;
                        }
                    }
                    logger.info("Response Code: " + conn.getResponseCode());
                    conn.disconnect();
                    JSONParser parser = new JSONParser();
                    json = (JSONObject) parser.parse(jsonOutput);
                    ja = (JSONArray) json.get("REGISTER_COMPLAINT");
                    logger.info("JSON Object: \n" + ja.get(0).toString());
                    json = (JSONObject) ja.get(0);
                    output = json.get("STATUS").toString();
                } else {
                    logger.info("Response Code: " + conn.getResponseCode());
                    output = "FALSE";
                }
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            logger.info("Status :" + output);
            request.setAttribute("status", output);

            rd = request.getRequestDispatcher("RegisterComplaint.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("getBillInformation")) {
            String WSURL = "http://172.16.100.29/api/autoreply_api";
            JSONArray ja = new JSONArray();
            logger.info("WSURL FOR getBillInformation:" + WSURL);
            String refNo = request.getParameter("session.sce.refnumber");
            logger.info("Number Send By IVR In getBillInformation :" + refNo);
            try {
                URL url = new URL(WSURL + "/" + refNo + "/2");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                System.out.println(conn.getResponseMessage());
                if (conn.getResponseMessage().equalsIgnoreCase("ok")) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
                    while ((output = br.readLine()) != null) {
                        if (output != null) {
                            jsonOutput += output;
                        }
                    }
                    logger.info("Response Code: " + conn.getResponseCode());
                    conn.disconnect();
                    JSONParser parser = new JSONParser();
                    ja = (JSONArray) parser.parse(jsonOutput);
                    logger.info("JSON Object: \n" + ja.get(0).toString());
                    json = (JSONObject) ja.get(0);
                    output = json.get("STATUS").toString();
                } else {
                    logger.info("Response Code: " + conn.getResponseCode());
                    output = "FALSE";
                }
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            logger.info("Status :" + output);
            request.setAttribute("status", output);

            rd = request.getRequestDispatcher("getBillInformation.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("getLoadshedingInformation")) {
            String WSURL = "http://172.16.100.29/api/autoreply_api";
            JSONArray ja = new JSONArray();
            logger.info("WSURL FOR getLoadshedingInformation:" + WSURL);
            String refNo = request.getParameter("session.sce.refnumber");
            logger.info("Number Send By IVR In getLoadshedingInformation :" + refNo);
            try {
                URL url = new URL(WSURL + "/" + refNo + "/1");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                System.out.println(conn.getResponseMessage());
                if (conn.getResponseMessage().equalsIgnoreCase("ok")) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
                    while ((output = br.readLine()) != null) {
                        if (output != null) {
                            jsonOutput += output;
                        }
                    }
                    logger.info("Response Code: " + conn.getResponseCode());
                    conn.disconnect();
                    JSONParser parser = new JSONParser();
                    ja = (JSONArray) parser.parse(jsonOutput);
                    logger.info("JSON Object: \n" + ja.get(0).toString());
                    json = (JSONObject) ja.get(0);
                    output = json.get("STATUS").toString();
                } else {
                    logger.info("Response Code: " + conn.getResponseCode());
                    output = "FALSE";
                }
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            logger.info("Status :" + output);
            request.setAttribute("status", output);

            rd = request.getRequestDispatcher("getLoadshedingInformation.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("getComplaintStatus")) {
            String WSURL = "http://172.16.100.29/api/complaints/status";
            JSONArray ja = new JSONArray();
            logger.info("WSURL FOR getComplaintStatus:" + WSURL);
            String ticket = request.getParameter("session.sce.ticket");
            logger.info("Number Send By IVR In getComplaintStatus :" + ticket);
            try {
                URL url = new URL(WSURL + "/" + ticket);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                System.out.println(conn.getResponseMessage());
                if (conn.getResponseMessage().equalsIgnoreCase("ok")) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
                    while ((output = br.readLine()) != null) {
                        if (output != null) {
                            jsonOutput += output;
                        }
                    }
                    logger.info("Response Code: " + conn.getResponseCode());
                    conn.disconnect();
                    JSONParser parser = new JSONParser();
                    ja = (JSONArray) parser.parse(jsonOutput);
                    logger.info("JSON Object: \n" + ja.get(0).toString());
                    json = (JSONObject) ja.get(0);
                    if (json.get("STATUS").toString().equalsIgnoreCase("NULL")) {
                        output = "Invalid";
                    } else {
                        output = json.get("STATUS").toString();
                    }
                } else {
                    logger.info("Response Code: " + conn.getResponseCode());
                    output = "FALSE";
                }
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            logger.info("Status :" + output);
            request.setAttribute("status", output);
            rd = request.getRequestDispatcher("getComplaintStatus.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("isVipNumber")) {
             String WSURL = "http://172.16.100.29/api/contact";
//            String WSURL = "http://ccms.pitc.com.pk/api/contact";
            //JSONArray ja = new JSONArray();
            logger.info("WSURL FOR isVipNumber:" + WSURL);
            String mobNo = request.getParameter("session.sce.mobileno");
            logger.info("Mobile Number Send By IVR In isVipNumber :" + mobNo);
            try {
                URL url = new URL(WSURL + "/" + mobNo);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                System.out.println(conn.getResponseMessage());
                if (conn.getResponseMessage().equalsIgnoreCase("ok")) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
                    while ((output = br.readLine()) != null) {
                        if (output != null) {
                            jsonOutput += output;
                        }
                    }
                    logger.info("Response Code: " + conn.getResponseCode());
                    conn.disconnect();
                    JSONParser parser = new JSONParser();
                    json = (JSONObject) parser.parse(jsonOutput);
                    JSONObject js = (JSONObject) json.get("type");
                   
                    if (js.get("isvip").toString().equals("yes")) {
                        output = "TRUE";
                        }
                    else{
                        output="FALSE";
                    }

                } else {
                    logger.info("Response Code: " + conn.getResponseCode());
                    output = "FALSE";
                }
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            logger.info("Status :" + output);
            request.setAttribute("status", output);

            rd = request.getRequestDispatcher("CheckVipNumber.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("IsPriorityCustomer")) {
            String WSURL = fh.IsPriorityCustomer();
            logger.info("WSURL FOR IsPriorityCustomer:" + WSURL);
            String Number = request.getParameter("session.sce.number");
            logger.info("Number Send By IVR In IsPriorityCustomer :" + Number);
            try {
                URL url = new URL(WSURL + Number);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
                logger.info("Response Code: " + conn.getResponseCode());
                conn.disconnect();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutput);
                logger.info("JSON Object: \n" + json);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            if (Number.equals("03365054021") || Number.equals("03365531133") || Number.equals("03359990600")) {
                request.setAttribute("out1", "success");
                request.setAttribute("out2", "true");
                request.setAttribute("out3", Number);
            } else {
                request.setAttribute("out1", json.get("status"));
                request.setAttribute("out2", json.get("status_message"));
                request.setAttribute("out3", json.get("result_id"));
            }
//            logger.info("status: " + json.get("status") + "\nStatus_message: " + json.get("status_message") + "\nresult_id: " + json.get("result_id"));
            rd = request.getRequestDispatcher("getAgentid.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("checkCurrentTime")) {
            try {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Date date = new Date();
                //System.out.println(dateFormat.format(date));

                String string1 = "09:00:00";
                Date time1 = new SimpleDateFormat("HH:mm:ss").parse(string1);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(time1);

                String string2 = "19:00:00";
                Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(time2);
                calendar2.add(Calendar.DATE, 1);

                String currentTime = dateFormat.format(date);
                Date d = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
                Calendar calendar3 = Calendar.getInstance();
                calendar3.setTime(d);
                calendar3.add(Calendar.DATE, 1);

                Date x = calendar3.getTime();
                if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                    //checkes whether the current time is between 14:49:00 and 20:11:13.
                    System.out.println(true);
                    request.setAttribute("out1", "true");
                } else {
                    request.setAttribute("out1", "false");
                }

                rd = request.getRequestDispatcher("checkTime.jsp");
                try {
                    rd.forward(request, response);
                } catch (ServletException e) {
                    logger.error("No Internet." + e.getMessage());
                    e.printStackTrace();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (serviceName.equals("pinGeneration")) {

            //Pin Generation Process
//            ATMPINGenerationRequestTransaction att= new ATMPINGenerationRequestTransaction();
//           
//            ATMPINGenerationRequestBody at=new ATMPINGenerationRequestBody();
//            at.setNewPIN("1111");;
//            at.setCardNo("");
//            at.setDCType(output);
//            att.setBody(at);
//            NewWebServiceFromWSDL nw = new NewWebServiceFromWSDL();
//            nw.atmpinGeneration(att);
            //================================
            System.out.println("Enter Pin Generation");
            int cardNumber = Integer.parseInt(request.getParameter("session.sce.CardNumber"));
            int Number = Integer.parseInt(request.getParameter("session.sce.PinNumber"));
            System.out.println("PINGeneration Input Data : " + cardNumber + " | " + Number);
            logger.info("Number Send By IVR In pinGeneration: " + Number);
            if (Number > 5000) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                System.out.println("Pingeneration True");
                request.setAttribute("out1", "true");
            } else {
                request.setAttribute("out1", "false");
                System.out.println("PinGeneration false");
            }
            rd = request.getRequestDispatcher("pinGeneration.jsp");
            try {
                System.out.println("PinGeneration Response");
                rd.forward(request, response);
            } catch (ServletException e) {
                logger.error("No Internet." + e.getMessage());
                System.out.println("PinGeneration Error :" + e.toString());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("pinValidation")) {

            //Pin Validation
//            PINValidationRequestTransaction prt = new PINValidationRequestTransaction();
//            PINValidationRequestBody pvr= new PINValidationRequestBody();
//            pvr.setCardNo("");
//            pvr.setNewPIN("2222");
//            pvr.setDCType("343");
//            NewWebServiceFromWSDL mws=new NewWebServiceFromWSDL();
//            prt.setBody(pvr);
//            mws.pinValidation(prt);
            //===============
            int Number = Integer.parseInt(request.getParameter("session.sce.Pin"));
            logger.info("Number Send By IVR In pinValidation: " + Number);
            if (Number > 7000) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                System.out.println(true);
                request.setAttribute("out1", "Pin Validation true");
            } else {
                request.setAttribute("out1", "Pin Validation false");
            }
            rd = request.getRequestDispatcher("pinValidation.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException e) {
                logger.error("No Internet." + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("GetCustomerDetails")) {
            String WSURL = fh.GetCustomerDetails();
            logger.info("WSURL For GetCustomerDetails: " + WSURL);
            String Number = request.getParameter("session.sce.mobilenumber");
            logger.info("Number Send By IVR In GetCustomerDetails: " + Number);
            try {
                URL url = new URL(WSURL + Number);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                    logger.info("Response Code in Trty \t" + conn.getResponseCode());
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                logger.info("Response Code: " + conn.getResponseCode());
                conn.setReadTimeout(timeout);
                if (conn.getResponseCode() != 201) {
                    if (conn.getResponseCode() == 500) {
                        logger.error("IPCC is maintaining list of failed requests for manual rectification.");
                    } else {
                        logger.error("IPCC is maintaining list of failed requests for manual rectification.");
                        throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + "\t \t \t" + new Timestamp(System.currentTimeMillis()));
                    }
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
//                    logger.info("Response Code 2" + conn.getResponseCode());
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
//                    logger.info("Response Code 3" + conn.getResponseCode());
                conn.disconnect();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutput);
                request.setAttribute("out1", json.get("status"));
                request.setAttribute("out2", json.get("status_message").toString());
                String status = json.get("status").toString();
                logger.info("GetCustomerDetails Json Response " + json);
                if (status.equals("failure")) {
                    request.setAttribute("out2", "Phone Is Blocked.");
                    request.setAttribute("out3", "");
                    request.setAttribute("out4", "");
                    request.setAttribute("out5", "");
                } else {
                    String data = json.get("data").toString();
                    json = (JSONObject) parser.parse(data);
//                    request.setAttribute("out3", (json.get("name")).toString().replaceAll("[\\!\\@\\#\\$\\%\\&\\*\\-\\+\\&.\\^:,]","").trim());
                    request.setAttribute("out3", (json.get("name")).toString().replaceAll("[^a-zA-Z0-9]", " ").trim());
                    request.setAttribute("out4", json.get("Email"));
                    request.setAttribute("out5", json.get("Phone"));
                }

            } catch (Exception e) {
                logger.error("Unable To Connect To Host" + e.getMessage());
                e.printStackTrace();
            }
//            logger.info("Status: " + json.get("status") + "\nStatus_message: " + json.get("status_message") + "\nName: " + json.get("name") + "\n Email:" + json.get("Email") + "\n Phone: " + json.get("Phone"));
            rd = request.getRequestDispatcher("GetCustomerDetails.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException e) {
                logger.error("No Internet." + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                logger.error("There Is No Internet Connection." + e.getMessage());
                e.printStackTrace();
            }

        }
        if (serviceName.equals("ChangeStatus")) {
            String TicketID = "";
            String DuplicateWSURL = fh.CheckCustomerResolvedTickets();
            logger.info("DuplicateWSURL For CheckCustomerResolvedTickets(Change Status): " + DuplicateWSURL);
            String Number = request.getParameter("session.sce.TicketId"); // This Will Get The Number
            Number = Number.substring(1, Number.length());
            logger.info("CheckCustomerResolvedTickets(Change Status) Number Send By IVR " + Number);
            try {
                URL url = new URL(DuplicateWSURL + Number);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
                logger.info("Response Code: " + conn.getResponseCode());
                conn.disconnect();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutput);
                logger.info("JSON Object: \n" + json);
                TicketID = (json.get("result_id")).toString();
                logger.info("TicketID FOR Change Status: " + TicketID);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host" + e.getMessage());
                e.printStackTrace();
            }
            try {
                String WSURL = fh.ChangeStatus();
                logger.info("WSURL FOR Change Status: " + WSURL);
//                String TicketId = request.getParameter("session.sce.TicketId");
                logger.info("Ticket ID Get Through CheckCustomerOpenTickets  Is :" + TicketID);
                String Status = request.getParameter("session.sce.TicketStatus");
                logger.info("Status  Send By IVR Is :" + Status);
                URL url = new URL(WSURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                String input = "{\"TicketId\":" + TicketID + ",\"ticketStatus\":" + Status + "}";
                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();
//                if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
//                    throw new RuntimeException("Failed : HTTP error code : "
//                            + conn.getResponseCode());
//                }
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                String outputCS = null;
                jsonOutput = "";
                while ((outputCS = br.readLine()) != null) {
                    if (outputCS != null) {
                        jsonOutput += outputCS;
                    }
                    logger.info("Output in Change Status function from Server: " + outputCS);
                }
                conn.disconnect();
                String jsonOutputwithoutQuotes = jsonOutput.replace('\'', ' ').trim();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutputwithoutQuotes);
                logger.info("status: " + json.get("status") + "\nStatus_message: " + json.get("status_message") + "\nresult_id: " + json.get("result_id") + "\t \t \t" + new Timestamp(System.currentTimeMillis()));
                request.setAttribute("out1", json.get("status").toString());
                request.setAttribute("out2", json.get("status_message").toString());
                request.setAttribute("out3", json.get("result_id").toString());
                logger.info("JSON Response In Change Status: \n" + json);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host" + e.getMessage());
                e.printStackTrace();
            }
            rd = request.getRequestDispatcher("ChangeStatus.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Reqiest: " + e.getMessage());
                e.printStackTrace();
            }
        } // Change Status End
        if (serviceName.equals("create")) {
            //TicketCreated
            if (CheckTicketExist(request, response)) {
                logger.info("Ticket Already Created Flag true: " + request.getParameter("session.sce.altmobilenumber"));
                request.setAttribute("out1", "TicketAlreadyCreated");
                request.setAttribute("out2", "Ticket Already Created");
                request.setAttribute("out3", "");
                rd = request.getRequestDispatcher("Create.jsp");
                try {
                    rd.forward(request, response);
                } catch (ServletException | IOException e) {
                    logger.error("Unable To Forward Reqiest: " + e.getMessage());
                    e.printStackTrace();
                }
            } else {
                logger.info("Ticket is going to create: " + request.getParameter("session.sce.altmobilenumber"));
                String WSURL = fh.create();
                logger.info("WSURL For Create===" + WSURL);
                String Service = request.getParameter("session.sce.mobilenumber");
                logger.info("Mobile Number Send By IVR: " + Service);
                String MobileNumber = request.getParameter("session.sce.altmobilenumber");
                //            if (MobileNumber == null) {
                //                MobileNumber = "";
                //            }
                logger.info("Alt.Mobile Number Send By IVR :" + MobileNumber);
                String Subject = request.getParameter("session.sce.subject");
                logger.info("Subject Send By IVR: " + Subject);
                String Details = request.getParameter("session.sce.details");
                logger.info("Details Send By IVR : " + Details);
                try {
                    URL url = new URL(WSURL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setConnectTimeout(timeout);
                    conn.setReadTimeout(timeout);
                    String input = "{\"Service\":\"" + Service + "\",\"MobileNumber\":\"" + MobileNumber + "\",\"Subject\":\"" + Subject + "\",\"Details\":\"" + Details + "\"}";
                    logger.info("Input: " + input);
                    OutputStream os = conn.getOutputStream();
                    os.write(input.getBytes());
                    os.flush();
                    logger.info("os" + os + "conn.getResponseCode()" + conn.getResponseCode() + "HttpURLConnection.HTTP_CREATED" + HttpURLConnection.HTTP_CREATED + "conn.getResponseCode" + conn.getResponseCode());
                    if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                        throw new RuntimeException("Failed : HTTP error code : "
                                + conn.getResponseCode());
                    }
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
                    String output2 = null;
                    while ((output2 = br.readLine()) != null) {
                        if (output2 != null) {
                            jsonOutput += output2;
                        }
                        logger.info("Response : " + output2);
                    }
                    conn.disconnect();
                    JSONParser parser = new JSONParser();
                    json = (JSONObject) parser.parse(jsonOutput);
                    //request.setAttribute("out1", json.get("status"));
                    if (json.get("status").equals("success")) {
                        logger.info("Ticket Created Successfully, Your Ticket id: " + (json.get("result_id")).toString());
                        request.setAttribute("out1", "TicketCreated");
                        request.setAttribute("out2", "Ticket Created Successfully");
                        request.setAttribute("out3", (json.get("result_id")).toString());
                    } else {
                        logger.info("Ticket Created Failure, Error Reason: " + (json.get("status_message")).toString());
                        request.setAttribute("out1", "failure");
                        request.setAttribute("out2", json.get("status_message").toString());
                        request.setAttribute("out3", json.get("result_id").toString());
                    }
                    logger.info("JSON Object In Create Function: \n" + json);
                } catch (Exception e) {
                    logger.error("Unable To Connect To Host" + e.getMessage());
                    e.printStackTrace();
                }
                rd = request.getRequestDispatcher("Create.jsp");
                try {
                    rd.forward(request, response);
                } catch (ServletException | IOException e) {
                    logger.error("Unable To Forward Request" + e.getMessage());
                    e.printStackTrace();
                }
            }

        }
        if (serviceName.equals("CheckCustomerOpenTickets")) {
            String WSURL = fh.CheckCustomerOpenTickets();
            String isIssueID = "";
            logger.info("WSURL For CheckCustomerOpenTickets:" + WSURL);
            String Number = request.getParameter("session.sce.number");
            if (Number.startsWith("0")) {
                isIssueID = "false";
            } else {
                isIssueID = null;
                isIssueID = "true";
            }
//            String isIssueID = request.getParameter("session.sce.isIssueID");
            logger.info(ANSI_GREEN + "isIssueID After Checking Number  \t" + ANSI_RED + isIssueID + ANSI_RESET);
            logger.info("Number Send By IVR :" + Number);
            try {
                URL url = new URL(WSURL + Number + "&isIssueID=" + isIssueID);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
                logger.info("Response Code: " + conn.getResponseCode());
                conn.disconnect();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutput);
                logger.info("JSON Response: " + json);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            request.setAttribute("out1", json.get("status"));
            request.setAttribute("out2", json.get("status_message"));
            request.setAttribute("out3", (json.get("result_id")).toString());
//            logger.info("status: " + json.get("status") + "\nStatus_message: " + json.get("status_message") + "\nresult_id: " + json.get("result_id"));
            rd = request.getRequestDispatcher("CheckCustomerOpenTickets.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("CheckPassportStatus")) {
            String WSURL = fh.getPassportStatus();
            String isIssueID = "";
            logger.info("WSURL For CheckPassportStatus:" + WSURL);
            String Number = request.getParameter("session.sce.PassportNumber");

            logger.info("Number Send By IVR :" + Number);
            try {
                URL url = new URL(WSURL + "?PassportNumber=" + Number);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
                logger.info("Response Code: " + conn.getResponseCode());
                conn.disconnect();
                //JSONParser parser = new JSONParser();
                //json = (JSONObject) parser.parse(jsonOutput);
                logger.info("JSON Response: " + json);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host: " + e.getMessage());
                e.printStackTrace();
            }
            request.setAttribute("out1", jsonOutput);
//            logger.info("status: " + json.get("status") + "\nStatus_message: " + json.get("status_message") + "\nresult_id: " + json.get("result_id"));
            rd = request.getRequestDispatcher("passportStatus.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("RequestToftp")) {
            String filename = "";
            String splitfilename = "";
            logger.info("Calling  Request To sftp Server");
            String Request = request.getParameter("session.sce.request");
            logger.info("Request By IVR :" + Request);
            String userName = "ccfax";
            String password = "ccfax@123";
            String serverIP = "10.2.3.68";
            int serverPort = 22;
            String uploadPath = "/share/vpshare/fax/Sending";
            JSch jsch = new JSch();
            Session session = null;
            try {
                session = jsch.getSession(userName, serverIP);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword(password);
                session.connect();
                Channel channel = session.openChannel("sftp");
                channel.connect();
                ChannelSftp sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd(uploadPath);
                Vector filelist = sftpChannel.ls(uploadPath);
                for (int i = 0; i < filelist.size(); i++) {
                    LsEntry entry = (LsEntry) filelist.get(i);
                    logger.info("Filename: " + entry.getFilename());
                    if ((entry.getFilename().contains(".tif")) || (entry.getFilename().contains(".tiff")) || (entry.getFilename().contains(".TIFF")) || (entry.getFilename().contains(".TIF"))) {
                        String ext = FilenameUtils.getExtension(entry.getFilename());
                        String newFileName = entry.getFilename().split("_")[0];
                        DateFormat dt = new SimpleDateFormat("HHmmss");
                        Date date = new Date();
                        String d = dt.format(date).toString();
                        newFileName = newFileName + "_" + d;
                        //entry.getFilename().substring(0, entry.getFilename().length()-7);
                        newFileName = newFileName + "." + ext;

                        sftpChannel.rename("/share/vpshare/fax/Sending/" + entry.getFilename(), "/share/vpshare/fax/Sending/" + newFileName);

                        //splitfilename = entry.getFilename();
                        splitfilename = newFileName;
                        //filename = entry.getFilename().split("_")[0];
                        filename = newFileName.split("_")[0];

                        logger.info("File Ext Sending to IVR " + ext);
                        logger.info("File Name Sending to IVR without ext " + filename);
                        //logger.info("Extension for --" + ANSI_YELLOW_BACKGROUND + ANSI_RED + filename + ANSI_RESET + " is -- " + entry.getFilename().substring(entry.getFilename().lastIndexOf('.'), entry.getFilename().length()));
                    }
                }
                sftpChannel.exit();
                session.disconnect();
            } catch (JSchException | SftpException ex) {
                logger.error("Sftp error: " + ex.getMessage());
            }
            logger.info("Filename with extension: " + filename + "-- Filename without extension: " + ("8" + filename));
            if (filename.equals("")) {
                request.setAttribute("out1", "No New number");
                request.setAttribute("out2", ("fail"));
            } else {
                request.setAttribute("out1", splitfilename);
                int pos = filename.lastIndexOf(".");
                if (pos > 0) {
                    filename = filename.substring(0, pos);
                }
                request.setAttribute("out2", ("8" + filename));
            }
            rd = request.getRequestDispatcher("RequestToftp.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable to Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }

        if (serviceName.equals("ResponseByftp")) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                // Logger.getLogger(VxmlServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            String filename = "";
            String extension = "";
            logger.info("Calling  ResponseByftp function");
            String filenameivr = request.getParameter("session.sce.request");
//            String filenameivr = "faran1.tif";
            logger.info("Filename By IVR :" + filenameivr);
            String userName = "ccfax";
            String password = "ccfax@123";
            String serverIP = "10.2.3.68";
            int serverPort = 22;
            String uploadPath = "/share/vpshare/fax/Sending";
            JSch jsch = new JSch();
            Session session = null;
            try {
                session = jsch.getSession(userName, serverIP);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword(password);
                session.connect();
                Channel channel = session.openChannel("sftp");
                channel.connect();
                ChannelSftp sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd(uploadPath);
                Vector filelist = sftpChannel.ls(uploadPath);
                for (int i = 0; i < filelist.size(); i++) {
                    LsEntry entry = (LsEntry) filelist.get(i);
                    logger.info("File name: " + entry.getFilename());
                    if (entry.getFilename().equals(filenameivr)) {

                        //int position = entry.getFilename().lastIndexOf(".");
                        // if (position > 0) {
                        // extension = entry.getFilename().substring(position + 1);
                        extension = FilenameUtils.getExtension(entry.getFilename());
                        logger.info("Extension : " + extension);
                        //filename = entry.getFilename().substring(0, position);
                        filename = FilenameUtils.getBaseName(entry.getFilename());
                        logger.info("Filename  : " + filename);
                        filename = filename + "_" + new Timestamp(System.currentTimeMillis());
                        logger.info("Filename with time: " + filename);
                        //}

                        logger.info("Matched file name is (dir and ivr): " + entry.getFilename());
                        sftpChannel.rename(entry.getFilename(), uploadPath + "/Sent/" + filename + "." + extension);
                    }
                }
                sftpChannel.exit();
                session.disconnect();
            } catch (JSchException | SftpException ex) {
                logger.error("Error Sftp : " + ex.getMessage());
            }
            request.setAttribute("out1", "Success");
            rd = request.getRequestDispatcher("ResponseByftp.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable to forward request : " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("SetFeedBack")) {
            String IssueID = "";
            String isIssueID = "";
            String FeedBackID = request.getParameter("session.sce.FeedBackID");
            String Number = request.getParameter("session.sce.Number");
            String AgentID = request.getParameter("session.sce.AgentID");
            logger.info("FeedBack Is" + ANSI_YELLOW_BACKGROUND + ANSI_RED + ANSI_RESET + FeedBackID + "\t \t \t" + new Timestamp(System.currentTimeMillis()));
            logger.info("Number Is" + ANSI_YELLOW_BACKGROUND + ANSI_BLUE + ANSI_RESET + Number + "\t \t \t" + new Timestamp(System.currentTimeMillis()));
            logger.info("AgentID Is" + ANSI_YELLOW_BACKGROUND + ANSI_PURPLE + ANSI_RESET + AgentID + "\t \t \t" + new Timestamp(System.currentTimeMillis()));

            String WSURL = fh.CheckCustomerOpenTickets();
            logger.info("WSURL For CheckCustomerOpenTickets To get TickedID In SetFeedBack: " + WSURL);
            try {
                URL url = new URL(WSURL + Number + "&isIssueID=false");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
                logger.info("Response Code: " + conn.getResponseCode());
                conn.disconnect();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutput);
                logger.info(ANSI_GREEN + "Output from the CRM in CheckCustomerOpenTickets is--" + ANSI_BLUE + jsonOutput + ANSI_RESET);
                IssueID = (json.get("result_id")).toString();
                logger.info("TicketID FOR SetFeedBack : " + IssueID);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host" + e.getMessage());
                e.printStackTrace();
            }
            String WSURLSetFB = fh.SetFeedBack();
            logger.info("WSURL For SetFeedBack :" + WSURLSetFB);
            try {
                jsonOutput = "";
                output = "";
                URL url = new URL(WSURLSetFB);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                String input = "{\"FeedBackID\":" + Integer.parseInt(FeedBackID) + ",\"IssueID\":" + Integer.parseInt(IssueID) + ",\"Number\":\"" + Number + "\",\"AgentID\":\"" + AgentID + "\"}";
                logger.info("Input For SetFeedBack: " + ANSI_RED + input + ANSI_RESET);
                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
                logger.info("Response Code: " + conn.getResponseCode());
                conn.disconnect();
                logger.info(ANSI_GREEN + "Output from the CRM is " + ANSI_BLUE + jsonOutput + ANSI_RESET);
                json = null;
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutput);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host" + e.getMessage());
                e.printStackTrace();
            }
            request.setAttribute("out1", json.get("status"));
            request.setAttribute("out2", json.get("status_message"));
            logger.info("status: " + json.get("status") + "\nStatus_message: " + json.get("status_message"));
            rd = request.getRequestDispatcher("SetFeedBack.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (serviceName.equals("GetFeedBack")) {
            String WSURL = fh.GetFeedBack();
            logger.info("WSURL For GetFeedBack: " + WSURL);
            String IssueID = request.getParameter("session.sce.IssueID");
            logger.info("IssueID Send By IVR :" + IssueID);
            try {
                URL url = new URL(WSURL + IssueID);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                while ((output = br.readLine()) != null) {
                    if (output != null) {
                        jsonOutput += output;
                    }
                }
                logger.info("Response Code: " + conn.getResponseCode());
                conn.disconnect();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(jsonOutput);
                logger.info("JSON Object: \n" + json);
            } catch (Exception e) {
                logger.error("Unable To Connect To Host" + e.getMessage());
                e.printStackTrace();
            }
            request.setAttribute("out1", json.get("status"));
            request.setAttribute("out2", json.get("status_message"));
            request.setAttribute("out3", (json.get("result_id")).toString());
//            logger.info("status: " + json.get("status") + "\nStatus_message: " + json.get("status_message") + "\nresult_id: " + json.get("result_id"));
            rd = request.getRequestDispatcher("GetFeedBack.jsp");
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException e) {
                logger.error("Unable To Forward Request:" + e.getMessage());
                e.printStackTrace();
            }
        }

    } // Service Function End

    private boolean CheckTicketExist(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Checking if ticket exist");
        String PathInfo = getServletContext().getRealPath("");
        int timeout = 10000;
        String output = "";
        String jsonOutput = "";
        JSONObject json = null;
        RequestDispatcher rd = null;
        FileHandler fh = new FileHandler(PathInfo);
        fh.WSURL();
        String WSURL = fh.CheckCustomerOpenTickets();
        String isIssueID = "";
        logger.info("WSURL For CheckCustomerOpenTickets :" + WSURL);
        String Number = request.getParameter("session.sce.mobilenumber");
        if (Number.startsWith("0")) {
            isIssueID = "false";
        } else {
            isIssueID = null;
            isIssueID = "true";
        }
//            String isIssueID = request.getParameter("session.sce.isIssueID");
        logger.info(ANSI_GREEN + "isIssueID After Checking Number--" + ANSI_RED + isIssueID + ANSI_RESET);
        logger.info("Checking if ticket exist Number Send By IVR -" + Number);
        try {
            URL url = new URL(WSURL + Number + "&isIssueID=" + isIssueID);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setConnectTimeout(timeout);
            conn.setReadTimeout(timeout);
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            while ((output = br.readLine()) != null) {
                if (output != null) {
                    jsonOutput += output;
                }
            }
            logger.info("Response Code: " + conn.getResponseCode());
            conn.disconnect();
            JSONParser parser = new JSONParser();
            json = (JSONObject) parser.parse(jsonOutput);
            logger.info("JSON Object: \n" + json);
            rd = request.getRequestDispatcher("CheckCustomerOpenTickets.jsp");
            if (json.get("status").equals("success")) {
                logger.info("Ticket Existed, Service Response: " + json);
                return true;
                //                 request.setAttribute("out1", "Ticket Alre");
                //                request.setAttribute("out2", json.get("status_message"));
                //                request.setAttribute("out3", (json.get("result_id")).toString());
                //                try {
                //                rd.forward(request, response);
                //                } catch (ServletException | IOException e) {
                //                    e.printStackTrace();
                //                }
            } else {
                logger.info("Ticket Not Existed Sending Create Request, Service Response: " + json);
                return false;
            }
        } catch (Exception e) {
            logger.error("Unable To Connect To Host" + e.getMessage());
            e.printStackTrace();
        }
        return false;
//            logger.info("status: " + json.get("status") + "\nStatus_message: " + json.get("status_message") + "\nresult_id: " + json.get("result_id"));

    }

}
