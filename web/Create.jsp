<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<vxml version="1.0">
    <!-- 
    <% request.setCharacterEncoding("GB2312"); %>
    -->
    <%

        System.out.println("In Create.jsp page");
        String status = (String) request.getAttribute("out1");
        String status_message = (String) request.getAttribute("out2");
        String result_id = (String) request.getAttribute("out3");
        System.out.println("Status in jsp: " + status);
        System.out.println("Status Message in jsp: " + status_message);
        System.out.println("Result ID in jsp: " + result_id);
        out.println("<var name=\"rs\" expr=\"\'" + status + "\'\"/>");
        out.println("<var name=\"rsB\" expr=\"\'" + status_message + "\'\"/>");
        out.println("<var name=\"rsC\" expr=\"\'" + result_id + "\'\"/>");
    %>
    <form>	
        <block>			
            <log><%=status%></log>
            <log><%=status_message%></log>
            <log><%=result_id%></log>
            <return namelist="rs rsB rsC"/>
        </block>
    </form>
</vxml>