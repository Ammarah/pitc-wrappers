<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<vxml version="1.0">
    <!-- 
    <% request.setCharacterEncoding("GB2312"); %>
    -->
    <%

        System.out.println("In Requesttoftp.jsp page");
        String efilename = (String) request.getAttribute("out1");
        String filename = (String) request.getAttribute("out2");
        System.out.println("Filename with extension in jsp: " + efilename);
        System.out.println("Filename without extension in jsp: " + filename);
        out.println("<var name=\"rs\" expr=\"\'" + efilename + "\'\"/>");
        out.println("<var name=\"rsB\" expr=\"\'" + filename + "\'\"/>");
    %>
    <form>	
        <block>			
            <log><%=efilename%></log>
            <log><%=filename%></log>
            <return namelist="rs rsB"/>
        </block>
    </form>
</vxml>