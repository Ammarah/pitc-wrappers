<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<vxml version="1.0">
    <% request.setCharacterEncoding("GB2312"); %>
    <%
        System.out.println("In GetCustomerDetails.jsp Page");
        String status = (String) request.getAttribute("out1");
        String status_message = (String) request.getAttribute("out2");
        String name = (String) request.getAttribute("out3");
        String email = (String) request.getAttribute("out4");
        String phone = (String) request.getAttribute("out5");
        System.out.println("Status in jsp: " + status);
        System.out.println("Status Message in jsp: " + status_message);
        System.out.println("Name in jsp \t" + name);
        System.out.println("Email in jsp \t" + email);
        System.out.println("Phone in jsp \t" + phone);
        out.println("<var name=\"rs\" expr=\"\'" + status + "\'\"/>");
        out.println("<var name=\"rsB\" expr=\"\'" + status_message + "\'\"/>");
        out.println("<var name=\"rsC\" expr=\"\'" + name + "\'\"/>");
        out.println("<var name=\"rsD\" expr=\"\'" + email + "\'\"/>");
        out.println("<var name=\"rsE\" expr=\"\'" + phone + "\'\"/>");
    %>
    <form>	
        <block>			
            <log><%=status%></log>
            <log><%=status_message%></log>
            <log><%=name%></log>
            <log><%=email%></log>
            <log><%=phone%></log>
            <return namelist="rs rsB rsC rsD rsE"/>
        </block>
    </form>
</vxml>